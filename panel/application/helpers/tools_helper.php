<?php
function convertToSEO($text){
$turkce = array("ç","Ç","ğ","Ğ","ü","Ü","ö","Ö","ı","İ","ş","Ş",".",",","!","'","\""," ","?","*","_","|","=","(",")");

$convert = array("c","c","g","g","u","u","o","o","i","i","s","s","-","-","-","-","-","-","-","-","-","-","-","-","-","-");
return strtolower(str_replace($turkce, $convert, $text));
}

function get_active_user(){

$t=&get_instance();
$user=$t->session->userdata("userpanel");
if($user){
return $user;
}else{
return false;
}

}

function upload_picture($file,$uploadPath,$width,$height,$name){
$t=&get_instance();
$t->load->library("simpleimagelib");
$upload_error=false;
try {
  $simpleImage = $t->simpleimagelib->get_simple_image_instance();
  $simpleImage
    ->fromFile($file)                   
    ->autoOrient()                              
    ->thumbnail($width, $height)                          
    ->toFile("{$uploadPath}/$name", 'image/png');                               
 
} catch(Exception $err) {

  $error = $err->getMessage();
  $upload_error=true;
}

if($upload_error){
echo $error;
}else{
return true;
}


}

