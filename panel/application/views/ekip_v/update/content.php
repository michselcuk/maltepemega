<div class="row">
	<div class="col-md-12"><h4><?php echo $item->adsoyad . " kaydını düzenliyorsunuz."; ?></h4>
	</div>
</div>
<p></p>
<div class="row">
<div class="col-md-2"></div>
	<div class="col-md-8">

<div class="widget">
					<div class="widget-body">
						<form action="<?php echo base_url("ekip/update/$item->id"); ?>" method="POST" enctype="multipart/form-data">
							<div class="form-group">
								<label>Adı Soyadı</label>
								<input type="text" class="form-control" placeholder="Adı Soyadı" name="adsoyad" required value="<?php echo $item->adsoyad; ?>">
							</div>
							<div class="form-group">
								<label>Görevi</label>
								<input type="text" class="form-control" placeholder="Görevi" name="gorev" required value="<?php echo $item->gorev; ?>">
							</div>

							<div class="form-group">
								<label>Açıklama</label>
								<textarea class="m-0" data-plugin="summernote" data-options="{height: 250}" name="aciklama" required><?php echo $item->aciklama; ?></textarea>
							</div>
							<div class="row">
							<div class="col-md-2">
								<img src="<?php echo base_url("uploads/$viewFolder/$item->img_url"); ?>">
							</div>
							<div class="form-group col-md-9">
								<label >Fotoğrafı</label>
								<input type="file" class="form-control" name="img_url">
							</div>
							</div>

							<button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
							<a href="<?php echo base_url("ekip"); ?>" class="btn btn-danger btn-md btn-outline">İptal</a>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
	</div>
	<div class="col-md-2"></div>
</div>
