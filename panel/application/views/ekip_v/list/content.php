<div class="row">
	<div class="col-md-12"><h4>Mega Ekibi
	<a href="<?php echo base_url("ekip/new_form")?>" class="btn btn-xs btn-primary btn-outline pull-right"><i class="fa fa-plus"></i> Yeni Ekle</a></h4>


	</div>
</div>
<p></p>
<div class="row">

<div class="col-md-12">
				<div class="widget p-lg">

					<?php if(empty($items)) {?>
					<div class="alert alert-info text-center">
								<h5>KAYIT BULUNAMADI</h5>
								<p>Gösterilecek kayıt bulunamadı. Yeni kayıt eklemek için lütfen <a href="<?php echo base_url("ekip/new_form")?>">tıklayınız. </a></p>
</div>
<?php } else {?>
					<div class="table-responsive">
					<table class="table table-hover table-bordered content-container">
						<thead>
							<th class="w50">Fotoğrafı</th>
							<th>Adı Soyadı</th>
							<th>Görevi</th>
							<th>Açıklama</th>
							<th class="w50">Durumu</th>
							<th class="w160">İşlemler</th>
						</thead>



						<tbody class="sortable" data-url="<?php echo base_url("ekip/rankSetter"); ?>">
		<?php foreach($items as $item)  {  ?>
							<tr id="ord-<?php echo $item->id; ?>">
							<td><img width="50px" src="<?php echo base_url("uploads/$viewFolder/$item->img_url"); ?>" alt="<?php echo $item-> adsoyad; ?>" class="img-rounded"></td>
							<td><?php echo $item-> adsoyad; ?></td>
							<td><?php echo $item-> gorev; ?></td>
							<td><?php echo substr($item-> aciklama,0,70); ?></td>
							<td><input 
								data-url="<?php echo base_url("ekip/isActiveSetter/$item->id"); ?>" 
								class="isActive" type="checkbox" data-switchery data-size="small" 
								<?php 
										if(($item-> aktif)==1){

											echo "checked";
										}

								?>

							 /></td>
							<td>
								<button 
								data-url="<?php echo base_url("ekip/delete/$item->id"); ?>" 
								class="btn btn-sm btn-danger btn-outline remove-btn">
								<i class="fa fa-trash"></i> Sil
								</button>
								<a href="<?php echo base_url("ekip/update_form/$item->id"); ?>" class="btn btn-sm btn-info btn-outline"><i class="fa fa-pencil-square-o"></i> Düzenle</a>
							</td>
						</tr>
					<?php }?>
						</tbody>

					</table></div>
				<?php }?>
				</div><!-- .widget -->
			</div>
				</div>
				</div>