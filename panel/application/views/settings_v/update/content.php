<div class="row">
	<div class="col-md-12"><h4>Site ayarlarını düzenliyorsunuz.</h4>
	</div>
</div>
<p></p>
<div class="row">
<div class="col-md-2"></div>
	<div class="col-md-8">

<div class="widget">
					<div class="widget-body">
					<form action="<?php echo base_url("settings/update/$item->id"); ?>" method="POST" enctype="multipart/form-data">

					<div class="m-b-lg nav-tabs-horizontal">
						<!-- tabs list -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#tab-1" aria-controls="tab-3" role="tab" data-toggle="tab">Genel Ayarlar</a></li>
							<li role="presentation"><a href="#tab-2" aria-controls="tab-1" role="tab" data-toggle="tab">İletişim Ayarları</a></li>
							<li role="presentation"><a href="#tab-3"  aria-controls="tab-2" role="tab" data-toggle="tab">Sosyal Medya Ayarları</a></li>
							<li role="presentation"><a href="#tab-4"  aria-controls="tab-2" role="tab" data-toggle="tab">Logo Ayarları</a></li>
						</ul><!-- .nav-tabs -->

						<!-- Tab panes -->
						<div class="tab-content p-md">
							<div role="tabpanel" class="tab-pane in active fade" id="tab-1">

						<table class="table table-hover table-bordered content-container">
						<tbody>
							<div class="form-group">
								<label>Site Başlığı</label>
								<input type="text" class="form-control" placeholder="Site Başlığı" name="title" required value="<?php echo $item->title; ?>">
							</div>
							<div class="form-group">
								<label>Yazar</label>
								<input type="text" class="form-control" placeholder="Yazar" name="author" required value="<?php echo $item->author; ?>">
							</div>
							<div class="form-group">
								<label>Site Açıklaması</label>
								<input type="text" class="form-control" placeholder="Site Açıklaması" name="description" required value="<?php echo $item->description; ?>">
							</div>
							<div class="form-group">
								<label>Anahtar Kelimeler</label>
								<input type="text" class="form-control" placeholder="Anahtar Kelimeler" name="keywords" required value="<?php echo $item->keywords; ?>">
							</div>
							<div class="form-group">
								<label>Footer</label>
								<input type="text" class="form-control" placeholder="Footer" name="footer" required value="<?php echo $item->footer; ?>">
							</div>
						</tbody>
						</table>


							</div><!-- .tab-pane  -->

							<div role="tabpanel" class="tab-pane fade" id="tab-2">

						<div class="form-group">
								<label>Adres</label>
								<input type="text" class="form-control" placeholder="Adres" name="adres" required value="<?php echo $item->adres; ?>">
							</div>
							<div class="form-group">
								<label>Telefon Numarası</label>
								<input type="text" class="form-control" placeholder="Telefon Numarası" name="telefon" required value="<?php echo $item->telefon; ?>">
							</div>
							<div class="form-group">
								<label>E-Mail Adresi</label>
								<input type="text" class="form-control" placeholder="E-Mail Adresi" name="mail" required value="<?php echo $item->mail; ?>">
							</div>
							</div><!-- .tab-pane  -->

							<div role="tabpanel" class="tab-pane fade" id="tab-3">
							<div class="form-group">
								<label>Facebook Adresi</label>
								<input type="text" class="form-control" placeholder="Facebook Adresi" name="facebook" required value="<?php echo $item->facebook; ?>">
							</div>
							<div class="form-group">
								<label>İnstagram Adresi</label>
								<input type="text" class="form-control" placeholder="İnstagram Adresi" name="instagram" required value="<?php echo $item->instagram; ?>">
							</div>
							<div class="form-group">
							<label>Twitter Adresi</label>
								<input type="text" class="form-control" placeholder="Twitter Adresi" name="twitter" required value="<?php echo $item->twitter; ?>">
							</div>

							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab-4">
							<div class="row">
							<div class="col-md-2">
								<img src="<?php echo base_url("uploads/$viewFolder/$item->logo"); ?>">
							</div>
							<div class="form-group col-md-9">
								<label >Logo</label>
								<input type="file" class="form-control" name="logo">
							</div>
							</div>
							</div><!-- .tab-pane  -->
						</div><!-- .tab-content  -->
					</div>

					<button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
							<a href="<?php echo base_url("settings"); ?>" class="btn btn-danger btn-md btn-outline">İptal</a>
					</form>



					</div><!-- .widget-body -->
				</div><!-- .widget -->
	</div>
	<div class="col-md-2"></div>
</div>
