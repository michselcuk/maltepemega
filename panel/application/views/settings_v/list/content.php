<div class="row">
	<div class="col-md-12"><h4>Ayarlar
	</h4>


	</div>
</div>
<p></p>
<div class="row">

<div class="col-md-12">
				<div class="widget p-lg">
					<?php foreach($items as $item)  {  ?>
					<div class="m-b-lg nav-tabs-horizontal">
						<!-- tabs list -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#tab-1" aria-controls="tab-3" role="tab" data-toggle="tab">Genel Ayarlar</a></li>
							<li role="presentation"><a href="#tab-2" aria-controls="tab-1" role="tab" data-toggle="tab">İletişim Ayarları</a></li>
							<li role="presentation"><a href="#tab-3"  aria-controls="tab-2" role="tab" data-toggle="tab">Sosyal Medya Ayarları</a></li>
							<li role="presentation"><a href="#tab-4"  aria-controls="tab-2" role="tab" data-toggle="tab">Logo Ayarları</a></li>
						</ul><!-- .nav-tabs -->

						<!-- Tab panes -->
						<div class="tab-content p-md">
							<div role="tabpanel" class="tab-pane in active fade" id="tab-1">

						<table class="table table-hover table-bordered content-container">
						<tbody>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="w160 text-center"><b>Site Başlığı</b></td>
							<td><?php echo $item-> title; ?></td>
							</tr>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="text-center"><b>Yazar</b></td>
							<td><?php echo $item-> author; ?></td>
							</tr>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="text-center"><b>Anahtar Kelimeler</b></td>
							<td><?php echo $item-> keywords; ?></td>
							</tr>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="text-center"><b>Site Açıklaması</b></td>
							<td><?php echo $item-> description; ?></td>
							</tr>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="text-center"><b>Footer</b></td>
							<td><?php echo $item-> footer; ?></td>
							</tr>
						</tbody>
						</table>


							</div><!-- .tab-pane  -->

							<div role="tabpanel" class="tab-pane fade" id="tab-2">
					<table class="table table-hover table-bordered content-container">
						<tbody>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="w160 text-center"><b>Adres</b></td>
							<td><?php echo $item-> adres; ?></td>
							</tr>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="text-center"><b>Telefon Numarası</b></td>
							<td><?php echo $item-> telefon; ?></td>
							</tr>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="text-center"><b>E-mail Adresi</b></td>
							<td><?php echo $item-> mail; ?></td>
							</tr>
						</tbody>
					</table>
							</div><!-- .tab-pane  -->

							<div role="tabpanel" class="tab-pane fade" id="tab-3">
								<table class="table table-hover table-bordered content-container">
						<tbody>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="w160 text-center"><b>Facebook Adresi</b></td>
							<td><?php echo $item-> facebook; ?></td>
							</tr>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="text-center"><b>İnstagram Adresi</b></td>
							<td><?php echo $item-> instagram; ?></td>
							</tr>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="text-center"><b>Twitter Adresi</b></td>
							<td><?php echo $item-> twitter; ?></td>
							</tr>
						</tbody>
					</table>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tab-4">
								<tbody>
							<tr id="ord-<?php echo $item->id; ?>">
							<td class="w160 text-center"><b>Logo</b></td>
							<td><img width="100px" src="<?php echo base_url("uploads/$viewFolder/$item->logo"); ?>" class="img-rounded" alt="<?php echo $item-> title; ?>"> <?php echo $item-> title; ?></td>
							</tr>
						</tbody>
							</div><!-- .tab-pane  -->
						</div><!-- .tab-content  -->
					</div><!-- .nav-tabs-horizontal -->
			<?php }?>
				<a href="<?php echo base_url("settings/update_form/$item->id"); ?>" class="btn btn-sm btn-info btn-outline"><i class="fa fa-pencil-square-o"></i> Düzenle</a>
				</div><!-- .widget -->

			</div>
				</div>
				</div>