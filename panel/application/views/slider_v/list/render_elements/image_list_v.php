<?php if(empty($item_images)) {?>
					<div class="alert alert-info text-center">
								<h5>KAYIT BULUNAMADI</h5>
								<p>Gösterilecek kayıt bulunamadı.</p>
</div>
<?php } else {?>
					<div class="table-responsive">

					<table class="table table-bordered table-striped table-hover">
						<thead>
							<th>Fotoğraf</th>
							<th>Fotoğraf Adı</th>
							<th>Durumu</th>
							<th>Kapak</th>
							<th>İşlem</th>
						</thead>
						<tbody class="sortable" data-url="<?php echo base_url("slider/imageRankSetter"); ?>">
							<?php foreach($item_images as $image){ ?>

							<tr id="ord-<?php echo $image->id; ?>">
								<td class="w100"><img width="50px" src="<?php echo base_url("uploads/{$viewFolder}/$image->img_url"); ?>" alt="<?php echo $image->img_url; ?>" class="img-responsive"></td>
								<td><?php echo $image->img_url; ?></td>

								<td class="w100"><input 
								data-url="<?php echo base_url("slider/imageIsActiveSetter/$image->id"); ?>" 
								class="isActive" type="checkbox" data-switchery data-size="small" 
								<?php 
										if(($image-> aktif)==1){
											echo "checked";
										}

								?>

							 /></td>
							 	<td class="w100"><input 
								data-url="<?php echo base_url("slider/isCoverSetter/$image->id"); ?>" 
								class="isCover" type="checkbox" data-switchery data-size="small" data-color="#ff5b5b" 
								<?php 
										if(($image-> isCover)==1){
											echo "checked";
										}

								?>

							 /></td>
								<td class="w160">
								<button 
								data-url="<?php echo base_url("slider/imageDelete/$image->id"); ?>"
								class="btn btn-sm btn-danger btn-outline remove-btn">
								<i class="fa fa-trash"></i> Sil
								</button>
								<a href="<?php echo base_url("slider/update_form/$image->id"); ?>" class="btn btn-sm btn-info btn-outline"><i class="fa fa-pencil-square-o"></i> Düzenle</a>
							</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
					</div>
<?php } ?>