<div class="row">
	<div class="col-md-12"><h4><?php echo $item->img_url . " kaydını düzenliyorsunuz."; ?></h4>
	</div>
</div>
<p></p>
<div class="row">
<div class="col-md-2"></div>
	<div class="col-md-8">

<div class="widget">
					<div class="widget-body">
						<form action="<?php echo base_url("slider/update/$item->id"); ?>" method="POST">
							<div class="form-group">
								<label>Slider Sloganı</label>
								<input type="text" class="form-control" placeholder="Slider Sloganı" name="slogan" required value="<?php echo $item->slogan; ?>">
							</div>
							<div class="form-group">
								<label>Slider Açıklaması</label>
								<textarea class="m-0" data-plugin="summernote" data-options="{height: 250}" name="aciklama" required><?php echo $item->aciklama; ?></textarea>
							</div>
							<button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
							<a href="<?php echo base_url("slider"); ?>" class="btn btn-danger btn-md btn-outline">İptal</a>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->	
	</div>
	<div class="col-md-2"></div>
</div>
