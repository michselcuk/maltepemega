<div class="row">
	<div class="col-md-12"><h4><?php echo $item->ad . " kaydını düzenliyorsunuz."; ?></h4>
	</div>
</div>
<p></p>
<div class="row">
<div class="col-md-2"></div>
	<div class="col-md-8">

<div class="widget">
					<div class="widget-body">
						<form action="<?php echo base_url("partner/update/$item->id"); ?>" method="POST" enctype="multipart/form-data">
							<div class="form-group">
								<label>Adı</label>
								<input type="text" class="form-control" placeholder="Adı" name="ad" required value="<?php echo $item->ad; ?>">
							</div>
							<div class="form-group">
								<label>Linki</label>
								<input type="text" class="form-control" placeholder="Linki" name="link" required value="<?php echo $item->link; ?>">
							</div>
							<div class="row">
							<div class="col-md-2">
								<img src="<?php echo base_url("uploads/$viewFolder/$item->img_url"); ?>">
							</div>
							<div class="form-group col-md-9">
								<label >Logo</label>
								<input type="file" class="form-control" name="img_url">
							</div>
							</div>
							<button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
							<a href="<?php echo base_url("partner"); ?>" class="btn btn-danger btn-md btn-outline">İptal</a>
						</form>


					</div><!-- .widget-body -->
				</div><!-- .widget -->
	</div>
	<div class="col-md-2"></div>
</div>
