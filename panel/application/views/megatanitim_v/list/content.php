<div class="row">
	<div class="col-md-12"><h4>Mega Tanıtım
</h4>


	</div>
</div>
<p></p>
<div class="row">

<div class="col-md-12">
				<div class="widget p-lg">

					<?php if(empty($items)) {?>
					<div class="alert alert-info text-center">
								<h5>KAYIT BULUNAMADI</h5>
								<p>Gösterilecek kayıt bulunamadı. Yeni kayıt eklemek için lütfen <a href="<?php echo base_url("megatanitim/new_form")?>">tıklayınız. </a></p>
</div>
<?php } else {?>
					<div class="table-responsive">
					<table class="table table-hover table-bordered content-container ">
						<thead>
							<th>Slogan 1</th>
							<th>Slogan2</th>
							<th>Alt Başlık</th>
							<th>Açıklama</th>
							<th class="w200">İşlemler</th>
						</thead>



						<tbody>
		<?php foreach($items as $item)  {  ?>
							<tr id="ord-<?php echo $item->id; ?>">
							<td><?php echo $item->baslik1; ?></td>
							<td><?php echo $item->baslik2; ?></td>
							<td><?php echo $item->altbaslik; ?></td>
							<td><?php echo substr($item-> aciklama,0,50); ?></td>
							<td>
								<a href="<?php echo base_url("megatanitim/update_form/$item->id"); ?>" class="btn btn-sm btn-info btn-outline"><i class="fa fa-pencil-square-o"></i> Düzenle</a>
								<a href="<?php echo base_url("megatanitim/image_form/$item->id"); ?>" class="btn btn-sm btn-dark btn-outline"><i class="fa fa-file-image-o"></i> Resimler</a>
							</td>
						</tr>
					<?php }?>
						</tbody>

					</table></div>
				<?php }?>
				</div><!-- .widget -->
			</div>
				</div>
				</div>