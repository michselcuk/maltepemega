<div class="row">
	<div class="col-md-12"><h4>Mega tanıtım sayfası verilerini düzenliyorsunuz.</h4>
	</div>
</div>
<p></p>
<div class="row">
<div class="col-md-2"></div>
	<div class="col-md-8">

<div class="widget">
					<div class="widget-body">
						<form action="<?php echo base_url("megatanitim/update/$item->id"); ?>" method="POST">
							<div class="form-group">
								<label>Slogan 1</label>
								<input type="text" class="form-control" placeholder="Slogan" name="baslik1" required value="<?php echo $item->baslik1; ?>">
							</div>
							<div class="form-group">
								<label>Slogan 2</label>
								<input type="text" class="form-control" placeholder="Slogan" name="baslik2" required value="<?php echo $item->baslik2; ?>">
							</div>
							<div class="form-group">
								<label>Alt Başlık</label>
								<input type="text" class="form-control" placeholder="Alt Başlık" name="altbaslik" required value="<?php echo $item->altbaslik; ?>">
							</div>

							<div class="form-group">
								<label>Proje Açıklaması</label>
								<textarea class="m-0" data-plugin="summernote" data-options="{height: 250}" name="aciklama" required><?php echo $item->aciklama; ?></textarea>
							</div>
							<button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
							<a href="<?php echo base_url("megatanitim"); ?>" class="btn btn-danger btn-md btn-outline">İptal</a>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
	</div>
	<div class="col-md-2"></div>
</div>
