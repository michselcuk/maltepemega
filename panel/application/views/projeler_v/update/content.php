<div class="row">
	<div class="col-md-12"><h4><?php echo $item->adi . " kaydını düzenliyorsunuz."; ?></h4>
	</div>
</div>
<p></p>
<div class="row">
<div class="col-md-2"></div>
	<div class="col-md-8">

<div class="widget">
					<div class="widget-body">
						<form action="<?php echo base_url("projeler/update/$item->id"); ?>" method="POST">
							<div class="form-group">
								<label>Proje Adı</label>
								<input type="text" class="form-control" placeholder="Proje Adı" name="adi" required value="<?php echo $item->adi; ?>">
							</div>

							<div class="form-group">
								<label>Proje Yürütücüsü</label>
								<div class="radio radio-primary">
										<input type="radio"  name="ililce" value="il" required
										<?php if($item->il=="1"){

											echo "checked";
										}

										?>
										>
										<label for="radio-primary">İstanbul MEM</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" name="ililce" value="ilce" required
										<?php if($item->ilce=="1"){

											echo "checked";
										}

										?>

										>
										<label for="radio-primary">Maltepe MEM</label>
								</div>
							</div>
							<div class="form-group">
								<label>Proje Katılımcı Sayısı</label>
							<div class="col-sm-12">
								<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="Okul Sayısı" name="okul" required value="<?php echo $item->okul; ?>">
								</div>
								<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="Öğretmen Sayısı" name="ogretmen" required value="<?php echo $item->ogretmen; ?>">
								</div>
								<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="Öğrenci Sayısı" name="ogrenci" required value="<?php echo $item->ogrenci; ?>">
								</div>
							</div>
							</div><br>

							<div class="form-group">
								<label>Proje Açıklaması</label>
								<textarea class="m-0" data-plugin="summernote" data-options="{height: 250}" name="aciklama" required><?php echo $item->aciklama; ?></textarea>
							</div>
							<button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
							<a href="<?php echo base_url("projeler"); ?>" class="btn btn-danger btn-md btn-outline">İptal</a>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->



	
	</div>
	<div class="col-md-2"></div>
</div>
