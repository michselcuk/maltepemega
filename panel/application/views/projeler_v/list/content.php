<div class="row">
	<div class="col-md-12"><h4>Projeler
	<a href="<?php echo base_url("projeler/new_form")?>" class="btn btn-xs btn-primary btn-outline pull-right"><i class="fa fa-plus"></i> Yeni Ekle</a></h4>


	</div>
</div>
<p></p>
<div class="row">

<div class="col-md-12">
				<div class="widget p-lg">

					<?php if(empty($items)) {?>
					<div class="alert alert-info text-center">
								<h5>KAYIT BULUNAMADI</h5>
								<p>Gösterilecek kayıt bulunamadı. Yeni kayıt eklemek için lütfen <a href="<?php echo base_url("projeler/new_form")?>">tıklayınız. </a></p>
</div>
<?php } else {?>
					<div class="table-responsive">
					<table class="table table-hover table-bordered content-container ">
						<thead>
							<th>Proje Adı</th>
							<th>Proje Açıklaması</th>
							<th>Proje Linki</th>
							<th>Proje Yürütücüsü</th>
							<th>Durumu</th>
							<th class="w255">İşlemler</th>
						</thead>



						<tbody class="sortable" data-url="<?php echo base_url("projeler/rankSetter"); ?>">
		<?php foreach($items as $item)  {  ?>
							<tr id="ord-<?php echo strip_tags($item->id); ?>">
							<td><?php echo strip_tags($item-> adi); ?></td>
							<td><?php echo character_limiter(strip_tags($item-> aciklama),50); ?></td>
							<td><?php echo $item-> url; ?></td>
							<td>
							<?php 
										if(($item-> ilce)==1){

											echo "Maltepe MEM";
										}else{
											echo "İstanbul MEM";
										}

								?>
							
							
							</td>
							<td><input 
								data-url="<?php echo base_url("projeler/isActiveSetter/$item->id"); ?>" 
								class="isActive" type="checkbox" data-switchery data-size="small" 
								<?php 
										if(($item-> aktif)==1){

											echo "checked";
										}

								?>

							 /></td>
							<td>
								<button 
								data-url="<?php echo base_url("projeler/delete/$item->id"); ?>" 
								class="btn btn-sm btn-danger btn-outline remove-btn">
								<i class="fa fa-trash"></i> Sil
								</button>
								<a href="<?php echo base_url("projeler/update_form/$item->id"); ?>" class="btn btn-sm btn-info btn-outline"><i class="fa fa-pencil-square-o"></i> Düzenle</a>
								<a href="<?php echo base_url("projeler/image_form/$item->id"); ?>" class="btn btn-sm btn-dark btn-outline"><i class="fa fa-file-image-o"></i> Resimler</a>
							</td>
						</tr>
					<?php }?>
						</tbody>

					</table></div>
				<?php }?>
				</div><!-- .widget -->
			</div>
				</div>
				</div>