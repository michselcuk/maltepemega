<?php if(empty($item_images)) {?>
					<div class="alert alert-info text-center">
								<h5>KAYIT BULUNAMADI</h5>
								<p>Gösterilecek kayıt bulunamadı.</p>
</div>
<?php } else {?>


					<table class="table table-bordered table-striped table-hover">
						<thead>
							<th>Fotoğraf</th>
							<th>Fotoğraf Adı</th>
							<th>Durumu</th>
							<th>Kapak</th>
							<th>İşlem</th>
						</thead>
						<tbody class="sortable" data-url="<?php echo base_url("projeler/imageRankSetter"); ?>">
							<?php foreach($item_images as $image){ ?>

							<tr id="ord-<?php echo $image->id; ?>">
								<td class="w100"><img width="50px" src="<?php echo base_url("uploads/{$viewFolder}/$image->img_url"); ?>" alt="<?php echo $image->img_url; ?>" class="img-responsive"></td>
								<td><?php echo $image->img_url; ?></td>

								<td class="w100"><input 
								data-url="<?php echo base_url("projeler/imageIsActiveSetter/$image->id"); ?>" 
								class="isActive" type="checkbox" data-switchery data-size="small" 
								<?php 
										if(($image-> aktif)==1){
											echo "checked";
										}

								?>

							 /></td>
							 	<td class="w100"><input 
								data-url="<?php echo base_url("projeler/isCoverSetter/$image->id/$image->proje_id"); ?>" 
								class="isCover" type="checkbox" data-switchery data-size="small" data-color="#ff5b5b" 
								<?php 
										if(($image-> isCover)==1){
											echo "checked";
										}

								?>

							 /></td>
								<td class="w100"><button 
								data-url="<?php echo base_url("projeler/imageDelete/$image->id/$image->proje_id"); ?>" 
								class="btn btn-sm btn-danger btn-outline remove-btn btn-block">
								<i class="fa fa-trash"></i> Sil
								</button></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
<?php } ?>