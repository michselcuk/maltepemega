
<div class="row">
	<div class="col-md-12">

<div class="widget">
					<div class="widget-body">
						<form data-url="<?php echo base_url("projeler/refresh_image_list/$item->id") ?>" action="<?php echo base_url("projeler/image_upload/$item->id") ?>" id="dropzone"class="dropzone" data-plugin="dropzone" data-options="{ url: '<?php echo base_url("projeler/image_upload/$item->id") ?>'}">
							<div class="dz-message">
								<h3 class="m-h-lg">Yüklemek istediğiniz fotoğrafları buraya sürükleyiniz.</h3>
								<p class="m-b-lg text-muted">Yüklemek için fotoğraflarınızı sürükleyiniz yada tıklayınız.</p>
							</div>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
					</div>
	<div class="col-md-2"></div>
</div>

<div class="row">
	<div class="col-md-12"><h4><?php echo $item->adi . " kaydına ait fotoğraflar."; ?></h4>
	</div>
</div>

<div class="row">
	<div class="col-md-12">

<div class="widget">
					<div class="widget-body image_list_container">

				<?php $this->load->view("{$viewFolder}/{$subViewFolder}/render_elements/image_list_v"); ?>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
	</div>
	<div class="col-md-2"></div>
</div>