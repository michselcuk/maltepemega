<div class="row">
	<div class="col-md-12"><h4>Proje Ekle</h4>
	</div>
</div>
<p></p>
<div class="row">
<div class="col-md-2"></div>
	<div class="col-md-8">

<div class="widget">
					<div class="widget-body">
						<form action="<?php echo base_url("projeler/save"); ?>" method="POST">
							<div class="form-group">
								<label>Proje Adı</label>
								<input type="text" class="form-control" placeholder="Proje Adı" name="adi" required>
							</div>

							<div class="form-group">
								<label>Proje Yürütücüsü</label>
								<div class="radio radio-primary">
										<input type="radio"  name="ililce" value="il" required>
										<label for="radio-primary">İstanbul MEM</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" name="ililce" value="ilce" required>
										<label for="radio-primary">Maltepe MEM</label>
								</div>
							</div>
							<div class="form-group">
								<label>Proje Katılımcı Sayısı</label>
							<div class="col-sm-12">
								<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="Okul Sayısı" name="okul" required>
								</div>
								<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="Öğretmen Sayısı" name="ogretmen" required>
								</div>
								<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="Öğrenci Sayısı" name="ogrenci" required>
								</div>
							</div>
							</div><br>

							<div class="form-group">
								<label>Proje Açıklaması</label>
								<textarea class="m-0" data-plugin="summernote" data-options="{height: 250}" name="aciklama" required></textarea>
							</div>
							<button type="submit" class="btn btn-primary btn-md btn-outline">Kaydet</button>
							<a href="<?php echo base_url("projeler"); ?>" class="btn btn-danger btn-md btn-outline">İptal</a>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->



	
	</div>
	<div class="col-md-2"></div>
</div>
