	<div class="simple-page-wrap">
		<div class="simple-page-logo animated swing">
			<a href="index.html">
				<img src="http://maltepemega.com/seminer/assets/assets/images/megalogo.png" width="120px">

					<span style="color:#fff"></br>Maltepe Eğitim ve Gelişim Akademisi</span>

			</a>
		</div><!-- logo -->
		<div class="simple-page-form animated flipInY" id="login-form">
	<h4 class="form-title m-b-xl text-center">Panel Giriş Ekranı</h4>
	<form action="<?php echo base_url("userop/do_login")?>" method="post">
		<div class="form-group">
			<input id="sign-in-email" type="email" class="form-control" name= "user_email" placeholder="E-mail Adresi">
			<?php if(isset($form_error)) {?>
			<small class="pull-right input-form-error kirmizi"><?php echo form_error("user_email") ?></small>
			<?php } ?>
		</div>

		<div class="form-group">
			<input id="sign-in-password" type="password" class="form-control" name= "user_password" placeholder="Şifre">
			<?php if(isset($form_error)) {?>
			<small class="pull-right input-form-error kirmizi"><?php echo form_error("user_password") ?></small>
			<?php } ?>
		</div>
		<button type="submit" class="btn btn-primary">GİRİŞ YAP </button>
	</form>
</div><!-- #login-form -->

<div class="simple-page-footer">
</div><!-- .simple-page-footer -->


	</div>