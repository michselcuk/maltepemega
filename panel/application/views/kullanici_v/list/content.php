<div class="row">
	<div class="col-md-12"><h4>Kullanıcılar
	<a href="<?php echo base_url("kullanici/new_form")?>" class="btn btn-xs btn-primary btn-outline pull-right"><i class="fa fa-plus"></i> Kullanıcı Ekle</a></h4>


	</div>
</div>
<p></p>
<div class="row">

<div class="col-md-12">
				<div class="widget p-lg">

					<?php if(empty($items)) {?>
					<div class="alert alert-info text-center">
								<h5>KAYIT BULUNAMADI</h5>
								<p>Gösterilecek kayıt bulunamadı. Yeni kayıt eklemek için lütfen <a href="<?php echo base_url("kullanici/new_form")?>">tıklayınız. </a></p>
</div>
<?php } else {?>
					<div class="table-responsive">
					<table class="table table-hover table-bordered content-container">
						<thead>
							<th>Adı - Soyadı</th>
							<th>Kullanıcı Adı</th>
							<th>E-posta Adresi</th>
							<th class="w50">Durumu</th>
							<th class="w280">İşlemler</th>
						</thead>



						<tbody class="text-center">
		<?php foreach($items as $item)  {  ?>
							<tr id="ord-<?php echo $item->id; ?>">
							<td><?php echo $item-> adsoyad; ?></td>
							<td><?php echo $item-> kadi; ?></td>
							<td><?php echo $item-> email; ?></td>
							<td><input 
								data-url="<?php echo base_url("kullanici/isActiveSetter/$item->id"); ?>" 
								class="isActive" type="checkbox" data-switchery data-size="small" 
								<?php 
										if(($item-> aktif)==1){

											echo "checked";
										}

								?>

							 /></td>
							<td>
								<button 
								data-url="<?php echo base_url("kullanici/delete/$item->id"); ?>" 
								class="btn btn-sm btn-danger btn-outline remove-btn">
								<i class="fa fa-trash"></i> Sil
								</button>
								<a href="<?php echo base_url("kullanici/update_form/$item->id"); ?>" class="btn btn-sm btn-info btn-outline"><i class="fa fa-pencil-square-o"></i> Düzenle</a>
								<a href="<?php echo base_url("kullanici/update_password_form/$item->id"); ?>" class="btn btn-sm btn-purple btn-outline"><i class="fa fa-key"></i> Şifre Değiştir</a>
							</td>
						</tr>
					<?php }?>
						</tbody>

					</table></div>
				<?php }?>
				</div><!-- .widget -->
			</div>
				</div>
				</div>