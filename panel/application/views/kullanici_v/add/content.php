<div class="row">
	<div class="col-md-12"><h4>Yeni Kullanıcı Ekle</h4>
	</div>
</div>
<p></p>
<div class="row">
<div class="col-md-2"></div>
	<div class="col-md-8">

<div class="widget">
					<div class="widget-body">
						<form action="<?php echo base_url("kullanici/save"); ?>" method="POST">
							<div class="form-group">
								<label>Adı Soyadı</label>
								<input type="text" class="form-control" placeholder="Adı Soyadı" name="adsoyad" required value="<?php echo isset($form_error) ? set_value("adsoyad") :""; ?>">
								<?php if(isset($form_error)) {?>
									<small class="pull-right input-form-error"><?php echo form_error("adsoyad") ?></small>
								<?php } ?>
							</div>
							<div class="form-group">
								<label>Kullanıcı Adı</label>
								<input type="text" class="form-control" placeholder="Kullanıcı Adı" name="kadi" required value="<?php echo isset($form_error) ? set_value("kadi") :""; ?>">
								<?php if(isset($form_error)) {?>
									<small class="pull-right input-form-error"><?php echo form_error("kadi") ?></small>
								<?php } ?>
							</div>
							<div class="form-group">
								<label>E-mail Adresi</label>
								<input type="email" class="form-control" placeholder="E-mail Adresi" name="email" required value="<?php echo isset($form_error) ? set_value("email") :""; ?>">
								<?php if(isset($form_error)) {?>
									<small class="pull-right input-form-error"><?php echo form_error("email") ?></small>
								<?php } ?>
							</div>
							<div class="form-group">
								<label>Şifre</label>
								<input type="password" class="form-control" placeholder="Şifre" name="sifre" required value="<?php echo isset($form_error) ? set_value("sifre") :""; ?>">
								<?php if(isset($form_error)) {?>
									<small class="pull-right input-form-error"><?php echo form_error("sifre") ?></small>
								<?php } ?>
							</div>
							<div class="form-group">
								<label>Şifre Tekrarı</label>
								<input type="password" class="form-control" placeholder="Şifre Tekrarı" name="resifre" required value="<?php echo isset($form_error) ? set_value("resifre") :""; ?>">
								<?php if(isset($form_error)) {?>
									<small class="pull-right input-form-error"><?php echo form_error("resifre") ?></small>
								<?php } ?>
							</div>
							
							<button type="submit" class="btn btn-primary btn-md btn-outline">Kaydet</button>
							<a href="<?php echo base_url("kullanici"); ?>" class="btn btn-danger btn-md btn-outline">İptal</a>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->



	
	</div>
	<div class="col-md-2"></div>
</div>
