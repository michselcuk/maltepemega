<div class="row">
	<div class="col-md-12"><h4>Duyuru Ekle</h4>
	</div>
</div>
<p></p>
<div class="row">
<div class="col-md-2"></div>
	<div class="col-md-8">

<div class="widget">
					<div class="widget-body">
						<form action="<?php echo base_url("duyurular/save"); ?>" method="POST">
							<div class="form-group">
								<label>Duyuru Başlığı</label>
								<input type="text" class="form-control" placeholder="Duyuru Başlığı" name="baslik" required>
							</div>
							<div class="form-group">
								<label>Duyuru İçeriği</label>
								<textarea class="m-0" data-plugin="summernote" data-options="{height: 250}" name="aciklama" required></textarea>
							</div>
							<button type="submit" class="btn btn-primary btn-md btn-outline">Kaydet</button>
							<a href="<?php echo base_url("duyurular"); ?>" class="btn btn-danger btn-md btn-outline">İptal</a>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->



	
	</div>
	<div class="col-md-2"></div>
</div>
