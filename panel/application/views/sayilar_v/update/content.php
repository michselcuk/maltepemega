<div class="row">
	<div class="col-md-12"><h4>Proje sayısal verilerini düzenliyorsunuz</h4>
	</div>
</div>
<p></p>
<div class="row">
<div class="col-md-2"></div>
	<div class="col-md-8">

<div class="widget">
					<div class="widget-body">
						<form action="<?php echo base_url("sayilar/update/$item->id"); ?>" method="POST">
							<div class=row>
							<div class="form-group col-md-6">
								<label>Okul Sayısı</label>
								<input type="text" class="form-control" name="okul" required value="<?php echo $item->okul; ?>">
							</div>	
							<div class="form-group col-md-6">
								<label>Öğretmen Sayısı</label>
								<input type="text" class="form-control"  name="ogretmen" required value="<?php echo $item->ogretmen; ?>">
							</div>	
							</div>
							<div class=row>
							<div class="form-group col-md-6">
								<label>Öğrenci Sayısı</label>
								<input type="text" class="form-control" name="ogrenci" required value="<?php echo $item->ogrenci; ?>">
							</div>	
							<div class="form-group col-md-6">
								<label>Derslik Sayısı</label>
								<input type="text" class="form-control" name="derslik" required value="<?php echo $item->derslik; ?>">
							</div>	
							</div>


							
							

							<button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
							<a href="<?php echo base_url("sayilar"); ?>" class="btn btn-danger btn-md btn-outline">İptal</a>
						</form>
					</div><!-- .widget-body -->
				</div><!-- .widget -->
	</div>
	<div class="col-md-2"></div>
</div>
