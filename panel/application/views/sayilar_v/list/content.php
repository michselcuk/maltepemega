<div class="row">
	<div class="col-md-12"><h4>Proje İstatistiksel Verileri
	</h4>


	</div>
</div>
<p></p>
<div class="row">

<div class="col-md-12">
				<div class="widget p-lg">

					<?php if(empty($items)) {?>
					<div class="alert alert-info text-center">
								<h5>KAYIT BULUNAMADI</h5>
								<p>Gösterilecek kayıt bulunamadı. Yeni kayıt eklemek için lütfen <a href="<?php echo base_url("sayilar/new_form")?>">tıklayınız. </a></p>
</div>
<?php } else {?>
					<div class="table-responsive">
					<table class="table table-hover table-bordered content-container">
						<thead>
							<th>Okul Sayısı</th>
							<th>Öğretmen Sayısı</th>
							<th>Öğrenci Sayısı</th>
							<th>Derslik Sayısı</th>
							<th class="w160">İşlemler</th>
						</thead>

						<tbody>
		<?php foreach($items as $item)  {  ?>
							<tr id="ord-<?php echo $item->id; ?>" class="text-center">
							<td><?php echo $item-> okul; ?></td>
							<td><?php echo $item-> ogretmen; ?></td>
							<td><?php echo $item-> ogrenci; ?></td>
							<td><?php echo $item-> derslik; ?></td>
							<td>
								<a href="<?php echo base_url("sayilar/update_form/$item->id"); ?>" class="btn btn-sm btn-info btn-outline"><i class="fa fa-pencil-square-o"></i> Düzenle</a>
							</td>
						</tr>
					<?php }?>
						</tbody>

					</table></div>
				<?php }?>
				</div><!-- .widget -->
			</div>
				</div>
				</div>