<?php $user = get_active_user(); ?>

<aside id="menubar" class="menubar light">
  <div class="app-user">
    <div class="media">
      <div class="media-left">
        <div class="avatar avatar-md avatar-circle">
          <a href="javascript:void(0)"><img class="img-responsive" src="<?php echo base_url("assets");?>/assets/images/221.jpg" alt="avatar"/></a>
        </div><!-- .avatar -->
      </div>
      <div class="media-body">
        <div class="foldable">
          <h5><a href="javascript:void(0)" class="username"><?php echo $user->adsoyad?></a></h5>
          <ul>
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle usertitle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <small>İşlemler</small>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu animated flipInY">
                <li>
                  <a class="text-color" href="<?php echo base_url("kullanici/update_form/$user->id") ?>">
                    <span class="m-r-xs"><i class="fa fa-user"></i></span>
                    <span>Profil</span>
                  </a>
                </li>
                <li role="separator" class="divider"></li>
                <li>
                  <a class="text-color" href="<?php echo base_url("logout") ?>">
                    <span class="m-r-xs"><i class="fa fa-power-off"></i></span>
                    <span>Çıkış Yap</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- .media-body -->
    </div><!-- .media -->
  </div><!-- .app-user -->

  <div class="menubar-scroll">
    <div class="menubar-scroll-inner">
      <ul class="app-menu">
        <li >
          <a href="<?php echo base_url("");?>">
            <i class="menu-icon zmdi zmdi-home zmdi-hc-lg"></i>
            <span class="menu-text">Ana Sayfa</span>
          </a>
        </li>
         <li>
          <a href="<?php echo base_url("Slider"); ?>">
            <i class="menu-icon zmdi zmdi-image-o zmdi-hc-lg"></i>
            <span class="menu-text"> Slider</span>
          </a>
        </li>
         <li>
          <a href="<?php echo base_url("Projeler"); ?>">
            <i class="menu-icon zmdi zmdi-puzzle-piece zmdi-hc-lg"></i>
            <span class="menu-text">Projeler</span>
          </a>
        </li>
        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-layers zmdi-hc-lg"></i>
            <span class="menu-text">Mega</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu">
            <li><a href="<?php echo base_url("Ekip"); ?>"><span class="menu-text">Mega Ekibi</span></a></li>
            <li><a href="<?php echo base_url("Megatanitim"); ?>"><span class="menu-text">İçerik</span></a></li>
          </ul>
        </li>
        <li>
          <a href="<?php echo base_url("Duyurular"); ?>">
           <i class="menu-icon zmdi zmdi-notifications-none zmdi-hc-lg"></i>
            <span class="menu-text">Duyurular</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url("Sayilar"); ?>">
            <i class="menu-icon zmdi zmdi-n-1-square zmdi-hc-lg"></i>
            <span class="menu-text">Sayısal Veriler</span>
          </a>
        </li>
        

       
        
        <li>
          <a href="<?php echo base_url("Partner"); ?>">
            <i class="menu-icon zmdi zmdi-link zmdi-hc-lg"></i>
            <span class="menu-text">Linkler</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url("Kullanici"); ?>">
         <i class="menu-icon zmdi zmdi-account zmdi-hc-lg"></i>
            <span class="menu-text">Kullanıcılar</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url("Settings"); ?>">
            <i class="menu-icon zmdi zmdi-settings zmdi-hc-lg"></i>
            <span class="menu-text">Ayarlar</span>
          </a>
        </li>
        

      </ul><!-- .app-menu -->
    </div><!-- .menubar-scroll-inner -->
  </div><!-- .menubar-scroll -->
</aside>