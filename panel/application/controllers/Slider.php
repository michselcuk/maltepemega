<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {

public $viewFolder = "";
public function __construct()
{

	parent::__construct();
	if(!get_active_user()){
		redirect(base_url("login"));
	}
	
	$this->viewFolder = "slider_v";
	$this->load->model("slider_model");
	$this->load->model("slider_image_model");
}


public function index(){

		$viewData = new stdClass();
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "list";
		$viewData->item=$this->slider_model->get(
			array()
		);

		$viewData->item_images=$this->slider_image_model->get_all(
			array(
			), "sira ASC"
		);



		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
}
	public function imageDelete($id){

		$fileName=$this->slider_image_model->get(
			array(
				"id" => $id
			)
		);

		$delete=$this->slider_image_model->delete(array("id" => $id));
if($delete){
unlink("uploads/{$this->viewFolder}/$fileName->img_url");
$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde silindi.",
"type" => "success"
);


}else{

$alert=array(
"title" => "İşlem başarısız.",
"text" => "Silme sırasında bir hata oluştu.",
"type" => "error"
);


}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("slider"));
	}

public function imageIsActiveSetter($id){

	if($id){

		$isActive = ($this->input->post("data") === "true") ? 1 : 0;
		$this->slider_image_model->update(
			array(
				"id" => $id
			),
			array(
				"aktif" =>$isActive
			)
			

		);


	}

}

public function imageRankSetter(){

	$data = $this->input->post("data");
	parse_str($data, $order);
	$items = $order["ord"];
	foreach($items as $rank => $id){

		$this->slider_image_model->update(
			array(
				"id"  => $id,
				"sira !=" =>$rank
			),array(
				"sira" => $rank
			)

		);


	}
}

public function refresh_image_list(){


$viewData = new stdClass();
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "list";
		$viewData->item_images=$this->slider_image_model->get_all(
			array()
		);
	$render_html=$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/image_list_v",$viewData, true);
echo $render_html;
}

public function isCoverSetter($id){

	if($id){

		$isCover = ($this->input->post("data") === "true") ? 1 : 0;
		$this->slider_image_model->update(
			array(
				"id" => $id,
			),
			array(
				"isCover" =>$isCover
			)
			);

				$this->slider_image_model->update(
			array(
				"id !=" => $id,
			),
			array(
				"isCover" => 0
			)
		);


$viewData = new stdClass();
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "list";
		$viewData->item_images=$this->slider_image_model->get_all(
			array(
			), "sira ASC"
		);
	$render_html=$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/image_list_v",$viewData, true);
echo $render_html;


	}

}

public function image_upload(){
/*$file_name = convertToSEO(pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME)). "." . pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
$config["allowed_types"]="jpg|jpeg|png";
$config["upload_path"]="uploads/$this->viewFolder";
$config["file_name"]=$file_name;


$this->load->library("upload", $config);
$upload=$this->upload->do_upload("file");

if($upload){*/

 $sayi=rand(0,5000);
 	  $sayi1=rand(0,5000);
$file_name = convertToSEO(pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME)). "." . pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
$image_1920x700=upload_picture($_FILES["file"]["tmp_name"],"uploads/$this->viewFolder",1920,700,"$sayi$sayi1$file_name");
if($image_1920x700){



//	$uploaded_file = $this->upload->data("file_name");
	$this->slider_image_model->add(
		array(
			"img_url" =>"$sayi$sayi1$file_name",
			"sira" => 0,
			"aktif" => 1,
			"isCover" => 0,
		)
	);


}else{

echo "no";
}
}









public function update_form($id){
		$viewData = new stdClass();

$item=$this->slider_model->get(
array(
	"id" => $id,
)

);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "update";
		$viewData->item=$item;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);

	}



	public function update($id){

$update=$this->slider_image_model->update(
array(
"id" => $id
),
array(
      "slogan" => $this->input->post("slogan"),
      "aciklama" => $this->input->post("aciklama"),
)
);
if($update){

$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde güncellendi.",
"type" => "success"
);


}else{

$alert=array(
"title" => "İşlem başarısız.",
"text" => "Güncelleme sırasında bir hata oluştu.",
"type" => "error"
);


}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("slider"));
}


















}
