<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends CI_Controller {

public $viewFolder = "";
public function __construct()
{

	parent::__construct();
	if(!get_active_user()){
		redirect(base_url("login"));
	}
	
	$this->viewFolder = "partner_v";
	$this->load->model("partner_model");
}



	public function index(){
		$viewData = new stdClass();
		$items = $this->partner_model->get_all(

			array(), "sira ASC"
		);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "list";
		$viewData->items = $items;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
	}


	public function new_form(){
		$viewData = new stdClass();
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "add";
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);

	}


	public function save(){

if($_FILES["img_url"]["name"] == ""){


$alert=array(
"title" => "İşlem başarısız.",
"text" => "Lütfen bir fotoğraf seçiniz.",
"type" => "error"
);
$this->session->set_flashdata("alert", $alert);
redirect(base_url("partner/new_form"));

}else{

	  $sayi=rand(0,5000);
 	  $sayi1=rand(0,5000);
$file_name = convertToSEO(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME)). "." . pathinfo($_FILES["img_url"]["name"],PATHINFO_EXTENSION);
$image_167x107=upload_picture($_FILES["img_url"]["tmp_name"],"uploads/$this->viewFolder",167,107,"$sayi$sayi1$file_name");
if($image_167x107){
		$data=array(
      "ad" => $this->input->post("ad"),
      "link" => $this->input->post("link"),
      "aktif" => "1",
      "sira" => "0",
      "img_url" => "$sayi$sayi1$file_name" ,
);



$insert=$this->partner_model->add($data);
if($insert){

$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde eklendi.",
"type" => "success"
);


}else{

$alert=array(
"title" => "İşlem başarısız.",
"text" => "Kayıt ekleme sırasında bir hata oluştu.",
"type" => "error"
);

}

$this->session->set_flashdata("alert", $alert);
redirect(base_url("partner"));
	}}

}

public function update_form($id){
		$viewData = new stdClass();
$item=$this->partner_model->get(
array(
	"id" => $id,
)

);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "update";
		$viewData->item=$item;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
	}



public function update($id){


if($_FILES["img_url"]["name"] == ""){
$update=$this->partner_model->update(
array(
"id" => $id
),
array(
      "ad" => $this->input->post("ad"),
      "link" => $this->input->post("link"),
)
);
if($update){
$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde güncellendi.",
"type" => "success"
);
}else{
$alert=array(
"title" => "İşlem başarısız.",
"text" => "Güncelleme sırasında bir hata oluştu.",
"type" => "error"
);
}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("partner"));
}else{



	  $sayi=rand(0,5000);
 	  $sayi1=rand(0,5000);
$file_name = convertToSEO(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME)). "." . pathinfo($_FILES["img_url"]["name"],PATHINFO_EXTENSION);
$image_167x107=upload_picture($_FILES["img_url"]["tmp_name"],"uploads/$this->viewFolder",167,107,"$sayi$sayi1$file_name");

$update=$this->partner_model->update(
array(
"id" => $id
),
array(
       "ad" => $this->input->post("ad"),
      "link" => $this->input->post("link"),
        "img_url" => "$sayi$sayi1$file_name" ,
)
);
if($update){
$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde güncellendi.",
"type" => "success"
);
}else{
$alert=array(
"title" => "İşlem başarısız.",
"text" => "Güncelleme sırasında bir hata oluştu.",
"type" => "error"
);
}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("partner"));

}

}


public function delete($id){
$delete=$this->partner_model->delete(array("id" => $id));
if($delete){

$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde silindi.",
"type" => "success"
);


}else{

$alert=array(
"title" => "İşlem başarısız.",
"text" => "Silme sırasında bir hata oluştu.",
"type" => "error"
);


}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("partner"));
	}




public function isActiveSetter($id){

	if($id){

		$isActive = ($this->input->post("data") === "true") ? 1 : 0;
		$this->partner_model->update(
			array(
				"id" => $id
			),
			array(
				"aktif" =>$isActive
			)
			

		);


	}

}


public function rankSetter(){

	$data = $this->input->post("data");
	parse_str($data, $order);
	$items = $order["ord"];
	foreach($items as $rank => $id){

		$this->partner_model->update(
			array(
				"id"  => $id,
				"sira !=" =>$rank
			),array(
				"sira" => $rank
			)

		);


	}
}



}
