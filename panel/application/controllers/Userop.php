<?php
class Userop extends CI_Controller{
	public $viewFolder="";
	public function __construct()
	{

		parent::__construct();
		$this->viewFolder = "kullanici_v";
		$this->load->model("kullanici_model");
	}

	public function login(){
		if(get_active_user()){
			redirect(base_url());
		}

		$viewData = new stdClass();
		$this->load->library("form_validation");
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "login";
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
	}

	public function do_login(){

		if(get_active_user()){
			redirect(base_url());
		}
		
		$this->load->library("form_validation");
		$this->form_validation->set_rules("user_email", "E-mail Adresi", "required|trim");
		$this->form_validation->set_rules("user_password", "Şifre", "required|trim");


$this->form_validation->set_message(
array(
		"required" => " {field} alanı doldurulmalıdır.",
)

);

if($validate=$this->form_validation->run() == FALSE){
	$viewData = new stdClass();
	$viewData->viewFolder=$this->viewFolder;
	$viewData->subViewFolder="login";
	$viewData->form_error=true;
	$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);


}else{

	$user=$this->kullanici_model->get(
		array(
			"email"    => $this->input->post("user_email"),
			"sifre"    => md5($this->input->post("user_password")),
			"aktif"    => 1
		)
	);

 if($user){
 		 		$alert=array(
		"title" => "İşlem başarılı.",
		"text" => "Hoşgeldiniz $user->adsoyad !",
		"type" => "success"
					);

 		$this->session->set_userdata("userpanel",$user);
 		$this->session->set_flashdata("alert", $alert);
 		redirect(base_url());




 }else{
		$alert=array(
		"title" => "İşlem başarısız.",
		"text" => "Lütfen E-mail adresinizi ve şifrenizi kontrol ediniz!",
		"type" => "error"
					);
 		$this->session->set_flashdata("alert", $alert);
 		redirect(base_url("login"));
 }


}
	}



	public function logout(){
		$this->session->unset_userdata("userpanel");
		redirect(base_url("login"));
}
}