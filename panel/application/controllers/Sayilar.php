<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sayilar extends CI_Controller {

public $viewFolder = "";
public function __construct()
{

	parent::__construct();
	if(!get_active_user()){
		redirect(base_url("login"));
	}
	
	$this->viewFolder = "sayilar_v";
	$this->load->model("sayilar_model");
}



	public function index(){
		$viewData = new stdClass();
		$items = $this->sayilar_model->get_all(

			array()
		);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "list";
		$viewData->items = $items;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
	}


	public function new_form(){
		$viewData = new stdClass();
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "add";
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);

	}

public function update_form($id){
		$viewData = new stdClass();
$item=$this->sayilar_model->get(
array(
	"id" => $id,
)

);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "update";
		$viewData->item=$item;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
	}



	public function update($id){


$update=$this->sayilar_model->update(
array(
"id" => $id
),
array(
      "okul" => $this->input->post("okul"),
      "ogretmen" => $this->input->post("ogretmen"),
      "ogrenci" => $this->input->post("ogrenci"),
      "derslik"      => $this->input->post("derslik"),
)
);
if($update){
$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde güncellendi.",
"type" => "success"
);
}else{
$alert=array(
"title" => "İşlem başarısız.",
"text" => "Güncelleme sırasında bir hata oluştu.",
"type" => "error"
);
}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("sayilar"));

}



}
