<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller {

public $viewFolder = "";
public function __construct()
{

	parent::__construct();
	$this->viewFolder = "panel_v";
	if(!get_active_user()){
		redirect(base_url("login"));
	}


}



	public function index()
	{
		$this->load->view("{$this->viewFolder}/index");
	}

	public function test()
	{
		echo "test";
	}
	
}
