<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kullanici extends CI_Controller {

public $viewFolder = "";
public function __construct()
{

	parent::__construct();
	if(!get_active_user()){
		redirect(base_url("login"));
	}
	
	$this->viewFolder = "kullanici_v";
	$this->load->model("kullanici_model");
}

	public function index(){
		$viewData = new stdClass();
		$items = $this->kullanici_model->get_all(

			array()
		);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "list";
		$viewData->items = $items;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
	}


	public function new_form(){
		$viewData = new stdClass();
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "add";
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);

	}


	public function save(){

$this->load->library("form_validation");
$this->form_validation->set_rules("kadi", "Kullanıcı Adı", "required|trim|is_unique[kullanici.kadi]");
$this->form_validation->set_rules("adsoyad", "Adı Soyadı", "required|trim");
$this->form_validation->set_rules("email", "E-mail Adresi", "required|trim|valid_email|is_unique[kullanici.email]");
$this->form_validation->set_rules("sifre", "Şifre", "required|trim");
$this->form_validation->set_rules("resifre", "Şifre Tekrarı", "required|trim|matches[sifre]");

$this->form_validation->set_message(
array(
		"required" => " {field} alanı doldurulmalıdır.",
		"valid_email" => " Lütfen geçerli bir e-mail adresi giriniz.",
		"is_unique" => " {field} daha önce kullanılmış.",
		"matches" => " Şifreler birbiri ile uyuşmuyor."
)

);

$validate=$this->form_validation->run();

if($validate){
	$insert = $this->kullanici_model->add(
		array(
			"kadi" => $this->input->post("kadi"),
			"adsoyad" => $this->input->post("adsoyad"),
			"email" => $this->input->post("email"),
			"sifre" => md5($this->input->post("sifre")),
			"aktif" => 1,
		)
	);
if($insert){

$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde eklendi.",
"type" => "success"
);


}else{

$alert=array(
"title" => "İşlem başarısız.",
"text" => "Kayıt ekleme sırasında bir hata oluştu.",
"type" => "error"
);

}
$this->session->set_flashdata("alert",$alert);
redirect(base_url("kullanici"));

}else {

$viewData = new stdClass();
$viewData->viewFolder=$this->viewFolder;
$viewData->subViewFolder="add";
$viewData->form_error=true;
$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

}

}

public function update_password_form($id){
		$viewData = new stdClass();
$item=$this->kullanici_model->get(
array(
	"id" => $id,
)

);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "password";
		$viewData->item=$item;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
	}






public function update_form($id){
		$viewData = new stdClass();
$item=$this->kullanici_model->get(
array(
	"id" => $id,
)

);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "update";
		$viewData->item=$item;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
	}


	public function update_password($id){

$this->load->library("form_validation");
$this->form_validation->set_rules("sifre", "Şifre", "required|trim");
$this->form_validation->set_rules("resifre", "Şifre Tekrarı", "required|trim|matches[sifre]");

$this->form_validation->set_message(
array(
		"required" => " {field} alanı doldurulmalıdır.",
		"matches" => " Şifreler birbiri ile uyuşmuyor."
)

);

$validate=$this->form_validation->run();

if($validate){
	$update=$this->kullanici_model->update(
array(
"id" => $id
),
array(
      "sifre" => md5($this->input->post("sifre")),
)
);
if($update){
$alert=array(
"title" => "İşlem başarılı.",
"text" => "Şifreniz başarılı bir şekilde güncellendi.",
"type" => "success"
);
}else{
$alert=array(
"title" => "İşlem başarısız.",
"text" => "Şifre güncelleme sırasında bir hata oluştu.",
"type" => "error"
);
}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("kullanici"));

}else {

$viewData = new stdClass();
$viewData->viewFolder=$this->viewFolder;
$viewData->subViewFolder="password";
$viewData->form_error=true;
$viewData->item=$this->kullanici_model->get(
array(
"id" => $id,
)
);


$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

}

}



	public function update($id){

$this->load->library("form_validation");
$oldUser = $this->kullanici_model->get(
array(
	"id" => $id
)
);

if($oldUser->kadi != $this->input->post("kadi")){
$this->form_validation->set_rules("kadi", "Kullanıcı Adı", "required|trim|is_unique[kullanici.kadi]");
}
if($oldUser->email != $this->input->post("email")){
$this->form_validation->set_rules("email", "E-mail Adresi", "required|trim|valid_email|is_unique[kullanici.email]");

}
$this->form_validation->set_rules("adsoyad", "Adı Soyadı", "required|trim");


$this->form_validation->set_message(
array(
		"required" => " {field} alanı doldurulmalıdır.",
		"valid_email" => " Lütfen geçerli bir e-mail adresi giriniz.",
		"is_unique" => " {field} daha önce kullanılmış."
)

);

$validate=$this->form_validation->run();

if($validate){
	$update=$this->kullanici_model->update(
array(
"id" => $id
),
array(
      "kadi" => $this->input->post("kadi"),
	  "adsoyad" => $this->input->post("adsoyad"),
	  "email" => $this->input->post("email"),
)
);
if($update){
$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde güncellendi.",
"type" => "success"
);
}else{
$alert=array(
"title" => "İşlem başarısız.",
"text" => "Güncelleme sırasında bir hata oluştu.",
"type" => "error"
);
}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("kullanici"));

}else {

$viewData = new stdClass();
$viewData->viewFolder=$this->viewFolder;
$viewData->subViewFolder="update";
$viewData->form_error=true;
$viewData->item=$this->kullanici_model->get(
array(
"id" => $id,
)
);


$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

}

}






public function delete($id){
$delete=$this->kullanici_model->delete(array("id" => $id));
if($delete){

$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde silindi.",
"type" => "success"
);


}else{

$alert=array(
"title" => "İşlem başarısız.",
"text" => "Silme sırasında bir hata oluştu.",
"type" => "error"
);


}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("kullanici"));
	}




public function isActiveSetter($id){

	if($id){

		$isActive = ($this->input->post("data") === "true") ? 1 : 0;
		$this->kullanici_model->update(
			array(
				"id" => $id
			),
			array(
				"aktif" =>$isActive
			)
			

		);


	}

}

}
