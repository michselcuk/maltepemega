<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Duyurular extends CI_Controller {

public $viewFolder = "";
public function __construct()
{

	parent::__construct();
	if(!get_active_user()){
		redirect(base_url("login"));
	}
	
	$this->viewFolder = "duyurular_v";
	$this->load->model("duyurular_model");
	$this->load->model("duyurular_image_model");
	$this->load->helper("text");
}



	public function index(){
		$viewData = new stdClass();
		$items = $this->duyurular_model->get_all(

			array(), "sira ASC"
		);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "list";
		$viewData->items = $items;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
	}


	public function new_form(){
		$viewData = new stdClass();
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "add";
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);

	}


	public function save(){


$insert=$this->duyurular_model->add(
array(
      "baslik" => $this->input->post("baslik"),
      "aciklama" => $this->input->post("aciklama"),
      "url"      => convertToSEO($this->input->post("baslik")),
      "aktif" => "1",
      "sira" => "0",
)
);
if($insert){

$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde eklendi.",
"type" => "success"
);


}else{

$alert=array(
"title" => "İşlem başarısız.",
"text" => "Kayıt ekleme sırasında bir hata oluştu.",
"type" => "error"
);

}

$this->session->set_flashdata("alert", $alert);
redirect(base_url("duyurular"));
	}

public function update_form($id){
		$viewData = new stdClass();

$item=$this->duyurular_model->get(
array(
	"id" => $id,
)

);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "update";
		$viewData->item=$item;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);

	}



	public function update($id){

$update=$this->duyurular_model->update(
array(
"id" => $id
),
array(
 		"baslik" => $this->input->post("baslik"),
     	"aciklama" => $this->input->post("aciklama"),
     	"url"      => convertToSEO($this->input->post("baslik")),
)
);
if($update){

$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde güncellendi.",
"type" => "success"
);


}else{

$alert=array(
"title" => "İşlem başarısız.",
"text" => "Güncelleme sırasında bir hata oluştu.",
"type" => "error"
);


}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("duyurular"));
}



	public function delete($id){
		$delete=$this->duyurular_model->delete(array("id" => $id));
if($delete){

$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde silindi.",
"type" => "success"
);


}else{

$alert=array(
"title" => "İşlem başarısız.",
"text" => "Silme sırasında bir hata oluştu.",
"type" => "error"
);


}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("duyurular"));
	}


	public function imageDelete($id,$parent_id){

		$fileName=$this->duyurular_image_model->get(
			array(
				"id" => $id
			)
		);

		$delete=$this->duyurular_image_model->delete(array("id" => $id));
/*if($delete){
unlink("uploads/{$this->viewFolder}/$fileName->img_url");
redirect(base_url("duyurular/image_form/$parent_id"));

}else{
redirect(base_url("duyurular/image_form/$parent_id"));

}*/


if($delete){
unlink("uploads/{$this->viewFolder}/$fileName->img_url");
$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde silindi.",
"type" => "success"
);


}else{

$alert=array(
"title" => "İşlem başarısız.",
"text" => "Silme sırasında bir hata oluştu.",
"type" => "error"
);


}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("duyurular/image_form/$parent_id"));





	}



public function isActiveSetter($id){

	if($id){

		$isActive = ($this->input->post("data") === "true") ? 1 : 0;
		$this->duyurular_model->update(
			array(
				"id" => $id
			),
			array(
				"aktif" =>$isActive
			)
			

		);


	}

}

public function imageIsActiveSetter($id){

	if($id){

		$isActive = ($this->input->post("data") === "true") ? 1 : 0;
		$this->duyurular_image_model->update(
			array(
				"id" => $id
			),
			array(
				"aktif" =>$isActive
			)
			

		);


	}

}



public function rankSetter(){

	$data = $this->input->post("data");
	parse_str($data, $order);
	$items = $order["ord"];
	foreach($items as $rank => $id){

		$this->duyurular_model->update(
			array(
				"id"  => $id,
				"sira !=" =>$rank
			),array(
				"sira" => $rank
			)

		);


	}
}


public function imageRankSetter(){

	$data = $this->input->post("data");
	parse_str($data, $order);
	$items = $order["ord"];
	foreach($items as $rank => $id){

		$this->duyurular_image_model->update(
			array(
				"id"  => $id,
				"sira !=" =>$rank
			),array(
				"sira" => $rank
			)

		);


	}
}

public function image_form($id){

		$viewData = new stdClass();
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "image";
		$viewData->item=$this->duyurular_model->get(
			array(
				"id" => $id
			)
		);

		$viewData->item_images=$this->duyurular_image_model->get_all(
			array(
				"duyuru_id" => $id
			),  "sira ASC"
		);



		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
}



public function image_upload($id){
/*$file_name = convertToSEO(pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME)). "." . pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
$config["allowed_types"]="jpg|jpeg|png";
$config["upload_path"]="uploads/$this->viewFolder";
$config["file_name"]=$file_name;


$this->load->library("upload", $config);
$upload=$this->upload->do_upload("file");

if($upload){
	$uploaded_file = $this->upload->data("file_name");*/
	 $sayi=rand(0,5000);
 	  $sayi1=rand(0,5000);
$file_name = convertToSEO(pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME)). "." . pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
$image_700x400=upload_picture($_FILES["file"]["tmp_name"],"uploads/$this->viewFolder",700,400,"$sayi$sayi1$file_name");
if($image_700x400){
	$this->duyurular_image_model->add(
		array(
			"img_url" =>"$sayi$sayi1$file_name",
			"sira" => 0,
			"aktif" => 1,
			"isCover" => 0,
			"duyuru_id" => $id
		)
	);


}else{

echo "no";
}


}



public function refresh_image_list($id){


$viewData = new stdClass();
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "image";
		$viewData->item_images=$this->duyurular_image_model->get_all(
			array(
				"duyuru_id" => $id
			)
		);
	$render_html=$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/image_list_v",$viewData, true);
echo $render_html;
}


public function isCoverSetter($id,$parent_id){

	if($id && $parent_id){

		$isCover = ($this->input->post("data") === "true") ? 1 : 0;
		$this->duyurular_image_model->update(
			array(
				"id" => $id,
				"duyuru_id" => $parent_id
			),
			array(
				"isCover" =>$isCover
			)
			);

				$this->duyurular_image_model->update(
			array(
				"id !=" => $id,
				"duyuru_id" => $parent_id
			),
			array(
				"isCover" => 0
			)
		);


$viewData = new stdClass();
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "image";
		$viewData->item_images=$this->duyurular_image_model->get_all(
			array(
				"duyuru_id" => $parent_id
			), "sira ASC"
		);
	$render_html=$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/image_list_v",$viewData, true);
echo $render_html;


	}

}







}
