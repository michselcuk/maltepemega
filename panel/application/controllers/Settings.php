<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

public $viewFolder = "";
public function __construct()
{

	parent::__construct();
	if(!get_active_user()){
		redirect(base_url("login"));
	}
	
	$this->viewFolder = "settings_v";
	$this->load->model("settings_model");
}



	public function index(){
		$viewData = new stdClass();
		$items = $this->settings_model->get_all(

			array()
		);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "list";
		$viewData->items = $items;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
	}


	public function new_form(){
		$viewData = new stdClass();
		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "add";
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);

	}

public function update_form($id){
		$viewData = new stdClass();
$item=$this->settings_model->get(
array(
	"id" => $id,
)

);

		$viewData->viewFolder = $this->viewFolder;
		$viewData->subViewFolder = "update";
		$viewData->item=$item;
		$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index",$viewData);
	}



public function update($id){


if($_FILES["logo"]["name"] == ""){
$update=$this->settings_model->update(
array(
"id" => $id
),
array(
      "title" => $this->input->post("title"),
      "keywords" => $this->input->post("keywords"),
      "author" => $this->input->post("author"),
      "footer" => $this->input->post("footer"),
      "description" => $this->input->post("description"),
      "telefon" => $this->input->post("telefon"),
      "mail" => $this->input->post("mail"),
      "adres" => $this->input->post("adres"),
      "twitter" => $this->input->post("twitter"),
      "facebook" => $this->input->post("facebook"),
      "instagram" => $this->input->post("instagram"),
)
);
if($update){
$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde güncellendi.",
"type" => "success"
);
}else{
$alert=array(
"title" => "İşlem başarısız.",
"text" => "Güncelleme sırasında bir hata oluştu.",
"type" => "error"
);
}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("settings"));
}else{


	  $sayi=rand(0,5000);
 	  $sayi1=rand(0,5000);
$file_name = convertToSEO(pathinfo($_FILES["logo"]["name"], PATHINFO_FILENAME)). "." . pathinfo($_FILES["logo"]["name"],PATHINFO_EXTENSION);
$image_100x100=upload_picture($_FILES["logo"]["tmp_name"],"uploads/$this->viewFolder",100,100,"$sayi$sayi1$file_name");

$update=$this->settings_model->update(
array(
"id" => $id
),
array(
      "title" => $this->input->post("title"),
      "keywords" => $this->input->post("keywords"),
      "author" => $this->input->post("author"),
      "footer" => $this->input->post("footer"),
      "description" => $this->input->post("description"),
      "telefon" => $this->input->post("telefon"),
      "mail" => $this->input->post("mail"),
      "adres" => $this->input->post("adres"),
      "twitter" => $this->input->post("twitter"),
      "facebook" => $this->input->post("facebook"),
      "instagram" => $this->input->post("instagram"),
      "logo" => "$sayi$sayi1$file_name"
)
);
if($update){
$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde güncellendi.",
"type" => "success"
);
}else{
$alert=array(
"title" => "İşlem başarısız.",
"text" => "Güncelleme sırasında bir hata oluştu.",
"type" => "error"
);
}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("settings"));

}

}


public function delete($id){
$delete=$this->settings_model->delete(array("id" => $id));
if($delete){

$alert=array(
"title" => "İşlem başarılı.",
"text" => "Kayıt başarılı bir şekilde silindi.",
"type" => "success"
);


}else{

$alert=array(
"title" => "İşlem başarısız.",
"text" => "Silme sırasında bir hata oluştu.",
"type" => "error"
);


}
$this->session->set_flashdata("alert", $alert);
redirect(base_url("settings"));
	}




public function isActiveSetter($id){

	if($id){

		$isActive = ($this->input->post("data") === "true") ? 1 : 0;
		$this->settings_model->update(
			array(
				"id" => $id
			),
			array(
				"aktif" =>$isActive
			)
			

		);


	}

}


public function rankSetter(){

	$data = $this->input->post("data");
	parse_str($data, $order);
	$items = $order["ord"];
	foreach($items as $rank => $id){

		$this->settings_model->update(
			array(
				"id"  => $id,
				"sira !=" =>$rank
			),array(
				"sira" => $rank
			)

		);


	}
}



}
