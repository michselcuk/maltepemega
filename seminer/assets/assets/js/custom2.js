$(document).ready(function () {

    $(".sortable").sortable();
    //image delete
    $(".content-container, .image_list_container").on('click', '.remove-btn', function () {
        let $data_url=$(this).data("url");

        Swal.fire({
            title: 'Kayıt Silinecektir',
            text: "Bu işlem geri alınamaz",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Evet Sil',
            cancelButtonText:'İptal'
        }).then((result) => {
            if (result.value) {

                window.location.href = $data_url;

            }
        });
    });
    //image active
    $(".content-container, .image_list_container").on('change', '.isActive', function () {
        let $data = $(this).prop("checked");
        let $data_url = $(this).data("url");
        // alert($data);
        // alert($data_url);
        if (typeof $data !== "unsingned" && typeof $data_url !== "unsigned"){
            $.post($data_url,{data:$data},function (response) {
                Swal.fire({
                    type: 'success',
                   title: 'Onay işlemi yapıldı',
                   showConfirmButton: false,
                   timer: 1000
               })
           })
       }

    });
    //okul onay
    $(".content-container").on('change', '.school_confirm', function () {
        let $data = $(this).prop("checked");
        let $data_url = $(this).data("url");
        // alert($data);
        if (typeof $data !== "unsingned" && typeof $data_url !== "unsigned"){
            $.post($data_url,{data:$data},function (response) {
                Swal.fire({
                    type: 'success',
                    title: 'Onay işlemi yapıldı',
                    showConfirmButton: false,
                    timer: 1000
                })
            })
        }

    });
    //yayınlama
    $(".content-container").on('change', '.publish', function () {
        let $data = $(this).prop("checked");
        let $data_url = $(this).data("url");
        // alert($data);
        if (typeof $data !== "unsingned" && typeof $data_url !== "unsigned"){
            $.post($data_url,{data:$data},function (response) {
                Swal.fire({
                    type: 'success',
                    title: 'Yayınlama onay işlemi yapıldı',
                    showConfirmButton: false,
                    timer: 1000
                })
            })
        }

    });
    //admin onay
    $(".content-container").on('change', '.admin_confirm', function () {
        let $data = $(this).prop("checked");
        let $data_url = $(this).data("url");
        // alert($data);
        if (typeof $data !== "unsingned" && typeof $data_url !== "unsigned"){
            $.post($data_url,{data:$data},function (response) {
                Swal.fire({
                    type: 'success',
                    title: 'Onay işlemi yapıldı',
                    showConfirmButton: false,
                    timer: 1000
                })
            })
        }

    });
    //image cover
    $(".image_list_container").on('change', '.isCover', function(){
        let $data = $(this).prop("checked");
        let $data_url = $(this).data("url");
        // alert($data);
        // alert($data_url);
        if (typeof $data !== "unsingned" && typeof $data_url !== "unsigned"){
            $.post($data_url,{data:$data},function (response) {
                $(".image_list_container").html(response);
                $('[data-switchery]').each(function(){
                    let $this = $(this),
                        color = $this.attr('data-color') || '#188ae2',
                        jackColor = $this.attr('data-jackColor') || '#ffffff',
                        size = $this.attr('data-size') || 'default'

                    new Switchery(this, {
                        color: color,
                        size: size,
                        jackColor: jackColor
                    });
                });

                Swal.fire({
                    type: 'success',
                   title: 'Onay işlemi yapıldı',
                   showConfirmButton: false,
                   timer: 1000
               });
                $(".sortable").sortable();
           })
       }

    });
    //image list
    $(".content-container, .image_list_container").on("sortupdate", '.sortable',  function(event, ui){
        let $data = $(this).sortable("serialize");

        let $data_url = $(this).data("url");
        $.post($data_url,{data : $data}, function (response) {
            // console.log(response);
        });

    });

    //image upload
    let uploadSelection = Dropzone.forElement("#dropzone");
    uploadSelection.on("complete",function (file) {

        let $data_url = $("#dropzone").data("url");
        // console.log($data_url);
        $.post($data_url, {}, function (response) {
            console.log(response);
            $(".image_list_container").html(response);
            $('[data-switchery]').each(function(){
                let $this = $(this),
                    color = $this.attr('data-color') || '#188ae2',
                    jackColor = $this.attr('data-jackColor') || '#ffffff',
                    size = $this.attr('data-size') || 'default'

                new Switchery(this, {
                    color: color,
                    size: size,
                    jackColor: jackColor
                });
            });
            $(".sortable").sortable();
        });
    });

});