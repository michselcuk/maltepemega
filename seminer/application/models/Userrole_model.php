<?php

class Userrole_model extends CI_Model {

    public $tableName = "userroles";

    public function __construct()
    {
        parent::__construct();
    }

    /* Tablodaki tek bir kaydı göster */
    public function get($where = array()) {
        return $this->db->where($where)->get($this->tableName)->row();
    }

    /* Tablodaki tüm kayıtları göster */
    public function get_all($where=array(),$order="id ASC") {
       return $this->db->where($where)->order_by($order)->get($this->tableName)->result();
    }

    /* Tabloya yeni bir kayıt ekle */
    public function add($data = array()) {
        return $this->db->insert($this->tableName, $data);
    }

    /* Kaydı Güncelle */
    public function update($where = array(), $data = array()) {
        return $this->db->where($where)->update($this->tableName, $data);
    }

    /* Kaydı Sil */
    public function delete($where = array()) {
        return $this->db->where($where)->delete($this->tableName);
    }
}