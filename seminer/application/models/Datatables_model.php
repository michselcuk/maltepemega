<?php

class Datatables_model extends CI_Model{
// teachers
	public function select_get(){

		$this->db->select('users.id as userid,fullname,phone,school_name,users.isActive as isActive');
        $this->db->from('users');
        $this->db->join('schools', 'schools.id=users.schoolid');
        $this->db->where('users.isActive',1);
        $this->db->like("school_name");
        $this->db->order_by("fullname","asc");
        return $this->db->get();

	}

    // teachers
    public function select_get_school($schoolid){

        $this->db->select('users.id as userid,fullname,phone,school_name,users.isActive as isActive');
        $this->db->from('users');
        $this->db->join('schools', 'schools.id=users.schoolid');
        $this->db->where('users.isActive',1);
        $this->db->where('schools.id',$schoolid);
        $this->db->where('users.userrole',2);
        $this->db->like("school_name");
        $this->db->order_by("fullname","asc");
        return $this->db->get();

    }

// courseteachers
	public function select_get2(){

		$this->db->select('id,fullname,phone,isActive');
        $this->db->from('courseteacher');
        $this->db->where('isActive',1);
        $this->db->order_by("fullname","asc");
        return $this->db->get();

	}
}