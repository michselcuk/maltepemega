<?php
    class Project_model extends CI_Model{
        //tablo adı tanımla
        public $tableName = "projects";

        public function __construct()
        {
            parent::__construct();
        }
        //tüm projeleri getir
        public function get_all($where=array()){
            return $this->db->where($where)->get($this->tableName)->result();
        }
        //tel veri getir
        public function get($where=array()){
            return $this->db->where($where)->get($this->tableName)->row();
        }
        //ekle
        public function add($data = array() ){
            return $this->db->insert($this->tableName, $data);
        }
        //sil
        public function delete($where = array()){
            return $this->db->where($where)->delete($this->tableName);
        }
        //güncelle
        public function update($where = array(),$data = array()){
            return $this->db->where($where)->update($this->tableName,$data);
        }
        //tüm projelerden kullanıcılar ile eşleşenleri ve projelerden okul ile eşleşenleri getir.
        public function projects_users_schools($where = array(),$order = "id ASC"){
            $this->db->select("projects.*, users.fullname,schools.school_name AS school_name")
                ->from("projects")
                ->join("users","projects.user_id = users.id")
                ->join("schools","projects.school_id=schools.id");
            return $this->db->where($where)->order_by($order)->get()->result();
        }

        public function project_user_school($where = array())
        {
            $this->db->select("projects.*, users.fullname,schools.school_name AS school_name")
                ->from("projects")
                ->join("users","projects.user_id = users.id")
                ->join("schools","projects.school_id=schools.id");
            return $this->db->where($where)->get()->row();
        }
        //giriş yapan kullanıcının okul bilgileri
        public function user_school_info($where = array())
        {
            $school_info = $this->db->select("*")->from("schools")->where($where)->get()->row();
            return $school_info;

        }
        //giriş yapan kullancı bilgileri
        public function user_info()
        {
            $user = get_active_user();
            return $user;

        }
        //istenen kullanıcıya ait projeleri getirir
        public function projects_user($where = array(), $order = "id ASC")
        {
            return $this->db->select("*")->where($where)->order_by($order)->get($this->tableName)->result();
        }

        public function project_extras($where=array())
        {
            return $this->db->select("*")->from("project_extras")->where($where)->get()->result();

        }
//SELECT * FROM `project_extras`
// JOIN project_extras_images ON (project_extras.id = project_extras_images.project_extra_id)
// WHERE project_extras.project_id = 6
        public function project_extras_images($where=array())
        {
            $this->db->select("*")->from("project_extras")
                ->join("project_extras_images","project_extras.id=project_extras_images.project_extra_id");
            return $this->db->where($where)->get()->result();
        }
    }