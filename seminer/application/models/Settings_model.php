<?php

class Settings_model extends CI_Model {

    public $tableName = "users";

    public function __construct()
    {
        parent::__construct();

    }

    /* Tablodaki tek bir kaydı göster */
    public function get($where = array()) {
        return $this->db->where($where)->get($this->tableName)->row();
    }

    /* Tablodaki tüm kayıtları göster */
    public function get_all($where=array(),$order="id ASC") {
        return $this->db->where($where)->order_by($order)->get($this->tableName)->result();
    }

    /* Tablodaki tüm kayıtları göster tablo adını parametre olarak ver */
    public function get_all_2($tableName,$order="id ASC") {
        return $this->db->order_by($order)->get($tableName)->result();
    }

      /* Tablodaki tüm kayıtları göster-custom sql */
    public function custom_get_all($select="") {
        return $this->db->query($select)->result();
    }

    /* Tabloya yeni bir kayıt ekle */
    public function add($data = array()) {
        return $this->db->insert($this->tableName, $data);
    }

    /* Kaydı Güncelle */
    public function update($where = array(), $data = array()) {
        return $this->db->where($where)->update($this->tableName, $data);
    }

    /* Kaydı Sil */
    public function delete($where = array()) {
        return $this->db->where($where)->delete($this->tableName);
    }

    public function sorted_get_all($tableName,$orderby=null){
        //istenirse orderby parametresi kullanılmayabilir

        return $this->db
            ->order_by($orderby,"asc")//asc,desc,random
            ->get($tableName)
            ->result();
    }


}