<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seminar extends MY_Controller
{

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "seminar_v";
        $this->load->model("seminar_model");

    }

    public function index()
    {
        $viewData = new stdClass();

        // Seminer Bilgileri
        $this->db->select('seminar.id seminarid,name,title,isActive');
        $this->db->from('seminar');
        $this->db->join('seminartype', 'seminartype.id = seminar.seminartype');
        $this->db->order_by("name","asc");
        $items = $this->db->get()->result();

        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $viewData->items = $items;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function newForm()
    {
        if (!isAllowedWriteModule())
        {
            redirect(base_url("seminar"));
        }
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            //Seminer Türleri
            $viewData->seminarType = $this->seminar_model->get_all_2("seminartype", "title");
            //Okul listesi
           // $viewData->schoolList = $this->seminar_model->get_all_2("school", "school_name");
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

    }

    public function save()
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtSeminerAdi", "Seminer Adı", "required|trim");
        $this->form_validation->set_rules("txtSeminerKisaAciklama", "Kısa Açıklama", "required|trim");
        $this->form_validation->set_rules("slcSeminerTipi", "Tür", "required|trim");
        $this->form_validation->set_rules("txtSeminerDetay", "Seminer Detayı", "required|trim");
        $this->form_validation->set_message(
            array("required"=>"<b>{field}</b> alanını doldurunuz.")
        );
        $validation = $this->form_validation->run();
        if($validation){
            $insert = $this->seminar_model->add(
                array(
                    "name"                  =>$this->input->post("txtSeminerAdi"),
                    "description"           =>$this->input->post("txtSeminerKisaAciklama"),
                    "seminartype"           =>$this->input->post("slcSeminerTipi"),
                    "detail"                =>$this->input->post("txtSeminerDetay"),
                    "isActive"              =>1,
                    "createdAt"             =>date("Y-m-d H:i:s")
                )
            );
            if($insert){
                $alert = array(
                    'title'  => 'İşlem Başarılı',
                    'text'  => 'Kayıt Oluşturuldu',
                    'type'  => 'success'
                );

            }else{
                $alert = array(
                    'title'  => 'Hata Oluştu...',
                    'text'  => 'Hata Oluştu...',
                    'type'  => 'error'
                );

            }
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("seminar"));

        } else{
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            //Seminer Türleri
            $viewData->seminarType = $this->seminar_model->get_all_2("seminartype", "title");
            //Okul listesi
            //$viewData->schoolList = $this->seminar_model->get_all_2("school", "school_name");

            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }

    }

    public function updateForm($id)
    {
        $viewData = new stdClass();

        $item = $this->seminar_model->get(
            array(
                "id"    =>$id
            )
        );

        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "update";
        //Seminer Türleri
        $viewData->seminarType = $this->seminar_model->get_all_2("seminartype", "title");
        //Okul listesi
       // $viewData->schoolList = $this->seminar_model->get_all_2("school", "school_name");

        $viewData->item = $item;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function update($id)
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtSeminerAdi", "Seminer Adı", "required|trim");
        $this->form_validation->set_rules("txtSeminerKisaAciklama", "Kısa Açıklama", "required|trim");
        $this->form_validation->set_rules("slcSeminerTipi", "Tür", "required|trim");
        $this->form_validation->set_rules("txtSeminerDetay", "Seminer Detayı", "required|trim");
        $this->form_validation->set_message(
            array("required"=>"<b>{field}</b> alanını doldurunuz.")
        );
        $validation = $this->form_validation->run();
        if($validation){
            $update = $this->seminar_model->update(
                array(
                    "id"            =>$id
                ),
                array(
                    "name"                  =>$this->input->post("txtSeminerAdi"),
                    "description"           =>$this->input->post("txtSeminerKisaAciklama"),
                    "seminartype"           =>$this->input->post("slcSeminerTipi"),
                    "detail"                =>$this->input->post("txtSeminerDetay"),
                    "isActive"              =>1,
                    "createdAt"             =>date("Y-m-d H:i:s")
                )
            );
            if($update){
                $alert = array(
                    'title'  => 'İşlem Başarılı',
                    'text'  => 'Kayıt Güncellendi',
                    'type'  => 'success'
                );
            }else{
                $alert = array(
                    'title'  => 'Hata Oluştu...',
                    'text'  => 'Hata Oluştu...',
                    'type'  => 'error'
                );
            }
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("seminar"));
        } else{
            $item = $this->seminar_model->get(
                array(
                    "id"    =>$id
                )
            );
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            //Seminer Türleri
            $viewData->seminarType = $this->seminar_model->get_all_2("seminartype", "title");
            //Okul listesi
            //$viewData->schoolList = $this->seminar_model->get_all_2("school", "school_name");

            $viewData->item = $item;
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }


    }

    public function delete($id){
        $delete = $this->seminar_model->delete(
            array(
                "id"    =>$id
            )
        );

        if($delete){
             redirect(base_url("seminar"));
        }else{
             redirect(base_url("seminar"));
        }
    }

    public function isActiveSetter($id){
        if($id){
            $isActive = ($this->input->post("data")==="true") ? 1 : 0;
            $this->seminar_model->update(
                array(
                    "id"            =>$id
                ),
                array(
                    "isActive"      =>$isActive
                )
            );
        }
    }

    public function rankSetter(){
        $data = $this->input->post("data");
        parse_str($data,$order);
        $items = $order["ord"];

        foreach ($items as $rank=>$id){
            $this->seminar_model->update(
                array(
                    "id"         =>$id,
                    "rank !="    =>$rank
                ),
                array(
                    "rank"       =>$rank
                )
            );
        }


    }
}
