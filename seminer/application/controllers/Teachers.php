<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teachers extends MY_Controller
{
    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "teachers_v";
        $this->load->model("teachers_model");

    }

    public function index()
    {
        $viewData = new stdClass();

        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function newForm()
    {
        if (!isAllowedWriteModule()) {
            redirect(base_url("teachers"));
        }
        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "add";
        $user = get_active_user();
        $userrole = $user->userrole;

        if($userrole == 3) {// Kullanıcı Okul Sorumlusu ise
            //Okul listesi
            $viewData->schools = $this->teachers_model->custom_get_all("select id,school_name from schools where isActive=1 and id=".$user->schoolid);
            $viewData->roles = $this->teachers_model->custom_get_all("select id,userrole from userroles where isActive=1 and (id=2 or id=3)");
        }else {
            //Okul listesi
            $viewData->schools = $this->teachers_model->custom_get_all("select id,school_name from schools where isActive=1");
            $viewData->roles = $this->teachers_model->custom_get_all("select id,userrole from userroles where isActive=1 and (id=2 or id=3 or id=4)");

        }
         $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function save()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtOgretmenAdi", "Öğretmenin Adı Soyadı", "required|trim");
        $this->form_validation->set_rules("slcOgretmenOkul", "Görev Yaptığı Okul", "required|trim");
        $this->form_validation->set_rules("slcKullaniciTuru", "Yetki Türü", "required|trim");
        $this->form_validation->set_rules("txtOgretmenCeptel", "Cep Telefonu", "required|trim");
        $this->form_validation->set_rules("txtOgretmenEposta", "E-Posta", "trim|valid_email");
        $this->form_validation->set_rules("txtOgretmenSifre", "Şifre", "required|trim|min_length[8]|max_length[12]");
        $this->form_validation->set_rules("txtOgretmenSifre2", "Şifre (Tekrar)", "required|trim|min_length[8]|max_length[12]|matches[txtOgretmenSifre]");
        $this->form_validation->set_message(
            array(
                "required" => "<b>{field}</b> alanını doldurunuz.",
                "valid_email" => "Lütfen geçerli bir e-posta adresi yazınız",
                "min_length" => "<b>{field}</b> alanı en az 8 karakterden oluşmalıdır ",
                "max_length" => "<b>{field}</b> alanı en fazla 12 karakterden oluşmalıdır ",
                "matches" => "Şifreler birbirleri ile aynı olmalıdır"
            )
        );
        $validation = $this->form_validation->run();
        if ($validation) {
            $insert = $this->teachers_model->add(
                array(
                    "fullname" => $this->input->post("txtOgretmenAdi"),
                    "schoolid" => $this->input->post("slcOgretmenOkul"),
                    "userrole" => $this->input->post("slcKullaniciTuru"),
                    "phone" => $this->input->post("txtOgretmenCeptel"),
                    "email" => $this->input->post("txtOgretmenEposta"),
                    "password" => md5($this->input->post("txtOgretmenSifre")),
                    "isActive" => 1,
                    "createdAt" => date("Y-m-d H:i:s")
                )
            );
            if ($insert) {
                $alert = array(
                    'title' => 'İşlem Başarılı',
                    'text' => 'Kayıt Oluşturuldu',
                    'type' => 'success'
                );
            } else {
                $alert = array(
                    'title' => 'Hata Oluştu...',
                    'text' => 'Hata Oluştu...',
                    'type' => 'error'
                );
            }
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("teachers"));
        } else {
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            $user = get_active_user();
            $userrole = $user->userrole;
            if($userrole == 3) {// Kullanıcı Okul Sorumlusu ise
                //Okul listesi
                $viewData->schools = $this->teachers_model->custom_get_all("select id,school_name from schools where isActive=1 and id=".$user->schoolid);
                $viewData->roles = $this->teachers_model->custom_get_all("select id,userrole from userroles where isActive=1 and (id=2 or id=3)");
            }else {
                //Okul listesi
                $viewData->schools = $this->teachers_model->custom_get_all("select id,school_name from schools where isActive=1");
                $viewData->roles = $this->teachers_model->custom_get_all("select id,userrole from userroles where isActive=1 and (id=2 or id=3 or id=4)");

            }
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
    }

    public function updateForm($id)
    {
        $viewData = new stdClass();
        $item = $this->teachers_model->get(
            array(
                "id" => $id
            )
        );
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "update";
        $user = get_active_user();
        $userrole = $user->userrole;
        if($userrole == 3) {// Kullanıcı Okul Sorumlusu ise
            //Okul listesi
            $viewData->schools = $this->teachers_model->custom_get_all("select id,school_name from schools where isActive=1 and id=".$user->schoolid);
            $viewData->roles = $this->teachers_model->custom_get_all("select id,userrole from userroles where isActive=1 and (id=2 or id=3)");
        }else {
            //Okul listesi
            $viewData->schools = $this->teachers_model->custom_get_all("select id,school_name from schools where isActive=1");
            $viewData->roles = $this->teachers_model->custom_get_all("select id,userrole from userroles where isActive=1 and (id=2 or id=3 or id=4)");

        }
        $viewData->item = $item;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function update($id)
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtOgretmenAdi", "Öğretmenin Adı Soyadı", "required|trim");
        $this->form_validation->set_rules("slcOgretmenOkul", "Görev Yaptığı Okul", "required|trim");
        $this->form_validation->set_rules("slcKullaniciTuru", "Yetki Türü", "required|trim");
        $this->form_validation->set_rules("txtOgretmenCeptel", "Cep Telefonu", "required|trim");
        $this->form_validation->set_rules("txtOgretmenEposta", "E-Posta", "trim|valid_email");
        $this->form_validation->set_rules("txtOgretmenSifre", "Şifre", "required|trim|min_length[8]|max_length[12]");
        $this->form_validation->set_rules("txtOgretmenSifre2", "Şifre (Tekrar)", "required|trim|min_length[8]|max_length[12]|matches[txtOgretmenSifre]");
        $this->form_validation->set_message(
            array(
                "required" => "<b>{field}</b> alanını doldurunuz.",
                "valid_email" => "Lütfen geçerli bir e-posta adresi yazınız",
                "min_length" => "<b>{field}</b> alanı en az 8 karakterden oluşmalıdır ",
                "max_length" => "<b>{field}</b> alanı en fazla 12 karakterden oluşmalıdır ",
                "matches" => "Şifreler birbirleri ile aynı olmalıdır"
            )
        );
        $validation = $this->form_validation->run();
        if ($validation) {
            $update = $this->teachers_model->update(
                array(
                    "id" => $id
                ),
                array(
                    "fullname" => $this->input->post("txtOgretmenAdi"),
                    "schoolid" => $this->input->post("slcOgretmenOkul"),
                    "phone" => $this->input->post("txtOgretmenCeptel"),
                    "email" => $this->input->post("txtOgretmenEposta"),
                    "password" => md5($this->input->post("txtOgretmenSifre")),
                    "userrole" => $this->input->post("slcKullaniciTuru"),
                    "isActive" => 1,
                    "createdAt" => date("Y-m-d H:i:s")
                )
            );
            if ($update) {
                $alert = array(
                    'title' => 'İşlem Başarılı',
                    'text' => 'Kayıt Güncellendi',
                    'type' => 'success'
                );
            } else {
                $alert = array(
                    'title' => 'Hata Oluştu...',
                    'text' => 'Hata Oluştu...',
                    'type' => 'error'
                );
            }
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("teachers"));
        } else {
            $viewData = new stdClass();
            $item = $this->teachers_model->get(
                array(
                    "id" => $id
                )
            );
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $user = get_active_user();
            $userrole = $user->userrole;
            if($userrole == 3) {// Kullanıcı Okul Sorumlusu ise
                //Okul listesi
                $viewData->schools = $this->teachers_model->custom_get_all("select id,school_name from schools where isActive=1 and id=".$user->schoolid);
                $viewData->roles = $this->teachers_model->custom_get_all("select id,userrole from userroles where isActive=1 and (id=2 or id=3)");
            }else {
                //Okul listesi
                $viewData->schools = $this->teachers_model->custom_get_all("select id,school_name from schools where isActive=1");
                $viewData->roles = $this->teachers_model->custom_get_all("select id,userrole from userroles where isActive=1 and (id=2 or id=3 or id=4)");

            }
            $viewData->item = $item;
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
    }

    public function delete($id)
    {
        $delete = $this->teachers_model->delete(
            array(
                "id" => $id
            )
        );
        if ($delete) {
            redirect(base_url("teachers"));
        } else {
            redirect(base_url("teachers"));
        }
    }

    public function isActiveSetter($id)
    {
        if ($id) {
            $isActive = ($this->input->post("data") === "true") ? 1 : 0;
            $this->teachers_model->update(
                array(
                    "id" => $id
                ),
                array(
                    "isActive" => $isActive
                )
            );
        }
    }

    public function teachersList()
    {
        $user = get_active_user();
        $userrole = $user->userrole;
        $this->load->model("datatables_model");
        if($userrole == 3){ // Kullanıcı Okul sorumlusu ise
            $query = $this->datatables_model->select_get_school($user->schoolid);
        }else if($userrole == 1){
            $query = $this->datatables_model->select_get();
        }

        $data = [];
        $i = 0;
        foreach ($query->result() as $r) {
            $i++;
            $sub_array = array();
            $sub_array[] = $i;
            $sub_array[] = $r->fullname;
            $sub_array[] = $r->phone;
            $sub_array[] = $r->school_name;
            $sub_array[] = "<a href='" . base_url("") . "teachers/updateForm/" . $r->userid . "' class='btn btn-outline btn-info custom-btn btn-xs'><i class='fa fa-edit'></i>&nbsp;Düzenle</a>&nbsp;
    <a href='" . base_url("") . "teachers/delete/" . $r->userid . "'  class='btn btn-outline btn-danger btn-haberSil btn-xs custom-btn'><i class='fa fa-trash'></i>&nbsp;Sil</a>";
            $data[] = $sub_array;
        }
        $result = array(

            "recordsTotal" => $query->num_rows(),
            "recordsFiltered" => $query->num_rows(),
            "data" => $data
        );
        echo json_encode($result);
    }
}