<?php
    class Report extends MY_Controller{
        public $viewFolder = "";

        public function __construct()
        {
            parent::__construct();
            $this->viewFolder ="reports_v";
            $this->load->model("project_model");
            $this->load->model("project_extras_model");
            $this->load->model("project_extras_image_model");
            if(!get_active_user()){
                redirect(base_url("login"));
            }

        }

        //projeler sayfasını görüntüle
        public function index($id){

            $viewData = new stdClass();

            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "list";

            //proje listesindeki kullanıcıları ve okullarını alıp getirecek.
//            $project = $this->project_model->get(
//                array(
//                    "id"    => $id
//                )
//            );
//            $user_id = $project->user_id;
//            $project_user = $this
            $project = $this->project_model->project_user_school(
                array(
                    "projects.id"   =>      $id
                )
            );
            $project_extras=$this->project_model->project_extras(
                array(
                    "project_id"    => $id
                )
            );
            $project_extras_images = $this->project_model->project_extras_images(
                array(
                    "project_id" => $id
                )
            );
            $viewData->project = $project;
            $viewData->project_extras = $project_extras;
            $viewData->project_extras_images = $project_extras_images;


            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }




    }