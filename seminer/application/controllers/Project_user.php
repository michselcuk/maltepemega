<?php
    class Project_user extends MY_Controller{
        public $viewFolder = "";

        public function __construct()
        {
            parent::__construct();
            $this->viewFolder ="project_user_v";
            $this->load->model("project_model");
            if(!get_active_user()){
                redirect(base_url("login"));
            }

        }

        //projeler sayfasını görüntüle
        public function index(){

            $viewData = new stdClass();

            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "list";
            $user=get_active_user();
            $user_id = $user->id;

            //proje listesindeki giriş yapan kullanıcılara ait projeleri getirir.
            $items = $this->project_model->projects_user(
                array(
                    "user_id"   => $user_id
                ),
                "rank ASC"
            );
            $viewData->items = $items;


//            print_r($items);die();
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //sıralama işlemi
        public function rankSetter(){
            $data = $this->input->post("data");
            parse_str($data,$order);
//            print_r($order);
            $items = $order["ord"];
//            print_r($items);

            foreach ($items as $rank => $id) {
                $this->project_model->update(
                    array(
                        "id"  =>  $id,
                        "rank !=" => $rank
                    ),
                    array(
                        "rank"    =>$rank
                    )

                );

            }
        }
        //proje ekle sayfasını görüntüle
        public function new_form(){
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //proje ekle işlemi
        public function save(){
            //login kullanıcı bilgilerini al
            $user = get_active_user();
//            print_r($user);die();
            //kullancının okul id si
            $school_id=$user->schoolid;
            //kullanıcının id si
            $user_id = $user->id;

            //validate işlemleri
            $this->load->library("form_validation");
            $this->form_validation->set_rules("title","Başlık","required|trim");
            $this->form_validation->set_rules("short_description","Kısa Açıklama","required|trim");
            $this->form_validation->set_rules("description","Açıklama","required|trim");
            $this->form_validation->set_message(
                array(
                    "required"=>"<strong>{field}</strong> alanı doldurulmalıdır"
                )
            );
            $validate = $this->form_validation->run();

            if ($validate){

                $insert = $this->project_model->add(
                    array(
                        "user_id"       =>  $user_id,
                        "school_id"     =>  $school_id,
                        "title"         =>  $this->input->post('title'),
						"project_url"         =>  convertToSEO($this->input->post("title")),
                        "description"   =>  $this->input->post('description'),
                        "short_description"   =>  $this->input->post('short_description'),
                        "project_teachers"   =>  $this->input->post('project_teachers'),
                        "project_teachers_count"   =>  $this->input->post('project_teachers_count'),
                        "project_students"   =>  $this->input->post('project_students'),
                        "project_students_count"   =>  $this->input->post('project_students_count'),
                        "createdAt"     =>  date("Y-m-d H:i:s")
                    )
                );
                //alert sistemi ekleneceek
                if ($insert){
                    $alert=array(
                        "title" => "işlem başarılı",
                        "text"  => "Kayıt başarılı bir şekilde eklendi",
                        "type"  => "success"
                    );
                }else{
                    $alert=array(
                        "title" => "işlem başarısız",
                        "text"  => "Ekleme işlemi sırasında bir sorun oluştu",
                        "type"  => "error"
                    );
                }
                //işlem sonucu session a aktar
                $this->session->set_flashdata("alert",$alert);
                redirect(base_url("project_user"));
            }
            else {
                $viewData = new stdClass();
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "add";
                $viewData->form_error = true;
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

//                echo validation_errors();
            }

        }
        //proje güncelle sayfasını görüntüle
        public function update_form($id){
            $viewData = new stdClass();
            // tablodan id'ye göre kayıtları getirme işlemi
            $item = $this->project_model->get(
            //where in array olması birden fazla şart oluşturmak için esneklik sağlar
            // örneğin id => $id, isActive =>1 dendiğinde hem aktif hem de id'si bir olanı demektir.
            //he burda kullanılmaz o ayrı :)
                array(
                    "id" => $id
                )
            );
//            print_r($item);die();


            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $viewData->item = $item;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //proje güncelleme işlemi
        public function update($id){
            //update işleminde login olan kullanıcı bilgilerine ihityaç yoktur.
            //login bilgisi alınırsa proje yazarı ve proje yazarı okul bilgisi de değiştirilir. !!!

            //validate işlemleri
            $this->load->library("form_validation");
            $this->form_validation->set_rules("title","Başlık","required|trim");
            $this->form_validation->set_rules("short_description","Kısa Açıklama","required|trim");
            $this->form_validation->set_rules("description","Açıklama","required|trim");
            $this->form_validation->set_message(
                array(
                    "required"=>"<strong>{field}</strong> alanı doldurulmalıdır"
                )
            );
            $validate = $this->form_validation->run();

            if ($validate){
                $update = $this->project_model->update(
                    array(
                        "id"  =>$id
                    ),
                    array(
                        "user_id"       =>  $this->input->post('user_id'),
                        "school_id"     =>  $this->input->post('school_id'),
                        "title"         =>  $this->input->post('title'),
                        "description"   =>  $this->input->post('description'),
                        "short_description"   =>  $this->input->post('short_description'),
                        "project_teachers"   =>  $this->input->post('project_teachers'),
                        "project_teachers_count"   =>  $this->input->post('project_teachers_count'),
                        "project_students"   =>  $this->input->post('project_students'),
                        "project_students_count"   =>  $this->input->post('project_students_count'),
                        "createdAt"     =>  date("Y-m-d H:i:s")
                    )
                );
                //alert sistemi ekleneceek
                if ($update){
                    $alert=array(
                        "title" => "işlem başarılı",
                        "text"  => "Kayıt başarılı bir şekilde güncellendi",
                        "type"  => "success"
                    );
                }else{
                    $alert=array(
                        "title" => "işlem başarısız",
                        "text"  => "Güncelleme işlemi sırasında bir sorun oluştu",
                        "type"  => "error"
                    );
                }
                $this->session->set_flashdata("alert",$alert);
                redirect(base_url("project_user"));
            }
            else {
                $viewData = new stdClass();
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "update";
                $item= $this->project_model->get(
                    array(
                        "id" =>$id
                    )
                );
                $viewData->item = $item;
                $viewData->form_error = true;
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

//                echo validation_errors();
            }

        }
        //proje silme işlemi
        public function delete($id){
            $delete = $this->project_model->delete(
                array(
                    "id"    => $id
                )
            );
            if ($delete){
                $alert=array(
                    "title" => "işlem başarılı",
                    "text"  => "Kayıt başarılı bir şekilde silindi",
                    "type"  => "success"
                );
            }else{
                $alert=array(
                    "title" => "işlem başarısız",
                    "text"  => "Silme işlemi sırasında bir sorun oluştu",
                    "type"  => "error"
                );
            }
            $this->session->set_flashdata("alert",$alert);
            redirect(base_url("project_user"));
        }
        //okul onay aktif pasif işlemi
        public function school_confirm($id){
            if ($id){
                $school_confirm = ($this->input->post("data") == "true") ? 1 : 0;

                $this->project_model->update(
                    array(
                        "id"          => $id
                    ),
                    array(
                        "school_confirm"    =>  $school_confirm
                    )
                );
            }
        }
        //ilçe onay aktif pasif işlemi
        public function admin_confirm($id){
            if ($id){
                $admin_confirm = ($this->input->post("data") === "true") ? 1 : 0;

                $this->project_model->update(
                    array(
                        "id"          => $id
                    ),
                    array(
                        "admin_confirm"    =>  $admin_confirm
                    )
                );
            }
        }
        //ana sayfa da yayınlama işlemi
        public function publish($id){
            if ($id){
                $publish = ($this->input->post("data") === "true") ? 1 : 0;

                $this->project_model->update(
                    array(
                        "id"          => $id
                    ),
                    array(
                        "publish"    =>  $publish
                    )
                );
            }
        }

    }