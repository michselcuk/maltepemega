<?php
    class Project_general extends MY_Controller{

        public $viewFolder = "";

        public function __construct()
        {
            parent::__construct();

            $this->viewFolder = "project_general_v";
            $this->load->helper(array('form', 'url'));
            $this->load->model("project_general_model");
            if(!get_active_user()){
                redirect(base_url("login"));
            }

        }

        public function index(){
            $viewData = new stdClass();

            //tablodan verilerin getirilmesi
            $items = $this->project_general_model->get_all(
                array()
            );

            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "list";
            $viewData->items = $items;

//            print_r($items);
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //ekle formunu getir
        public function new_form(){
            if (!isAllowedWriteModule()){
                redirect(base_url("dashboard"));
            }
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //kayıt ekle işlemi
        public function save(){

            //validate işlemleri
            $this->load->library("form_validation");
            $this->form_validation->set_rules("title","Başlık","required|trim");
            $this->form_validation->set_message(
                array(
                    "required"=>"<strong>{field}</strong> alanı doldurulmalıdır"
                )
            );

            $validate = $this->form_validation->run();

            if ($validate){
                //file upload
                if ($_FILES['img_url']['name']){
  /*                  $config["allowed_types"]=" jpeg|jpg|png|gif ";
                    $config["max_size"]=1024;
                    $config["remove_spaces"]=true;
                    $config["encrypt_name"]=true;
                    $config["upload_path"]="uploads/$this->viewFolder";
                    $this->load->library("upload",$config);
                    $upload=$this->upload->do_upload("img_url");
                    if ($upload){
                        $uploaded_file_name = $this->upload->data("file_name");
                    }
                    else{

                       echo 'başarısız';
                    } */
					
					 $sayi=rand(0,5000);
						$sayi1=rand(0,5000);
						$file_name = convertToSEO(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME)). "." . pathinfo($_FILES["img_url"]["name"],PATHINFO_EXTENSION);
						$image_700x400=upload_picture($_FILES["img_url"]["tmp_name"],"uploads/$this->viewFolder",700,400,"$sayi$sayi1$file_name");
							if($image_700x400){
							$data=array(
                        "title"                 =>$this->input->post('title'),
                        "description"           =>$this->input->post('description'),
                        "project_url"           =>  convertToSEO($this->input->post("title")),
                        "isActive"              =>  1,
                        "createdAt"             =>  date("Y-m-d H:i:s"),
						"img_url" => "$sayi$sayi1$file_name" ,
							);
					
					
			

                $insert = $this->project_general_model->add($data
             /*       array(
                        "img_url"               => $uploaded_file_name,
                        "title"                 =>$this->input->post('title'),
                        "description"           =>$this->input->post('description'),
                        "project_url"           =>  convertToSEO($this->input->post("title")),
                        "isActive"              =>  1,
                        "createdAt"             =>  date("Y-m-d H:i:s")
                    ) */
                );
                //alert sistemi ekleneceek
                if ($insert){
                    $alert=array(
                        "title" => "işlem başarılı",
                        "text"  => "Kayıt başarılı bir şekilde eklendi",
                        "type"  => "success"
                    );
                }else{
                    $alert=array(
                        "title" => "işlem başarısız",
                        "text"  => "Ekleme işlemi sırasında bir sorun oluştu",
                        "type"  => "error"
                    );
                }}
                //işlem sonucu session a aktar
                $this->session->set_flashdata("alert",$alert);
                redirect(base_url("project_general"));
            }
            else {
                $viewData = new stdClass();
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "add";
                $viewData->form_error = true;
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

//                echo validation_errors();
            }

        } }
        //update formunu getir
        public function update_form($id){
            if (!isAllowedUpdateModule()){
                redirect(base_url("dashboard"));
            }
            $viewData = new stdClass();
            // tablodan id'ye göre kayıtları getirme işlemi
            $item = $this->project_general_model->get(
            //where in array olması birden fazla şart oluşturmak için esneklik sağlar
            // örneğin id => $id, isActive =>1 dendiğinde hem aktif hem de id'si bir olanı demektir.
                //he burda kullanılmaz o ayrı :)
                array(
                    "id" => $id
                )
            );
//            print_r($item);die();


            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $viewData->item = $item;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //update işlemi
        public function update($id){
            if (!isAllowedUpdateModule()){
                redirect(base_url("dashboard"));
            }
            //validate işlemleri
            $this->load->library("form_validation");
            $this->form_validation->set_rules("title","Başlık","required|trim");
            $this->form_validation->set_message(
                array(
                    "required"=>"<strong>{field}</strong> alanı doldurulmalıdır"
                )
            );
            $validate = $this->form_validation->run();

            if ($validate){
                if ($_FILES['img_url']['name']){
           /*         $config["allowed_types"]=" jpeg|jpg|png|gif ";
                    $config["max_size"]=1024;
                    $config["remove_spaces"]=true;
                    $config["encrypt_name"]=true;
                    $config["upload_path"]="uploads/$this->viewFolder";
                    $this->load->library("upload",$config);
                    $upload=$this->upload->do_upload("img_url");
					$uploaded_file_name = $this->upload->data("file_name"); */
					
					
	  $sayi=rand(0,5000);
 	  $sayi1=rand(0,5000);
	  $file_name = convertToSEO(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME)). "." . pathinfo($_FILES["img_url"]["name"],PATHINFO_EXTENSION);
      $image_700x400=upload_picture($_FILES["img_url"]["tmp_name"],"uploads/$this->viewFolder",700,400,"$sayi$sayi1$file_name");
					
					$update = $this->project_general_model->update(
                    array(
                      "id"  =>$id
                    ),
                    array(
                        "img_url"               => "$sayi$sayi1$file_name" ,
                        "title"                 =>$this->input->post('title'),
                        "description"           =>$this->input->post('description'),
                        "project_url"           =>  convertToSEO($this->input->post("title")),
                        "isActive"              =>  1,
                        "createdAt"             =>  date("Y-m-d H:i:s")
                    )
                );
                //alert sistemi ekleneceek
                if ($update){
                    $alert=array(
                        "title" => "işlem başarılı",
                        "text"  => "Kayıt başarılı bir şekilde güncellendi",
                        "type"  => "success"
                    );
                }else{
                    $alert=array(
                        "title" => "işlem başarısız",
                        "text"  => "Güncelleme işlemi sırasında bir sorun oluştu",
                        "type"  => "error"
                    );
                }
										
                }
                else{
                $update = $this->project_general_model->update(
                    array(
                      "id"  =>$id
                    ),
                    array(
                        "title"                 =>$this->input->post('title'),
                        "description"           =>$this->input->post('description'),
                        "project_url"           =>  convertToSEO($this->input->post("title")),
                        "isActive"              =>  1,
                        "createdAt"             =>  date("Y-m-d H:i:s")
                    )
                );
                //alert sistemi ekleneceek
                if ($update){
                    $alert=array(
                        "title" => "işlem başarılı",
                        "text"  => "Kayıt başarılı bir şekilde güncellendi",
                        "type"  => "success"
                    );
                }else{
                    $alert=array(
                        "title" => "işlem başarısız",
                        "text"  => "Güncelleme işlemi sırasında bir sorun oluştu",
                        "type"  => "error"
                    );
			}}
                $this->session->set_flashdata("alert",$alert);
                redirect(base_url("project_general"));
            }
            else {
                $viewData = new stdClass();
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "update";
                $item= $this->project_general_model->get(
                    array(
                        "id" =>$id
                    )
                );
                $viewData->item = $item;
                $viewData->form_error = true;
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

//                echo validation_errors();
            }

        }
        //silme işlemi
        public function delete($id){
            if (!isAllowedDeleteModule()){
                redirect(base_url("dashboard"));
            }
            $delete = $this->project_general_model->delete(
                array(
                    "id"    => $id
                )
            );
            if ($delete){
                $alert=array(
                    "title" => "işlem başarılı",
                    "text"  => "Kayıt başarılı bir şekilde silindi",
                    "type"  => "success"
                );
            }else{
                $alert=array(
                    "title" => "işlem başarısız",
                    "text"  => "Silme işlemi sırasında bir sorun oluştu",
                    "type"  => "error"
                );
            }
            $this->session->set_flashdata("alert",$alert);
            redirect(base_url("project_general"));
        }
        //ürün aktif pasif işlemi
        public function isActiveSetter($id){
            if ($id){
                $isActive = ($this->input->post("data") === "true") ? 1 : 0;

                $this->project_general_model->update(
                  array(
                      "id"          => $id
                  ),
                  array(
                      "isActive"    =>  $isActive
                  )
                );
            }
        }


    }