<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courselist extends MY_Controller
{

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "courselist_v";
        $this->load->model("course_model");

    }

    public function index()
    {
        $viewData = new stdClass();

        $items = $this->course_model->custom_get_all("select course.id as courseid,name,quota,startdate,starttime,endtime,course.isActive as isActive from course join seminar on seminar.id=course.seminarid order by name asc");

        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $viewData->items = $items;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }
    public function detail($id)
    {

        $viewData = new stdClass();
        $user = get_active_user();
        $viewData->userInfo = $user;


        $this->db->select('*');
        $this->db->from('course');
        $this->db->join('seminar', 'seminar.id=course.seminarid');
        $this->db->join('schools', 'schools.id=course.schoolid');
        $this->db->join('courseteacher', 'courseteacher.id=course.teacherid');
        $this->db->join('seminartype', 'seminartype.id=seminar.seminartype');
        $this->db->where('course.id',$id);

        $viewData->items = $this->db->get()->result();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "detail";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

    }
}
