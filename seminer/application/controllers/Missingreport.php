<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Missingreport extends MY_Controller
{

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "teachersreport_v";
        $this->load->model("teachers_model");
        $this->load->library("pdf");


    }


    public function index()
    {
        $viewData = new stdClass();

        //$viewData->gunler = $this->teachers_model->custom_get_all("select distinct(startdate) as sdate from course");
        $user = get_active_user();
        $viewData->userInfo = $user;
        $teachers = $this->teachers_model->custom_get_all("select id,fullname,(select count(*) from recourses where userid=users.id) as secimsayisi from users where schoolid=".$user->schoolid." order by secimsayisi");
        $viewData->missingTeacher = $teachers;
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "missinglist";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }



    public function pdfList()
    { 

        $user = get_active_user();
        $this->db->select('*');
        $this->db->from('recourses');
        $this->db->join('users', 'users.id=recourses.userid');
        $this->db->join('course', 'course.id=recourses.courseid');
        $this->db->join('schools', 'schools.id=course.schoolid');
        $this->db->join('seminar', 'seminar.id=course.seminarid');
        $this->db->join('seminartype', 'seminartype.id=seminar.seminartype');
        $this->db->where('course.schoolid',$user->schoolid);
        $this->db->order_by("fullname","asc");
        $this->db->limit(10);
        $teachersListpdf = $this->db->get()->result();

        $html ="
        <html>
        <head>
        <style>
            @page { margin: 40px 10px 10px 10px; } 
            body{ 
                    font-family: 'DejaVu Sans,DejaVu Sans Condensed', 'DejaVu Sans Condensed';
                    font-size: 9pt;
                }
        </style>
        </head>
        <body>
        
  
      
        <h3 style='text-align: center'>Okulunuzdaki Eğitime Katılan Öğretmenlerin Listesi</h3>
        <table width='100%' style='border:1px solid #666' cellpadding='2'>
          <tr style='background-color: #bbb; height:30px'>
            <th align='right'>No</th>
            <th>Öğretmen</th>
            <th>Eğitim Adı</th>
            <th align='center'>Türü</th>
            <th align='center'>Tarih</th>
            <th align='center'>Saat</th>
            <th>Eğitim Yeri</th>
            <th>İmza</th>
 
            </tr>";
            $i=1;

            foreach ($teachersListpdf as $list) {
                $bcolor="";
                if($i%2==0)
                    $bcolor="background-color: #efefef";
                else
                    $bcolor="";

                $html.="<tr style='".$bcolor."'>
                        <td align='right'>".$i."</td>
                        <td>".$list->fullname."</td>
                        <td>".$list->name."</td>
                        <td align='center'>".$list->title."</td>
                        <td align='center'>".date("d.m.Y",strtotime($list->startdate))."</td>
                        <td align='center'>".date("H:i",strtotime($list->starttime))." - ".date("H:i",strtotime($list->endtime))."</td>
                        <td>".$list->school_name."</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>";  
                        $i++;
            }
      
        $html.= "
        </table>
        </body>
        </html>";


        $this->pdf->create($html,"Okul_Ogretmenleri_Egitim_Listesi");
        

    }

}

