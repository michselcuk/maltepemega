<?php
    class Project_extras extends MY_Controller{
        public $viewFolder = "";

        public function __construct()
        {
            parent::__construct();
            $this->viewFolder ="project_extras_v";
            $this->load->model("project_model");
            $this->load->model("project_extras_model");
            $this->load->model("project_extras_image_model");
            if(!get_active_user()){
                redirect(base_url("login"));
            }

        }

        //proje_extras sayfasını görüntüle
        public function index($id){
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "list";
            //proje isminin yazılması için gerekli
            $project = $this->project_model->get(
                array(
                    "id" => $id
                )
            );
            $viewData->project = $project;
            $viewData->project_id = $id;
            $viewData->items = $this->project_extras_model->get_all(
                array(
                    "project_id" => $id
                )
            );
//            print_r($viewData);
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //sıralama işlemi ? gerekli olmayabilir
        public function rankSetter(){
            $data = $this->input->post("data");
            parse_str($data,$order);
//            print_r($order);
            $items = $order["ord"];
//            print_r($items);

            foreach ($items as $rank => $id) {
                $this->project_extras_model->update(
                    array(
                        "id"  =>  $id,
                        "rank !=" => $rank
                    ),
                    array(
                        "rank"    =>$rank
                    )

                );

            }
        }
        //proje extras ekleme sayfasını görüntüle
        public function new_form($id){
            if (!isAllowedWriteModule()){
                redirect(base_url("dashboard"));
            }
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            $project = $this->project_model->get(
                array(
                    "id"    => $id
                )
            );
            $viewData->id = $id;
            $viewData->project = $project;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //proje ekle işlemi
        public function save(){
            if (!isAllowedWriteModule()){
                redirect(base_url("dashboard"));
            }
            //validate işlemleri
            $this->load->library("form_validation");
            $this->form_validation->set_rules("title","Başlık","required|trim");
            $this->form_validation->set_rules("description","Açıklama","required|trim");
            $this->form_validation->set_message(
                array(
                    "required"=>"<strong>{field}</strong> alanı doldurulmalıdır"
                )
            );
            $validate = $this->form_validation->run();

            if ($validate){

                $insert = $this->project_extras_model->add(
                    array(

                        "title"         =>  $this->input->post('title'),
                        "description"   =>  $this->input->post('description'),
                        "project_id"    =>  $this->input->post('project_id'),
                        "createdAt"     =>  date("Y-m-d H:i:s")
                    )
                );
                $project_id = $this->input->post('project_id');
                //alert sistemi ekleneceek
                if ($insert){
                    $alert=array(
                        "title" => "işlem başarılı",
                        "text"  => "Kayıt başarılı bir şekilde eklendi",
                        "type"  => "success"
                    );
                }else{
                    $alert=array(
                        "title" => "işlem başarısız",
                        "text"  => "Ekleme işlemi sırasında bir sorun oluştu",
                        "type"  => "error"
                    );
                }
                //işlem sonucu session a aktar
                $this->session->set_flashdata("alert",$alert);
                redirect(base_url("project_extras/index/$project_id"));
            }
            else {
                $viewData = new stdClass();
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "add";
                $viewData->form_error = true;
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

//                echo validation_errors();
            }

        }
        //proje güncelle sayfasını görüntüle
        public function update_form($id){
            if (!isAllowedUpdateModule()){
                redirect(base_url("dashboard"));
            }
            $viewData = new stdClass();
            // tablodan id'ye göre kayıtları getirme işlemi
            $item = $this->project_extras_model->get(
            //where in array olması birden fazla şart oluşturmak için esneklik sağlar
            // örneğin id => $id, isActive =>1 dendiğinde hem aktif hem de id'si bir olanı demektir.
            //he burda kullanılmaz o ayrı :)
                array(
                    "id" => $id
                )
            );
            $project_id = $item->project_id;
//            echo ($item->project_id);die();
            $project = $this->project_model->get(
                array(
                    "id"    => $project_id
                )
            );
            $viewData->project = $project;
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $viewData->item = $item;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //proje güncelleme işlemi
        public function update($id){
            if (!isAllowedUpdateModule()){
                redirect(base_url("dashboard"));
            }
            //update işleminde login olan kullanıcı bilgilerine ihityaç yoktur.
            //login bilgisi alınırsa proje yazarı ve proje yazarı okul bilgisi de değiştirilir. !!!
            $project_extra = $this->project_extras_model->get(
                array(
                    "id"    => $id
                )
            );
//            echo($project_extra->project_id);die();
            //validate işlemleri
            $this->load->library("form_validation");
            $this->form_validation->set_rules("title","Başlık","required|trim");
            $this->form_validation->set_rules("description","Açıklama","required|trim");
            $this->form_validation->set_message(
                array(
                    "required"=>"<strong>{field}</strong> alanı doldurulmalıdır"
                )
            );
            $validate = $this->form_validation->run();

            if ($validate){
                $update = $this->project_extras_model->update(
                    array(
                        "id"  =>$id
                    ),
                    array(
                        "title"         =>  $this->input->post('title'),
                        "description"   =>  $this->input->post('description'),
                        "createdAt"     =>  date("Y-m-d H:i:s")
                    )
                );
                //alert sistemi ekleneceek
                if ($update){
                    $alert=array(
                        "title" => "işlem başarılı",
                        "text"  => "Kayıt başarılı bir şekilde güncellendi",
                        "type"  => "success"
                    );
                }else{
                    $alert=array(
                        "title" => "işlem başarısız",
                        "text"  => "Güncelleme işlemi sırasında bir sorun oluştu",
                        "type"  => "error"
                    );
                }
                $this->session->set_flashdata("alert",$alert);
                redirect(base_url("project_extras/index/$project_extra->project_id"));
            }
            else {
                $viewData = new stdClass();
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "update";
                $item= $this->project_extras_model->get(
                    array(
                        "id" =>$id
                    )
                );
                $viewData->item = $item;
                $viewData->form_error = true;
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

//                echo validation_errors();
            }

        }
        //proje silme işlemi
        public function delete($id){
            if (!isAllowedDeleteModule()){
                redirect(base_url("dashboard"));
            }
            $project_extra = $this->project_extras_model->get(
              array(
                  "id"  => $id
              )
            );
            $project_id = $project_extra->project_id;
            $delete = $this->project_extras_model->delete(
                array(
                    "id"    => $id
                )
            );
            if ($delete){
                $alert=array(
                    "title" => "işlem başarılı",
                    "text"  => "Kayıt başarılı bir şekilde silindi",
                    "type"  => "success"
                );
            }else{
                $alert=array(
                    "title" => "işlem başarısız",
                    "text"  => "Silme işlemi sırasında bir sorun oluştu",
                    "type"  => "error"
                );
            }
            $this->session->set_flashdata("alert",$alert);
            redirect(base_url("project_extras/index/$project_id"));
        }
        //resim yükleme formu
        public function image_form($id){
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "image";
//            echo $id; die();
            $viewData->item = $this->project_extras_model->get(
                array(
                    "id" => $id
                )
            );
            $viewData->item_images = $this->project_extras_image_model->get_all(
                array(
                    "project_extra_id"    => $id

                )
//                , "rank ASC"

            );

            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

        }
        //resim yükleme işlemi
        public function image_upload($id){
/*
            $config["allowed_types"]=" jpeg|jpg|png|gif ";
            $config["max_size"]=1024;
            $config["remove_spaces"]=true;
            $config["encrypt_name"]=true;
            $config["upload_path"]="uploads/$this->viewFolder";
            $this->load->library("upload",$config);
            $upload=$this->upload->do_upload("file");
            if ($upload){
                $uploaded_file_name = $this->upload->data("file_name");
                $this->project_extras_image_model->add(
                    array(
                        "img_url"           => $uploaded_file_name,
                        "rank"              => 0,
                        "project_extra_id"  => $id,
                        "isActive"          => 1,
                        "isCover"           => 1,
                        "createdAt"         =>date("Y-m-d H:i:s")
                    )
                );
            }
            else{
                echo 'başarısız';
            }  */
			
			$sayi=rand(0,5000);
			$sayi1=rand(0,5000);
			$file_name = convertToSEO(pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME)). "." . pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
			$image_700x400=upload_picture($_FILES["file"]["tmp_name"],"uploads/$this->viewFolder",700,400,"$sayi$sayi1$file_name");
			if($image_700x400){


			$this->project_extras_image_model->add(
					array(
							"img_url" =>"$sayi$sayi1$file_name",
							"rank"              => 0,
							"project_extra_id"  => $id,
                        "isActive"          => 1,
                        "isCover"           => 1,
                        "createdAt"         =>date("Y-m-d H:i:s")
						)
				);


}else{

echo "no";
}
			
			
			
			
			
        }
        //resim listesini dinamik görüntüleme işlemi
        public function refresh_image_list($id){
//            echo $project_extra_id;die();
            $viewData = new stdClass();

            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "image";

//            $viewData->item = $this->project_extras_model->get(
//                array(
//                    "project_id" => $project_id
//                )
//            );
            $viewData->item_images = $this->project_extras_image_model->get_all(
                array(
                    "project_extra_id"    => $id
                )
            );
//            print_r($viewData);die();
            $render_html = $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/image_list_v", $viewData, true);
            echo $render_html;
        }
        //ürün resim aktif/pasif işlemi
        public function isImageActiveSetter($id){
            if ($id){
                $isActive = ($this->input->post("data") === "true") ? 1 : 0;

                $this->product_image_model->update(
                    array(
                        "id"          => $id
                    ),
                    array(
                        "isActive"    =>  $isActive
                    )
                );
            }
        }
        //kapak resmi seçme işlemi
        public function isCoverSetter($id, $parent_id){
            if ($id && $parent_id) {
                $isCover = ($this->input->post("data") === "true") ? 1 : 0;
                //kapak yapılacak fotoya ait kayıt
                $this->product_image_model->update(
                    array(
                        "id"            => $id,
                        "product_id"    => $parent_id
                    ),
                    array(
                        "isCover"    =>  $isCover
                    )
                );
                //kapak fotoğrafı olmayan durumlar için
                $this->product_image_model->update(
                    array(
                        "id !="         => $id,
                        "product_id"    => $parent_id
                    ),
                    array(
                        "isCover"    =>  0
                    )
                );
                $viewData = new stdClass();

                /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "image";
                // resmin ait olduğu ürün adı için product modele gidildi,

                $viewData->item = $this->product_model->get(
                    array(
                        // product_id yani parent_id product modelindeki id ye denk geliyor.
                        "id" => $parent_id
                    )
                );
                $viewData->item_images = $this->product_image_model->get_all(
                    array(
                        "product_id"    => $parent_id
                    ), "rank ASC"
                );

                $render_html = $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/image_list_v", $viewData, true);

                echo $render_html;
            }
        }
        //image sıralama işlemi
        public function imageRankSetter(){
            $data = $this->input->post("data");
            parse_str($data,$order);
//            print_r($order);
            $items = $order["ord"];
//            print_r($items);

            foreach ($items as $rank => $id) {
                $this->project_extras_image_model->update(
                    array(
                        "id"  =>  $id,
                        "rank !=" => $rank
                    ),
                    array(
                        "rank"    =>$rank
                    )

                );

            }
        }
        //silme işlemi
        public function imageDelete($id, $parent_id){
            $file=$this->project_extras_image_model->get(
                array(
                    "id"    =>$id
                )
            );

            $delete = $this->project_extras_image_model->delete(
                array(
                    "id"    => $id
                )
            );
            if ($delete){
                unlink("uploads/{$this->viewFolder}/$file->img_url");
                redirect(base_url("project_extras/image_form/$parent_id"));
            }else{
                redirect(base_url("project_extras/image_form/$parent_id"));
            }
        }

    }