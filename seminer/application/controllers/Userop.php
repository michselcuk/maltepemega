<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userop extends CI_Controller{
    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "users_v";
        $this->load->model("user_model");

    }

    public function login(){ 
        if(get_active_user()){
            redirect(base_url());
        }
        
        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "login";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);


    }

    public function do_login(){
        if(get_active_user()){
            redirect(base_url());
        }
        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtLoginCeptel", "Cep Telefonu", "required|trim");
        $this->form_validation->set_rules("txtLoginSifre", "Şifre", "required|trim|min_length[8]|max_length[12]");
        $this->form_validation->set_message(
            array(
                "required"=>"<b>{field}</b> alanını doldurunuz.",
                "min_length"=>"<b>{field}</b> alanı en az 8 karakterden oluşmalıdır ",
                "max_length"=>"<b>{field}</b> alanı en fazla 12 karakterden oluşmalıdır "
            )
        );

        if($this->form_validation->run()==FALSE) {
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "login";
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

        }else{

           // echo OzelKarakterTemizle($this->input->post("txtLoginCeptel"));

            $user = $this->user_model->get(
                array(
                    "phone"    => $this->input->post("txtLoginCeptel"),
                    "password"     =>md5($this->input->post("txtLoginSifre")),
                    "isActive"  =>1
                )
            );

            if($user){

                set_user_roles();
                $this->session->set_userdata("user",$user);
                $this->session->set_userdata("gun",4);

                redirect(base_url());

            }else{
                redirect(base_url("login"));
            }
        }
    } // #do_login

    public function logout(){
        $this->session->unset_userdata("user");
        redirect(base_url("login"));
    }

    public function send_email(){
        $config = array(
            "protocol"          => "smtp",
            "smtp_host"         => "ssl://smtp.gmail.com",
            "smtp_port"         => 465,
            "smtp_user"         => "akgunduz5434@gmail.com",
            "smtp_pass"         => "111",
            "start_tls"         => true,
            "charset"           => "utf-8",
            "mailtype"          => "html",
            "wordwrap"          => true,
            "newline"           => "\r\n"
        );

        $this->load->library("email", $config);
        $this->email->from("akgunduz5434@gmail.com","MEM");
        $this->email->to("akgunduz54@yahoo.com");
        $this->email->subject("Maltepe İlçe MEM");
        $this->email->message("Deneme Maili");

        $send = $this->email->send();
        if($send){
            echo ("başarılı");
        }else{
            echo $this->email->print_debugger();
        }


    }

}// #class