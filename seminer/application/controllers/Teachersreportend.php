<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teachersreportend extends MY_Controller
{

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "teachersreport_v";
        // $this->load->model("actual_model");
        $this->load->library("pdf");

    }


    public function index()
    {
        $viewData = new stdClass();

        $user = get_active_user();
        $viewData->userInfo = $user;

        //$this->db->select('select fullname,name,startdate,joined');
        $this->db->select('*');
        $this->db->from('recourses');
        $this->db->join('users', 'users.id=recourses.userid');
        $this->db->join('course', 'course.id=recourses.courseid');
        $this->db->join('seminar', 'seminar.id=course.seminarid');
        $this->db->where('users.schoolid', $user->schoolid);
        $this->db->order_by("fullname", "asc");
        $this->db->order_by("startdate", "asc");
        $viewData->teachers = $this->db->get()->result();
    
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "teachersreportend";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }




    public function pdfList()
    {

        $user = get_active_user();
        $this->db->select('*');
        $this->db->from('recourses');
        $this->db->join('users', 'users.id=recourses.userid');
        $this->db->join('course', 'course.id=recourses.courseid');
        $this->db->join('seminar', 'seminar.id=course.seminarid');
        $this->db->where('users.schoolid', $user->schoolid);
        $this->db->order_by("fullname", "asc");
        $this->db->order_by("startdate", "asc");
        $teachersListpdf = $this->db->get()->result();


        $html = "
        <html>
        <head>
        <style>
            @page { margin: 40px 10px 10px 10px; } 
            body{ 
                    font-family: 'DejaVu Sans,DejaVu Sans Condensed', 'DejaVu Sans Condensed';
                    font-size: 9pt;
                }
        </style>
        </head>
        <body>
        <center><h3>Okul Öğretmenlerimizin Eğitim Yoklama Listesi</h3></center>
        <table width='100%' style='border:1px solid #666' cellpadding='2'>
          <tr style='background-color: #bbb; height:30px'>
            <th align='right'>No</th>
            <th>Öğretmenin Adı Soyadı</th>
            <th>Eğitim Adı</th>
            <th align='center'>Tarih</th>
            <th>Durum</th>
        </tr>";
        $i = 1;

        foreach ($teachersListpdf as $list) {
            $bcolor = "";
            if ($i % 2 == 0)
                $bcolor = "background-color: #efefef";
            else
                $bcolor = "";


            if($list->joined==1)
                $durum= "GELDİ";
            else
                $durum = "<b style='color:red'>GELMEDİ</b>";

            $html .= "<tr style='" . $bcolor . "'>
                        <td align='right'>" . $i . "</td>
                        <td>" . $list->fullname . "</td>
                        <td>" . $list->name . "</td>
                        <td align='center'>" . date("d.m.Y", strtotime($list->startdate)) . "</td>
                        <td align='center'>". $durum . "</td>
                        </tr>";
            $i++;
        }

        $html .= "
        </table>
        </body>
        </html>";


        $this->pdf->create($html, "Okul_Ogretmenleri_Egitim_Listesi");


    }
    

}

