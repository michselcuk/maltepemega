<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School extends MY_Controller
{

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "school_v";
        $this->load->model("school_model");

    }

    public function index()
    {
        $viewData = new stdClass();

        $items = $this->school_model->get_all(
            array()
        );


        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $viewData->items = $items;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function newForm()
    {
        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "add";

        //İl listesi
        $viewData->cityList = $this->school_model->get_all_2("city", "id");

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function save()
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtOkulAdi", "Okul Adı", "required|trim");
        $this->form_validation->set_rules("txtOkulSorumlu", "Okul Sorumlusu", "required|trim");
        $this->form_validation->set_rules("slcOkulIl", "İl", "required|trim");
        $this->form_validation->set_rules("slcOkulIlce", "İlçe", "required|trim");
        $this->form_validation->set_rules("txtOkulAdres", "Adres", "required|trim");
        $this->form_validation->set_rules("txtOkulTelefon", "Okul Telefonu", "required|trim");
        $this->form_validation->set_message(
            array("required" => "<b>{field}</b> alanını doldurunuz.")
        );
        $validation = $this->form_validation->run();
        if ($validation) {
            $insert = $this->school_model->add(
                array(
                    "school_name" => $this->input->post("txtOkulAdi"),
                    "manager" => $this->input->post("txtOkulSorumlu"),
                    "manager_phone" => $this->input->post("txtOkulSorumluTelefon"),
                    "city" => $this->input->post("slcOkulIl"),
                    "town" => $this->input->post("slcOkulIlce"),
                    "address" => $this->input->post("txtOkulAdres"),
                    "school_phone" => $this->input->post("txtOkulTelefon"),
                    "web" => $this->input->post("txtOkulWeb"),
                    "isActive" => 1,
                    "rank" => 0,
                    "createdAt" => date("Y-m-d H:i:s")
                )
            );
            if ($insert) {
                $alert = array(
                    'title' => 'İşlem Başarılı',
                    'text' => 'Kayıt Oluşturuldu',
                    'type' => 'success'
                );

            } else {
                $alert = array(
                    'title' => 'Hata Oluştu...',
                    'text' => 'Hata Oluştu...',
                    'type' => 'error'
                );
            }
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("school"));
        } else {
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            //İl listesi
            $viewData->cityList = $this->school_model->get_all_2("city", "id");
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }


    }

    public function updateForm($id)
    {
        $viewData = new stdClass();

        $item = $this->school_model->get(
            array(
                "id" => $id
            )
        );

        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "update";
        $viewData->firstLoad = 1;
        $viewData->item = $item;
        //İl listesi
        $viewData->cityList = $this->school_model->get_all_2("city", "id");

        $viewData->townLists = $this->school_model->get_all_3("town", array("cityid" => $item->city), "town");

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function update($id)
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtOkulAdi", "Okul Adı", "required|trim");
        $this->form_validation->set_rules("txtOkulSorumlu", "Okul Sorumlusu", "required|trim");
        $this->form_validation->set_rules("slcOkulIl", "İl", "required|trim");
        $this->form_validation->set_rules("slcOkulIlce", "İlçe", "required|trim");
        $this->form_validation->set_rules("txtOkulAdres", "Adres", "required|trim");
        $this->form_validation->set_rules("txtOkulTelefon", "Okul Telefonu", "required|trim");
        $this->form_validation->set_message(
            array("required" => "<b>{field}</b> alanını doldurunuz.")
        );
        $validation = $this->form_validation->run();
        if ($validation) {
            $update = $this->school_model->update(
                array(
                    "id" => $id
                ),
                array(
                    "school_name" => $this->input->post("txtOkulAdi"),
                    "manager" => $this->input->post("txtOkulSorumlu"),
                    "manager_phone" => $this->input->post("txtOkulSorumluTelefon"),
                    "city" => $this->input->post("slcOkulIl"),
                    "town" => $this->input->post("slcOkulIlce"),
                    "address" => $this->input->post("txtOkulAdres"),
                    "school_phone" => $this->input->post("txtOkulTelefon"),
                    "web" => $this->input->post("txtOkulWeb"),
                    "isActive" => 1,
                    "rank" => 0,
                    "createdAt" => date("Y-m-d H:i:s")
                )
            );
            if ($update) {
                $alert = array(
                    'title' => 'İşlem Başarılı',
                    'text' => 'Kayıt Başarıyla Güncellendi...',
                    'type' => 'success'
                );

            } else {
                $alert = array(
                    'title' => 'Hata Oluştu...',
                    'text' => 'Hata Oluştu...',
                    'type' => 'error'
                );
            }
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("school"));
        } else {
            $item = $this->school_model->get(
                array(
                    "id" => $id
                )
            );
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            //İl listesi
            $viewData->cityList = $this->school_model->get_all_2("city", "id");
            $viewData->item = $item;
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }


    }

    public function delete($id)
    {
        $delete = $this->school_model->delete(
            array(
                "id" => $id
            )
        );

        if ($delete) {
            redirect(base_url("school"));
        } else {
            redirect(base_url("school"));
        }
    }

    public function isActiveSetter($id)
    {
        if ($id) {
            $isActive = ($this->input->post("data") === "true") ? 1 : 0;
            $this->school_model->update(
                array(
                    "id" => $id
                ),
                array(
                    "isActive" => $isActive
                )
            );
        }
    }


    public function townList($cityId)
    {
        $townList = $this->db->where('cityid', $cityId)->order_by("town")->get('town')->result();
        echo json_encode($townList);
    }
}
