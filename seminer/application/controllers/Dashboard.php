<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("dashboard_model");
        $this->load->model("settings_model");
        $this->viewFolder = "dashboard_v";
       if(!get_active_user()){
            redirect(base_url("login"));
        }
   
    }

    public function index()
	{

        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;

        $user = get_active_user();
        $viewData->userInfo = $user;
        // Günler
        $viewData->gunler=array("PAZARTESİ","SALI","ÇARŞAMBA","PERŞEMBE","CUMA");
        // Rolü
        $rol = $this->dashboard_model->custom_get("select userrole from userroles where id=".$user->userrole);
        $this->session->set_userdata("rol",$rol);
        // Okullar
        $viewData->schools = $this->settings_model->custom_get_all("select id,school_name from schools where isActive=1");
        // Saatler
        $viewData->starttimes = $this->settings_model->custom_get_all("select distinct(starttime) as stime from course order by stime");

        //Session'a gün değeri ilk olarak userop/do_login de verildi.
        $viewData->gunID = $this->session->userdata('gun');
        $viewData->okulID = $this->session->userdata('okul');
        $viewData->saat= $this->session->userdata('saat');
        $okulID = $this->session->userdata('okul');
        $saatDeger= $this->session->userdata('saat');

        $this->db->select('course.id as courseid,seminarid,name,quota,(SELECT COUNT(courseid) FROM recourses WHERE courseid=course.id) as actual,DATE_FORMAT(startdate,"%d.%m.%Y") AS startdate ,school_name,dayofweek(startdate) as gun, TIME_FORMAT(starttime, "%H : %i") as starttime,TIME_FORMAT(endtime, "%H : %i") as endtime,fullname,seminartype,title,course.isActive as isActive');
        $this->db->from('course');
        $this->db->join('courseteacher', 'courseteacher.id=course.teacherid');
        $this->db->join('seminar', 'seminar.id=course.seminarid');
        $this->db->join('seminartype', 'seminartype.id = seminar.seminartype');
        $this->db->join('schools', 'schools.id = course.schoolid');
        if(isset($okulID)){
            if($okulID!=0)
                $this->db->where("schools.id",$okulID);
        }
        if(isset($saatDeger)){
            if($saatDeger!=0)
            $this->db->where("starttime",$saatDeger);
        }
        $this->db->where("course.isActive",1);
        $this->db->order_by("startdate","asc");
        $viewData->courses = $this->db->get()->result();
        $this->load->view("{$viewData->viewFolder}/index", $viewData);

	}

    public function courseSave()
    {
        if($_POST["courseOk"]) {

            $courseid = $this->input->post("course");
            $userid = $this->input->post("user");

            //Başvuru Sayısı
            $row = $this->dashboard_model->custom_get("select count(*) as actual from recourses where courseid=" . $courseid);
            $actual = $row->actual;

            // Kullanıcı tarafından seçilen eğitimin bilgilerinin alınması
            $row2 = $this->dashboard_model->custom_get("select seminarid,quota,startdate,seminartype from course join seminar on seminar.id=course.seminarid where course.id=" . $courseid);
            $quota = $row2->quota;
            $seminarId = $row2->seminarid;
            $stdate = $row2->startdate;
            $semtype = $row2->seminartype;

            //Aynı kursa katılım tespiti
            $row3 = $this->dashboard_model->custom_get("select count(seminarid) as seminarid from recourses join course on course.id=recourses.courseid where userid=" . $userid . " and seminarid=" . $seminarId);
            $sameactual = $row3->seminarid;

            //Aynı gün birden fazla kursa katılım tespiti
            $row4 = $this->dashboard_model->custom_get("select count(startdate) as startdate from recourses join course on course.id=recourses.courseid where userid=" . $userid . " and startdate='" . $stdate . "'");
            $startdate = $row4->startdate;

            $atolye = 0;
            $seminer = 0;

            //katılım sağlanan seminer sayısı
            if($semtype == 1){
                $row5 = $this->dashboard_model->custom_get("select count(seminartype) as seminartype from recourses join course on course.id=recourses.courseid join seminar on seminar.id=course.seminarid where seminartype=1 and userid=". $userid);
                $seminer = $row5->seminartype;
            }
            //katılım sağlanan atölye sayısı
            if($semtype == 2){
                $row5 = $this->dashboard_model->custom_get("select count(seminartype) as seminartype from recourses join course on course.id=recourses.courseid join seminar on seminar.id=course.seminarid where seminartype=2 and userid=". $userid);
                $atolye = $row5->seminartype;
            }

            if ($actual >= $quota) {   // Kontenjan Dolu
                echo "Bu eğitimin kontenjanı dolmuştur.";
            } elseif ($sameactual > 0) {  //Aynı eğitime katılma
                echo "Aynı eğitimi sadece birkez seçebilirsiniz!";
            } elseif ($startdate > 0) {   //Aynı gün birden fazla eğitime katılma
                echo "Aynı gün sadece bir eğitime katılabilirsiniz!";
            } elseif ($semtype == 1 && $seminer >= 1) {   //En fazla 1 seminer seçbilir
                echo "Sadece bir Seminer eğitimine katılabilirsiniz!";
            } elseif ($semtype == 2 && $atolye >= 2) {   //En fazla 2 atölye seçbilir
                echo "En fazla 2 Atölye eğitimine katılabilirsiniz!";
            } else {
                $insert = $this->dashboard_model->add(
                    array(
                        "courseid" => $this->input->post("course"),
                        "userid" => $this->input->post("user"),
                        "createdAt" => date("Y-m-d H:i:s")
                    )
                );
                if ($insert)
                    echo "success";
                else {
                    echo "error";
                }

            }
        }

    }

    public function sessionDegistir(){

        $gun = $this->input->post("gunID");
        $this->session->set_userdata("gun",$gun);
        location.reload(false);
    }

    public function okulDegistir(){

        $okul = $this->input->post("okulID");
        $this->session->set_userdata("okul",$okul);
        location.reload(false);
    }

       public function saatDegistir(){

        $saat = $this->input->post("saatDeger");
        $this->session->set_userdata("saat",$saat);
        location.reload(false);
    }

    public function modalCourseDetail($id){

        $this->db->select('*');
        $this->db->from('course');
        $this->db->join('seminar', 'seminar.id=course.seminarid');
        $this->db->join('schools', 'schools.id=course.schoolid');
        $this->db->join('courseteacher', 'courseteacher.id=course.teacherid');
        $this->db->join('seminartype', 'seminartype.id=seminar.seminartype');
        $this->db->where('course.id',$id);

        $data  = $this->db->get()->result();
        echo json_encode($data);

    }

}
