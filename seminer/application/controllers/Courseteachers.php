<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Courseteachers extends MY_Controller
{
    public $viewFolder = "";

    public function __construct()
    {
    parent::__construct();
    $this->viewFolder = "courseteachers_v";
    $this->load->model("courseteachers_model");

    }

    public function index() {
    $viewData = new stdClass();

    $viewData->viewFolder = $this->viewFolder;
    $viewData->subViewFolder = "list";
    $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function newForm()
    {
    if (!isAllowedWriteModule())
    {
    redirect(base_url("courseteachers"));
    }
    $viewData = new stdClass();
    $viewData->viewFolder = $this->viewFolder;
    $viewData->subViewFolder = "add";
    //Okul listesi
    $viewData->schools = $this->courseteachers_model->custom_get_all("select id,school_name from schools where isActive=1");
    $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }
    public function save()
    {
    $this->load->library("form_validation");
    $this->form_validation->set_rules("txtOgretmenAdi", "Öğretmenin Adı Soyadı", "required|trim");
    $this->form_validation->set_rules("txtOgretmenCeptel", "Cep Telefonu", "trim");
    $this->form_validation->set_rules("txtOgretmenEposta", "E-Posta", "trim|valid_email");
    
    $this->form_validation->set_message(
    array(
    "required"      => "<b>{field}</b> alanını doldurunuz.",
    "valid_email"   => "Lütfen geçerli bir e-posta adresi yazınız",
    )
    );
    $validation = $this->form_validation->run();
    if($validation){
    $insert = $this->courseteachers_model->add(
    array(
    "fullname"                  => $this->input->post("txtOgretmenAdi"),
    "phone"                     => $this->input->post("txtOgretmenCeptel"),
    "email"                     => $this->input->post("txtOgretmenEposta"),
    "isActive"                  =>1,
    "createdAt"                 =>date("Y-m-d H:i:s")
    )
    );
    if($insert){
    $alert = array(
    'title'  => 'İşlem Başarılı',
    'text'  => 'Kayıt Oluşturuldu',
    'type'  => 'success'
    );
    }else{
    $alert = array(
    'title'  => 'Hata Oluştu...',
    'text'  => 'Hata Oluştu...',
    'type'  => 'error'
    );
    }
    $this->session->set_flashdata("alert", $alert);
    redirect(base_url("courseteachers"));
    } else{
    $viewData = new stdClass();
    $viewData->viewFolder = $this->viewFolder;
    $viewData->subViewFolder = "add";
    $viewData->form_error = true;
    $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }
    }

    public function updateForm($id)
    {
    $viewData = new stdClass();
    $item = $this->courseteachers_model->get(
    array(
    "id"    =>$id
    )
    );
    $viewData->viewFolder = $this->viewFolder;
    $viewData->subViewFolder = "update";

    $viewData->item = $item;
    $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function update($id)
    {
    $this->load->library("form_validation");
    $this->form_validation->set_rules("txtOgretmenAdi", "Öğretmenin Adı Soyadı", "required|trim");
    $this->form_validation->set_rules("txtOgretmenCeptel", "Cep Telefonu", "trim");
    $this->form_validation->set_rules("txtOgretmenEposta", "E-Posta", "trim|valid_email");

    $this->form_validation->set_message(
    array(
    "required"      => "<b>{field}</b> alanını doldurunuz.",
    "valid_email"   => "Lütfen geçerli bir e-posta adresi yazınız",
    )
    );
    $validation = $this->form_validation->run();
    if($validation){
    $update = $this->courseteachers_model->update(
    array(
    "id"            =>$id
    ),
    array(
    "fullname"                  => $this->input->post("txtOgretmenAdi"),
    "phone"                     => $this->input->post("txtOgretmenCeptel"),
    "email"                     => $this->input->post("txtOgretmenEposta"),
    "isActive"                  =>1,
    "createdAt"                 =>date("Y-m-d H:i:s")
    )
    );
    if($update){
    $alert = array(
    'title'  => 'İşlem Başarılı',
    'text'  => 'Kayıt Güncellendi',
    'type'  => 'success'
    );
    }else{
    $alert = array(
    'title'  => 'Hata Oluştu...',
    'text'  => 'Hata Oluştu...',
    'type'  => 'error'
    );
    }
    $this->session->set_flashdata("alert", $alert);
    redirect(base_url("courseteachers"));
    } else{
     $viewData = new stdClass();
    $item = $this->courseteachers_model->get(
    array(
    "id"    =>$id
    )
    );
    $viewData->viewFolder = $this->viewFolder;
    $viewData->subViewFolder = "update";

    $viewData->item = $item;
    $viewData->form_error = true;
    $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }
    }

    public function delete($id){
    $delete = $this->courseteachers_model->delete(
    array(
    "id"    =>$id
    )
    );
    if($delete){
    redirect(base_url("courseteachers"));
    }else{
    redirect(base_url("courseteachers"));
    }
    }

    public function isActiveSetter($id){
    if($id){
    $isActive = ($this->input->post("data")==="true") ? 1 : 0;
    $this->courseteachers_model->update(
    array(
    "id"            =>$id
    ),
    array(
    "isActive"      =>$isActive
    )
    );
    }
    }

    public function courseteachersList(){
   
       $this->load->model("datatables_model");
    $query = $this->datatables_model->select_get2();
    $data = [];
    $i=0;
    foreach($query->result() as $r) {
    $i++;
    $sub_array = array();
    $sub_array[] = $i;
    $sub_array[] = $r->fullname;
    $sub_array[] = $r->phone;
    $sub_array[] = "<a href='" . base_url("")."courseteachers/updateForm/". $r->id . "' class='btn btn-outline btn-info custom-btn btn-xs'><i class='fa fa-edit'></i>&nbsp;Düzenle</a>&nbsp;
    <a href='" . base_url("")."courseteachers/delete/". $r->id . "'  class='btn btn-outline btn-danger btn-xs custom-btn btn-haberSil'><i class='fa fa-trash'></i>&nbsp;Sil</a>";
    $data[] = $sub_array;
    }
    $result = array(

    "recordsTotal" => $query->num_rows(),
    "recordsFiltered" => $query->num_rows(),
    "data" => $data
    );
    echo json_encode($result);
    }
}