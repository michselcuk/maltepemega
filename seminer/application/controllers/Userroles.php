<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userroles extends MY_Controller
{
    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "userroles_v";
        $this->load->model("userrole_model");


    }

    public function index()
    {
        $viewData = new stdClass();
        $user = get_active_user();
        if(isAdmin()){
            $where = array();
        }else{
            $where = array(
                "id"    => $user->id
            );
        }

        $items = $this->userrole_model->get_all(
            $where
        );


        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $viewData->items = $items;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function newForm()
    {
        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "add";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function save()
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtKullaniciRolu", "Kullanıcı Rolü", "required|trim");
        $this->form_validation->set_message(
            array(
                "required"      => "<b>{field}</b> alanını doldurunuz."
            )
        );
        $validation = $this->form_validation->run();
        if($validation){
            $insert = $this->userrole_model->add(
                array(
                    "userrole"               =>$this->input->post("txtKullaniciRolu"),
                    "isActive"          =>1,
                    "modifiedAt"         =>date("Y-m-d H:i:s")
                )
            );
            if($insert){
                redirect(base_url("userroles"));
            }else{
                echo("Hata Oluştu...");
            }
        } else{
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }

    }

    public function updateForm($id)
    {
        $viewData = new stdClass();

        $item = $this->userrole_model->get(
            array(
                "id"    =>$id
            )
        );

        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "update";
        $viewData->item = $item;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function password_updateForm($id)
    {
        $viewData = new stdClass();
        $item = $this->userrole_model->get(
            array(
                "id"    =>$id
            )
        );
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "password";
        $viewData->item = $item;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
}

    public function update($id)
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtKullaniciRoluGuncelle", "Kullanıcı Rolü", "required|trim");

        $this->form_validation->set_message(
            array(
                "required"      => "<b>{field}</b> alanını doldurunuz.",
            )
        );
        $validation = $this->form_validation->run();
        if($validation){
            $update = $this->userrole_model->update(
                array(
                    "id"                =>$id
                ),
                array(
                    "userrole"               =>$this->input->post("txtKullaniciRoluGuncelle"),
                    "isActive"          =>1,
                    "modifiedAt"         =>date("Y-m-d H:i:s")
                )
            );
            if($update){
                redirect(base_url("userroles"));
            }else{
                echo("Başarısız");
            }
        } else{
            $item = $this->userrole_model->get(
                array(
                    "id"    =>$id
                )
            );
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $viewData->item = $item;
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }


    }

    public function permissionsForm($id)
    {
        $viewData = new stdClass();
        $item = $this->userrole_model->get(
            array(
                "id"    =>$id
            )
        );
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "permissions";
        $viewData->item = $item;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function update_permissions($id){
        $permission = json_encode($this->input->post("permission"));
        $update = $this->userrole_model->update(
            array(
                "id" => $id
            ),
            array(
                "permissions" => $permission
            )
        );
        if($update){
            $alert = array(
                    'title'  => 'İşlem Başarılı',
                    'text'  => 'Kayıt Oluşturuldu',
                    'type'  => 'success'
                );
        }else{
             $alert = array(
                    'title'  => 'Hata Oluştu...',
                    'text'  => 'Hata Oluştu...',
                    'type'  => 'error'
                );
        }
          $this->session->set_flashdata("alert", $alert);
        redirect(base_url("userroles"));
    }

    public function delete($id){
        $delete = $this->userrole_model->delete(
            array(
                "id"    =>$id
            )
        );

        if($delete){
            redirect(base_url("userroles"));
        }else{
            redirect(base_url("userroles"));
        }
    }

    public function isActiveSetter($id){
        if($id){
            $isActive = ($this->input->post("data")==="true") ? 1 : 0;
            $this->userrole_model->update(
                array(
                    "id"            =>$id
                ),
                array(
                    "isActive"      =>$isActive
                )
            );
        }
    }

}
