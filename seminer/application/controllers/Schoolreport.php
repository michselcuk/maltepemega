<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schoolreport extends MY_Controller
{

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "teachersreport_v";
        $this->load->model("teachers_model");
        $this->load->library("pdf");


    }


    public function index()
    {
        $viewData = new stdClass();
        $user = get_active_user();
        $viewData->gunler = $this->teachers_model->custom_get_all("select distinct(startdate) as sdate from course");
        $viewData->egitimler = $this->teachers_model->custom_get_all("select course.id as cid,name,startdate from course join seminar on seminar.id= course.seminarid where schoolid=".$user->schoolid);

        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "schoollist";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }


    public function listele() {

            $user = get_active_user();
            $courseid = $this->input->post("slcOkulEgitimSecimi");
            

            $this->db->select('*');
            $this->db->from('recourses');
            $this->db->join('users', 'users.id=recourses.userid');
            $this->db->join('course', 'course.id=recourses.courseid');
            $this->db->join('schools', 'schools.id=users.schoolid');
            $this->db->join('seminar', 'seminar.id=course.seminarid');
            $this->db->join('seminartype', 'seminartype.id=seminar.seminartype');
            $this->db->where('course.schoolid',$user->schoolid);
            $this->db->where('recourses.courseid',$courseid);
            $this->db->order_by("fullname","asc");
            $teachersList = $this->db->get()->result();


        if($this->input->post('btnOgretmenlistele')){
            $viewData = new stdClass();
        
            $viewData->userInfo = $user;
            $viewData->egitimler = $this->teachers_model->custom_get_all("select course.id as cid,name,startdate from course join seminar on seminar.id= course.seminarid where schoolid=".$user->schoolid);
            $viewData->teachers = $teachersList;
            $viewData->slcCid = $courseid;
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "schoollist";
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);  


        }

        if($this->input->post('btnOgretmenyazdir')){
            

        $okul = $this->teachers_model->custom_get("select school_name from schools where id=".$user->schoolid);

        $html ="
        <html>
        <head>
        <style>
            @page { margin: 40px 10px 10px 10px; } 
            body{ 
                    font-family: 'DejaVu Sans,DejaVu Sans Condensed', 'DejaVu Sans Condensed';
                    font-size: 9pt;
                }
        </style>
        </head>
        <body>";
            $i=1;

            foreach ($teachersList as $list) {
                $bcolor="";
                if($i%2==0)
                    $bcolor="background-color: #efefef";
                else
                    $bcolor="";
                if($i==1) {
                    $tarih = date("d.m.Y",strtotime($list->startdate));
                    $saat1= date("H:i",strtotime($list->starttime));
                    $saat2= date("H:i",strtotime($list->endtime));
                    $html .= "
                    <h2  style='text-align: center'>$okul->school_name</h2>
                    <h4  style='text-align: center'>$list->name</h4>
                    <h4  style='text-align: center'>$tarih  /  $saat1 - $saat2</h4>
                 
                  
                  <h3 style='text-align: center'>Eğitime Katılan Öğretmenlerin Yoklama Listesi</h3>
                    <table width='100%' style='border:1px solid #666' cellpadding='2'>
                    <tr style='background-color: #bbb; height:50px'>
                    <th align='right'>No</th>
                    <th>Öğretmenin Adı Soyadı</th>
                    <th>Öğretmenin Okulu</th>
                    <th>İmza</th>
                   </tr>";
                }
                $html.="<tr style='".$bcolor."; line-height: 25px'>
                        <td  style='width:50px; text-align:right'>".$i."</td>
                        <td style='width:200px'>".$list->fullname."</td>
                        <td style='width:350px'>".$list->school_name."</td>
                        <td></td>
                        </tr>";
                        $i++;
            }
      
        $html.= "
        </table>
        </body>
        </html>";


        $this->pdf->create($html,"Okul_Ogretmenleri_Egitim_Listesi");
        }
       
    }



}

