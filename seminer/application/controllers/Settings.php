<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller
{

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "settings_v";
        $this->load->model("settings_model");
        $this->load->model("teachers_model");

    }

    public function index()
    {
        $viewData = new stdClass();
        $user = get_active_user();
        $item = $this->settings_model->get(
            array(
                "id"    =>$user->id
            )
        );

        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $user = get_active_user();
        $userrole = $user->userrole;

        if($userrole == 1) {
            //Okul listesi
            $viewData->schools = $this->teachers_model->custom_get_all("select id,school_name from schools where isActive=1");
        }else{
            //Okul listesi
            $viewData->schools = $this->settings_model->custom_get_all("select id,school_name from schools where isActive=1 and id=".$user->schoolid);
        }

          $viewData->items = $item;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }



    public function update($id)
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtOgretmenAdi", "Adı Soyadı", "required|trim");
        $this->form_validation->set_rules("slcOgretmenOkul", "Görev Yaptığı Okul", "required|trim");
        $this->form_validation->set_rules("txtOgretmenCeptel", "Cep Telefonu", "required|trim");
        $this->form_validation->set_rules("txtOgretmenEposta", "E-Posta", "trim|valid_email");
        $this->form_validation->set_message(
            array(
                "required"=>"<b>{field}</b> alanını doldurunuz.",
                "valid_email" => "Lütfen geçerli bir e-posta adresi yazınız"
            )
        );
        $validation = $this->form_validation->run();
        if($validation){
            $update = $this->settings_model->update(
                array(
                    "id"            =>$id
                ),
                array(
                    "fullname"          =>$this->input->post("txtOgretmenAdi"),
                    "schoolid"          =>$this->input->post("slcOgretmenOkul"),
                    "phone"             =>$this->input->post("txtOgretmenCeptel"),
                    "email"             =>$this->input->post("txtOgretmenEposta"),
                    "createdAt"         =>date("Y-m-d H:i:s")
                )
            );
            if($update){
                $alert = array(
                    'title'  => 'İşlem Başarılı',
                    'text'  => 'Kayıt Güncellendi',
                    'type'  => 'success'
                );
            }else{
                $alert = array(
                    'title'  => 'Hata Oluştu...',
                    'text'  => 'Hata Oluştu...',
                    'type'  => 'error'
                );
            }
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("settings"));
        } else{
            $user = get_active_user();
            $item = $this->settings_model->get(
            array(
                "id"    =>$user->id
            )
            );
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "list";
            $user = get_active_user();
            $userrole = $user->userrole;

            if($userrole == 1) {// Kullanıcı Okul Sorumlusu ise
                //Okul listesi
                $viewData->schools = $this->teachers_model->custom_get_all("select id,school_name from schools where isActive=1");
            }else{
                //Okul listesi
                $viewData->schools = $this->settings_model->custom_get_all("select id,school_name from schools where isActive=1 and id=".$user->schoolid);
            }
            $viewData->items = $item;
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }


    }

    public function updatePassword($id)
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtOgretmenSifre", "Yeni Şifre", "required|trim");
        $this->form_validation->set_rules("txtOgretmenSifre2", "Yeni Şifre(Tekrar)", "required|trim");
        $this->form_validation->set_rules("txtOgretmenSifre", "Şifre", "required|trim|min_length[8]|max_length[12]");
        $this->form_validation->set_rules("txtOgretmenSifre2", "Şifre (Tekrar)", "required|trim|min_length[8]|max_length[12]|matches[txtOgretmenSifre]");

        $this->form_validation->set_message(
            array(
            "required"      => "<b>{field}</b> alanını doldurunuz.",
            "min_length"    => "<b>{field}</b> alanı en az 8 karakterden oluşmalıdır ",
            "max_length"    => "<b>{field}</b> alanı en fazla 12 karakterden oluşmalıdır ",
            "matches"       => "Şifreler birbirleri ile aynı olmalıdır"
             )
        );
        $validation = $this->form_validation->run();
        if($validation){
            $update = $this->settings_model->update(
                array(
                    "id"            =>$id
                ),
                array(
                    "password"          =>md5($this->input->post("txtOgretmenSifre")),
                    "createdAt"         =>date("Y-m-d H:i:s")
                )
            );
            if($update){
                $alert = array(
                    'title'  => 'İşlem Başarılı',
                    'text'  => 'Şifre Güncellendi',
                    'type'  => 'success'
                );
            }else{
                $alert = array(
                    'title'  => 'Hata Oluştu...',
                    'text'  => 'Hata Oluştu...',
                    'type'  => 'error'
                );
            }
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("settings"));
        } else{
            $user = get_active_user();
            $item = $this->settings_model->get(
            array(
                "id"    =>$user->id
            )
            );
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "list";
            $user = get_active_user();
            $userrole = $user->userrole;

            if($userrole == 1) {// Kullanıcı Okul Sorumlusu ise
                //Okul listesi
                $viewData->schools = $this->teachers_model->custom_get_all("select id,school_name from schools where isActive=1");
            }else{
                //Okul listesi
                $viewData->schools = $this->settings_model->custom_get_all("select id,school_name from schools where isActive=1 and id=".$user->schoolid);
            }
            $viewData->items = $item;
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }


    }


}
