<?php
    class Project_general_user_extras extends MY_Controller{

        public $viewFolder = "";

        public function __construct()
        {
            parent::__construct();

            $this->viewFolder = "project_general_user_extras_v";

            $this->load->model("project_general_model");
            $this->load->model("project_general_extras_model");
            $this->load->model("project_general_extras_image_model");
            if(!get_active_user()){
                redirect(base_url("login"));
            }

        }

        public function index(){
            $viewData = new stdClass();

            //tablodan verilerin getirilmesi
            $user = get_active_user();
            $school_id = $user->schoolid;
            $user_id = $user->id;
            $items = $this->project_general_extras_model->project_general_extras(
                array(
                    'user_id' => $user_id
                )
            );
//            print_r($items);die();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "list";
            $viewData->items = $items;
//            $viewData->project_general = $project_generals;

//            print_r($items);
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //ekle formunu getir
        public function new_form(){
            if (!isAllowedWriteModule()){
                redirect(base_url("dashboard"));
            }
            $project_generals = $this->project_general_model->get_all(
                array()
            );
            $viewData = new stdClass();
            $viewData->project_generals = $project_generals;
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //kayıt ekle işlemi
        public function save(){
            //login kullanıcı bilgilerini al
            $user = get_active_user();
//            print_r($user);die();
            //kullancının okul id si
            $school_id=$user->schoolid;
            //kullanıcının id si
            $user_id = $user->id;

            //validate işlemleri
            $this->load->library("form_validation");
            $this->form_validation->set_rules("title","Başlık","required|trim");
            $this->form_validation->set_message(
                array(
                    "required"=>"<strong>{field}</strong> alanı doldurulmalıdır"
                )
            );
            $validate = $this->form_validation->run();

            if ($validate){

                $insert = $this->project_general_extras_model->add(
                    array(
                        "user_id"               =>  $user_id,
                        "school_id"             =>  $school_id,
                        "project_general_id"    =>$this->input->post('project_general_id'),
                        "title"                 =>$this->input->post('title'),
                        "description"           =>$this->input->post('description'),
                        "project_teachers"      =>$this->input->post('project_teachers'),
                        "project_teachers_count"=>$this->input->post('project_teachers_count'),
                        "project_students"      =>$this->input->post('project_students'),
                        "project_students_count"=>$this->input->post('project_students_count'),
                        "publish"               =>  0,
                        "createdAt"             =>  date("Y-m-d H:i:s")
                    )
                );
                //alert sistemi ekleneceek
                if ($insert){
                    $alert=array(
                        "title" => "işlem başarılı",
                        "text"  => "Kayıt başarılı bir şekilde eklendi",
                        "type"  => "success"
                    );
                }else{
                    $alert=array(
                        "title" => "işlem başarısız",
                        "text"  => "Ekleme işlemi sırasında bir sorun oluştu",
                        "type"  => "error"
                    );
                }
                //işlem sonucu session a aktar
                $this->session->set_flashdata("alert",$alert);
                redirect(base_url("project_general_user_extras"));
            }
            else {
                $viewData = new stdClass();
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "add";
                $viewData->form_error = true;
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

//                echo validation_errors();
            }

        }
        //update formunu getir
        public function update_form($id){
            if (!isAllowedUpdateModule()){
                redirect(base_url("dashboard"));
            }
            $project_generals = $this->project_general_model->get_all(
                array()
            );
            $viewData = new stdClass();
            // tablodan id'ye göre kayıtları getirme işlemi
            $item = $this->project_general_extras_model->get(
            //where in array olması birden fazla şart oluşturmak için esneklik sağlar
            // örneğin id => $id, isActive =>1 dendiğinde hem aktif hem de id'si bir olanı demektir.
                //he burda kullanılmaz o ayrı :)
                array(
                    "id" => $id
                )
            );
//            print_r($item);die();


            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $viewData->item = $item;
            $viewData->project_generals = $project_generals;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        //update işlemi
        public function update($id){
            if (!isAllowedUpdateModule()){
                redirect(base_url("dashboard"));
            }
            //validate işlemleri
            $this->load->library("form_validation");
            $this->form_validation->set_rules("title","Başlık","required|trim");
            $this->form_validation->set_message(
                array(
                    "required"=>"<strong>{field}</strong> alanı doldurulmalıdır"
                )
            );
            $validate = $this->form_validation->run();

            if ($validate){
                $update = $this->project_general_extras_model->update(
                    array(
                      "id"  =>$id
                    ),
                    array(
                        "project_general_id"    =>$this->input->post('project_general_id'),
                        "title"                 =>$this->input->post('title'),
                        "description"           =>$this->input->post('description'),
                        "project_teachers"      =>$this->input->post('project_teachers'),
                        "project_teachers_count"=>$this->input->post('project_teachers_count'),
                        "project_students"      =>$this->input->post('project_students'),
                        "project_students_count"=>$this->input->post('project_students_count'),
                        "createdAt"             =>  date("Y-m-d H:i:s")
                    )
                );
                //alert sistemi ekleneceek
                if ($update){
                    $alert=array(
                        "title" => "işlem başarılı",
                        "text"  => "Kayıt başarılı bir şekilde güncellendi",
                        "type"  => "success"
                    );
                }else{
                    $alert=array(
                        "title" => "işlem başarısız",
                        "text"  => "Güncelleme işlemi sırasında bir sorun oluştu",
                        "type"  => "error"
                    );
                }
                $this->session->set_flashdata("alert",$alert);
                redirect(base_url("project_general_user_extras"));
            }
            else {
                $viewData = new stdClass();
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "update";
                $item= $this->project_general_extras_model->get(
                    array(
                        "id" =>$id
                    )
                );
                $viewData->item = $item;
                $viewData->form_error = true;
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

//                echo validation_errors();
            }

        }
        //silme işlemi
        public function delete($id){
            if (!isAllowedDeleteModule()){
                redirect(base_url("dashboard"));
            }
            $delete = $this->project_general_extras_model->delete(
                array(
                    "id"    => $id
                )
            );
            if ($delete){
                $alert=array(
                    "title" => "işlem başarılı",
                    "text"  => "Kayıt başarılı bir şekilde silindi",
                    "type"  => "success"
                );
            }else{
                $alert=array(
                    "title" => "işlem başarısız",
                    "text"  => "Silme işlemi sırasında bir sorun oluştu",
                    "type"  => "error"
                );
            }
            $this->session->set_flashdata("alert",$alert);
            redirect(base_url("project_general_user_extras"));
        }
        //ürün aktif pasif işlemi
        public function isActiveSetter($id){
            if ($id){
                $isActive = ($this->input->post("data") === "true") ? 1 : 0;

                $this->project_general_extras_model->update(
                  array(
                      "id"          => $id
                  ),
                  array(
                      "isActive"    =>  $isActive
                  )
                );
            }
        }
        //sıralama işlemi
        public function rankSetter(){
            $data = $this->input->post("data");
            parse_str($data,$order);
//            print_r($order);
            $items = $order["ord"];
//            print_r($items);

            foreach ($items as $rank => $id) {
                $this->project_general_extras_model->update(
                  array(
                      "id"  =>  $id,
                      "rank !=" => $rank
                  ),
                  array(
                      "rank"    =>$rank
                  )

                );
                
            }
        }
        //ana sayfa da yayınlama işlemi
        public function publish($id){
            if (!isAllowedUpdateModule()){
                die();
            }
            if ($id){
                $publish = ($this->input->post("data") === "true") ? 1 : 0;

                $this->project_general_extras_model->update(
                    array(
                        "id"          => $id
                    ),
                    array(
                        "publish"    =>  $publish
                    )
                );
            }
        }
        //resim yükleme formu
        public function image_form($id){
//            print_r($id);die();
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "image";
//            echo $id; die();
            $viewData->item = $this->project_general_extras_model->get(
                array(
                    "id" => $id
                )
            );

            $viewData->item_images = $this->project_general_extras_image_model->get_all(
                array(
                    "project_general_extra_id"    => $id

                )
//                , "rank ASC"

            );
//            print_r($viewData->item_images);die();

            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

        }
        //resim yükleme işlemi
        public function image_upload($id){

        /*    $config["allowed_types"]=" jpeg|jpg|png|gif ";
            $config["max_size"]=1024;
            $config["remove_spaces"]=true;
            $config["encrypt_name"]=true;
            $imgFolder  = "project_general_extras_v";
            $config["upload_path"]="uploads/$imgFolder";

            $this->load->library("upload",$config);
            $upload=$this->upload->do_upload("file");
            if ($upload){
                $uploaded_file_name = $this->upload->data("file_name");
                $this->project_general_extras_image_model->add(
                    array(
                        "img_url"           => $uploaded_file_name,
                        "rank"              => 0,
                        "project_general_extra_id"  => $id,
                        "isActive"          => 1,
                        "isCover"           => 1,
                        "createdAt"         =>date("Y-m-d H:i:s")
                    )
                );
            }
            else{
                echo 'başarısız';
            }    */
			
			$sayi=rand(0,5000);
			$sayi1=rand(0,5000);
			$file_name = convertToSEO(pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME)). "." . pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
			$image_700x400=upload_picture($_FILES["file"]["tmp_name"],"uploads/project_general_extras_v",700,400,"$sayi$sayi1$file_name");
			if($image_700x400){
			$this->project_general_extras_image_model->add(
					array(
						"img_url" =>"$sayi$sayi1$file_name",
						"rank"              => 0,
						"project_general_extra_id"  => $id,
                        "isActive"          => 1,
                        "isCover"           => 1,
                        "createdAt"         =>date("Y-m-d H:i:s")
						)
				);


}else{

echo "no";
}   		
			
			
			
			
			
        }
        //resim listesini dinamik görüntüleme işlemi
        public function refresh_image_list($id){
//            echo $project_extra_id;die();
            $viewData = new stdClass();

            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "image";

//            $viewData->item = $this->project_general_extras_model->get(
//                array(
//                    "project_id" => $project_id
//                )
//            );
            $viewData->item_images = $this->project_general_extras_image_model->get_all(
                array(
                    "project_general_extra_id"    => $id
                )
            );
//            print_r($viewData);die();
            $render_html = $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/image_list_v", $viewData, true);
            echo $render_html;
        }
        //ürün resim aktif/pasif işlemi
        public function isImageActiveSetter($id){
            if ($id){
                $isActive = ($this->input->post("data") === "true") ? 1 : 0;

                $this->project_general_extras_image_model->update(
                    array(
                        "id"          => $id
                    ),
                    array(
                        "isActive"    =>  $isActive
                    )
                );
            }
        }
        //kapak resmi seçme işlemi ????
        public function isCoverSetter($id, $parent_id){
            if ($id && $parent_id) {
                $isCover = ($this->input->post("data") === "true") ? 1 : 0;
                //kapak yapılacak fotoya ait kayıt
                $this->project_general_extras_image_model->update(
                    array(
                        "id"            => $id,
                        "product_id"    => $parent_id
                    ),
                    array(
                        "isCover"    =>  $isCover
                    )
                );
                //kapak fotoğrafı olmayan durumlar için
                $this->product_image_model->update(
                    array(
                        "id !="         => $id,
                        "product_id"    => $parent_id
                    ),
                    array(
                        "isCover"    =>  0
                    )
                );
                $viewData = new stdClass();

                /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "image";
                // resmin ait olduğu ürün adı için product modele gidildi,

                $viewData->item = $this->product_model->get(
                    array(
                        // product_id yani parent_id product modelindeki id ye denk geliyor.
                        "id" => $parent_id
                    )
                );
                $viewData->item_images = $this->product_image_model->get_all(
                    array(
                        "product_id"    => $parent_id
                    ), "rank ASC"
                );

                $render_html = $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/image_list_v", $viewData, true);

                echo $render_html;
            }
        }
        //image sıralama işlemi
        public function imageRankSetter(){
            $data = $this->input->post("data");
            parse_str($data,$order);
//            print_r($order);
            $items = $order["ord"];
//            print_r($items);

            foreach ($items as $rank => $id) {
                $this->project_general_extras_image_model->update(
                    array(
                        "id"  =>  $id,
                        "rank !=" => $rank
                    ),
                    array(
                        "rank"    =>$rank
                    )

                );

            }
        }
        //silme işlemi
        public function imageDelete($id, $parent_id){
            $file=$this->project_general_extras_image_model->get(
                array(
                    "id"    =>$id
                )
            );

            $delete = $this->project_general_extras_image_model->delete(
                array(
                    "id"    => $id
                )
            );
            if ($delete){
                unlink("uploads/project_general_extras_v/$file->img_url");
                redirect(base_url("project_general_user_extras/image_form/$parent_id"));
            }else{
                redirect(base_url("project_general_user_extras/image_form/$parent_id"));
            }
        }

    }