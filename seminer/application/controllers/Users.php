<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller
{
    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "users_v";
        $this->load->model("user_model");
        $this->load->model("userrole_model");
      

    }

    public function index()
    {
        $viewData = new stdClass();
        $user = get_active_user();
        if(isAdmin()){
            $where = array(
                "userrole"      => 1,
                "isActive"      => 1
            );
        }else{
            $where = array(
                "id"    => $user->id
            );
        }

        $items = $this->user_model->get_all(
            $where
        );

              $viewData->items = $items;
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function newForm()
    {

        $viewData = new stdClass();
        $this->load->model("userrole_model");
        $viewData->user_roles = $this->userrole_model->get_all(
            array(
                "isActive"      => 1
            )
        );
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "add";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function save()
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtKullaniciAdi", "Adı Soyadı", "required|trim");
        $this->form_validation->set_rules("txtKullaniciCeptel", "Cep Telefonu", "required|trim");
        $this->form_validation->set_rules("txtKullaniciEposta", "E-Posta Adresi", "required|trim|valid_email");
        $this->form_validation->set_rules("slcKullaniciRolu", "Kullanıcı Rolü", "required|trim");
        $this->form_validation->set_rules("txtKullaniciSifre", "Şifre", "required|trim|min_length[6]|max_length[12]");
         $this->form_validation->set_rules("txtKullaniciSifre2", "Şifre (Tekrar)", "required|trim|min_length[6]|max_length[12]|matches[txtKullaniciSifre]");
        $this->form_validation->set_message(
            array(
                "required"      => "<b>{field}</b> alanını doldurunuz.",
                "valid_email"   => "Lütfen geçerli bir e-posta adresi yazınız",
                "min_length"    => "<b>{field}</b> alanı en az 6 karakterden oluşmalıdır ",
                "max_length"    => "<b>{field}</b> alanı en fazla 12 karakterden oluşmalıdır ",
                "matches"       => "Şifreler birbirleri ile aynı olmalıdır"
            )
        );
        $validation = $this->form_validation->run();
        if($validation){
            $insert = $this->user_model->add(
                array(
                    "fullname"               =>$this->input->post("txtKullaniciAdi"),
                    "phone"            =>$this->input->post("txtKullaniciCeptel"),
                    "email"            =>$this->input->post("txtKullaniciEposta"),
                    "userrole"         =>$this->input->post("slcKullaniciRolu"),
                    "password"             =>md5($this->input->post("txtKullaniciSifre")),
                    "isActive"          =>1,
                    "createdAt"         =>date("Y-m-d H:i:s")
                )
            );
               if($insert){
                $alert = array(
                'title'  => 'İşlem Başarılı',
                'text'  => 'Kayıt Oluşturuldu',
                'type'  => 'success'
                );
                }else{
                $alert = array(
                'title'  => 'Hata Oluştu...',
                'text'  => 'Hata Oluştu...',
                'type'  => 'error'
                );
                }
                $this->session->set_flashdata("alert", $alert);
                redirect(base_url("users"));
        } else{
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }

    }

    public function updateForm($id)
    {
        $viewData = new stdClass();

        $item = $this->user_model->get(
            array(
                "id"    =>$id
            )
        );
        $viewData->user_roles = $this->userrole_model->get_all(
            array(
                "isActive"      => 1
            )
        );

        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "update";
        $viewData->item = $item;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }


    public function password_updateForm($id)
    {
        $viewData = new stdClass();
        $item = $this->user_model->get(
            array(
                "id"    =>$id
            )
        );
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "password";
        $viewData->item = $item;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
}

    public function update($id)
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtKullaniciAdi", "Adı Soyadı", "required|trim");
        $this->form_validation->set_rules("txtKullaniciCeptel", "Cep Telefonu", "required|trim");
        $this->form_validation->set_rules("txtKullaniciEposta", "E-Posta Adresi", "required|trim|valid_email");
        $this->form_validation->set_rules("slcKullaniciRolu", "Kullanıcı Rolü", "required|trim");
        $this->form_validation->set_rules("txtKullaniciSifre", "Şifre", "required|trim|min_length[6]|max_length[12]");
         $this->form_validation->set_rules("txtKullaniciSifre2", "Şifre (Tekrar)", "required|trim|min_length[6]|max_length[12]|matches[txtKullaniciSifre]");
        $this->form_validation->set_message(
            array(
                "required"      => "<b>{field}</b> alanını doldurunuz.",
                "valid_email"   => "Lütfen geçerli bir e-posta adresi yazınız",
                "min_length"    => "<b>{field}</b> alanı en az 6 karakterden oluşmalıdır ",
                "max_length"    => "<b>{field}</b> alanı en fazla 12 karakterden oluşmalıdır ",
                "matches"       => "Şifreler birbirleri ile aynı olmalıdır"
            )
        );
        $validation = $this->form_validation->run();
        if($validation){
            $update = $this->user_model->update(
                array(
                    "id"                =>$id
                ),
                array(
                    "fullname"               =>$this->input->post("txtKullaniciAdi"),
                    "phone"            =>$this->input->post("txtKullaniciCeptel"),
                    "email"            =>$this->input->post("txtKullaniciEposta"),
                    "userrole"         =>$this->input->post("slcKullaniciRolu"),
                    "password"             =>md5($this->input->post("txtKullaniciSifre")),
                    "isActive"          =>1,
                    "createdAt"         =>date("Y-m-d H:i:s")
                )
            );
            if($update){
                 $alert = array(
                'title'  => 'İşlem Başarılı',
                'text'  => 'Kayıt Güncellendi',
                'type'  => 'success'
                );
            }else{
                $alert = array(
                'title'  => 'Hata Oluştu...',
                'text'  => 'Hata Oluştu...',
                'type'  => 'error'
                );
            }
             $this->session->set_flashdata("alert", $alert);
                redirect(base_url("users"));
        } else{
            $item = $this->user_model->get(
                array(
                    "id"    =>$id
                )
            );
            $viewData = new stdClass();
            $viewData->user_roles = $this->userrole_model->get_all(
                array(
                    "isActive"      => 1
                )
            );
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $viewData->item = $item;
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }


    }

    public function update_password($id)
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("txtKullaniciSifre", "Şifre", "required|trim|min_length[6]|max_length[10]");
        $this->form_validation->set_rules("txtKullaniciSifre_Tekrar", "Şifre Tekrar", "required|trim|min_length[6]|max_length[10]|matches[txtKullaniciSifre]");
        $this->form_validation->set_rules("txtKullaniciSifre_Tekrar", "Şifre Tekrar", "required|trim|min_length[6]|max_length[10]|matches[txtKullaniciSifre]");
        $this->form_validation->set_message(
            array(
                "required"      => "<b>{field}</b> alanını doldurunuz.",
                "min_length"    => "<b>{field}</b> alanı en az 6 karakterden oluşmalıdır ",
                "max_length"    => "<b>{field}</b> alanı en fazla 10 karakterden oluşmalıdır ",
                "matches"       => "Şifreler birbirleri ile aynı olmalıdır"
            )
        );
        $validation = $this->form_validation->run();
        if($validation){
            $update = $this->user_model->update(
                array(
                    "id"                =>$id
                ),
                array(
                    "password"             =>md5($this->input->post("txtKullaniciSifre"))
                )
            );
            if($update){
                redirect(base_url("users"));
            }else{
                echo("Başarısız");
            }
        } else{
            $item = $this->user_model->get(
                array(
                    "id"    =>$id
                )
            );
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "password";
            $viewData->item = $item;
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }


    }

    public function delete($id){
        $delete = $this->user_model->delete(
            array(
                "id"    =>$id
            )
        );

        if($delete){
            redirect(base_url("users"));
        }else{
            redirect(base_url("users"));
        }
    }

    public function isActiveSetter($id){
        if($id){
            $isActive = ($this->input->post("data")==="true") ? 1 : 0;
            $this->user_model->update(
                array(
                    "id"            =>$id
                ),
                array(
                    "isActive"      =>$isActive
                )
            );
        }
    }

    public function rankSetter(){
        $data = $this->input->post("data");
        parse_str($data,$order);
        $items = $order["ord"];

        foreach ($items as $rank=>$id){
            $this->user_model->update(
                array(
                    "id"         =>$id,
                    "rank !="    =>$rank
                ),
                array(
                    "rank"       =>$rank
                )
            );
        }


    }

}
