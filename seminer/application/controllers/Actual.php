<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actual extends MY_Controller
{

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "actual_v";
        $this->load->model("actual_model");
        $this->load->library("pdf");


    }


    public function index()
    {
        $viewData = new stdClass();

        $user = get_active_user();
        $viewData->userInfo = $user;


        $this->db->select('recourses.id as actualid,name,userid,startdate,starttime,endtime,title,school_name');
        $this->db->from('recourses');
        $this->db->join('course', 'course.id=recourses.courseid');
        $this->db->join('seminar', 'seminar.id=course.seminarid');
        $this->db->join('schools', 'schools.id=course.schoolid');
        $this->db->join('seminartype', 'seminartype.id=seminar.seminartype');
        $this->db->where('userid',$user->id);
        $this->db->order_by("startdate","asc");
        $viewData->actual = $this->db->get()->result();


        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function detail($id)
    {
        if (!isAllowedWriteModule()) {
            redirect(base_url("actual"));
        }
        $viewData = new stdClass();
        $user = get_active_user();
        $viewData->userInfo = $user;


        $this->db->select('recourses.id as actualid,name,fullname, seminar.description as description,userid,startdate,starttime,endtime,title,school_name');
        $this->db->from('recourses');
        $this->db->join('course', 'course.id=recourses.courseid');
        $this->db->join('seminar', 'seminar.id=course.seminarid');
        $this->db->join('schools', 'schools.id=course.schoolid');
        $this->db->join('seminartype', 'seminartype.id=seminar.seminartype');
        $this->db->join('courseteacher', 'courseteacher.id=course.teacherid');
        $this->db->where('userid',$user->id);
        $this->db->where('recourses.id',$id);
        $this->db->order_by("startdate","asc");
        $viewData->items = $this->db->get()->result();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "detail";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

    }

    public function delete($id)
    {
        $delete = $this->actual_model->delete(
            array(
                "id" => $id
            )
        );

        if ($delete) {
            redirect(base_url("actual"));
        } else {
            redirect(base_url("actual"));
        }
    }

    public function pdfList()
    { 

        $user = get_active_user();

        $this->db->select('recourses.id as actualid,name,userid,startdate,starttime,endtime,title,school_name');
        $this->db->from('recourses');
        $this->db->join('course', 'course.id=recourses.courseid');
        $this->db->join('seminar', 'seminar.id=course.seminarid');
        $this->db->join('schools', 'schools.id=course.schoolid');
        $this->db->join('seminartype', 'seminartype.id=seminar.seminartype');
        $this->db->where('userid',$user->id);
        $this->db->order_by("startdate","asc");
        $actualList = $this->db->get()->result();


        $html ="
        <html>
        <head>
        <style>
            @page { margin: 40px 10px 10px 10px; } 
            body{ 
                    font-family: 'DejaVu Sans,DejaVu Sans Condensed', 'DejaVu Sans Condensed';
                    font-size: 9pt;
                }
        </style>
        </head>
        <body>
        <center><h3>SEÇİLEN EĞİTİMLERİN LİSTESİ</h3></center>
        <table width='100%' style='border:1px solid #666' cellpadding='2'>
          <tr style='background-color: #bbb; height:30px'>
            <th align='right'>No</th>
            <th>Eğitim Adı</th>
            <th align='center'>Türü</th>
            <th align='center'>Tarih</th>
            <th align='center'>Saat</th>
            <th>Eğitim Yeri</th>
            </tr>";
            $i=1;
            foreach ($actualList as $list) {
                $bcolor="";
                if($i%2==0)
                    $bcolor="background-color: #efefef";
                else
                    $bcolor="";

                $html.="<tr style='".$bcolor."'>
                        <td align='right'>".$i."</td>
                        <td>".$list->name."</td>
                        <td align='center'>".$list->title."</td>
                        <td align='center'>".date("d.m.Y",strtotime($list->startdate))."</td>
                        <td align='center'>".date("H:i",strtotime($list->starttime))." - ".date("H:i",strtotime($list->endtime))."</td>
                        <td>".$list->school_name."</td>
                        </tr>";  
                        $i++;
            }
          
        $html.= "
        </table>
        </body>
        </html>";


        $this->pdf->create($html,"EgitimListesi");
        

    }

}

