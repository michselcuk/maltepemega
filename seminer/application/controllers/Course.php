<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends MY_Controller
{

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "course_v";
        $this->load->model("course_model");

    }

    public function index()
    {
        $viewData = new stdClass();

        $items = $this->course_model->custom_get_all("select course.id as courseid,name,quota,startdate,starttime,endtime,course.isActive as isActive from course join seminar on seminar.id=course.seminarid order by name asc");

        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $viewData->items = $items;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function newForm()
    {
        if (!isAllowedWriteModule())
        {
            redirect(base_url("course"));
        }
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            $viewData->seminars = $this->course_model->custom_get_all("select id,name from seminar where isActive=1");
            $viewData->schools = $this->course_model->custom_get_all("select id,school_name from schools where isActive=1");
            $viewData->teachers = $this->course_model->custom_get_all("select id,fullname from courseteacher where isActive=1");

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

    }

    public function save()
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("slcEgitimAdi", "Eğitim Seçimi", "required|trim");
        $this->form_validation->set_rules("slcEgitimYeri", "Eğitim Yeri", "required|trim");
        $this->form_validation->set_rules("slcEgitimYapan", "Eğitimi Veren Kişi", "required|trim");
        $this->form_validation->set_rules("txtEgitimKontenjan", "Kontenjan", "required|trim");
        $this->form_validation->set_rules("txtEgitimTarihi", "Eğitim Tarihi", "required|trim");
        $this->form_validation->set_rules("txtEgitimBasSaat", "Eğitim Başlangıç Saati", "required|trim");
        $this->form_validation->set_rules("txtEgitimBitSaat", "Eğitim Bitiş Saati", "required|trim");
        $this->form_validation->set_message(
            array("required"=>"<b>{field}</b> alanını doldurunuz.")
        );
        $validation = $this->form_validation->run();
        if($validation){
            $insert = $this->course_model->add(
                array(
                    "seminarid"                 =>$this->input->post("slcEgitimAdi"),
                    "schoolid"                  =>$this->input->post("slcEgitimYeri"),
                    "teacherid"                 =>$this->input->post("slcEgitimYapan"),
                    "quota"                     =>$this->input->post("txtEgitimKontenjan"),
                    "startdate"                 =>$this->input->post("txtEgitimTarihi"),
                    "starttime"                 =>$this->input->post("txtEgitimBasSaat"),
                    "endtime"                   =>$this->input->post("txtEgitimBitSaat"),
                    "description"               =>$this->input->post("txtEgitimDetay"),
                    "isActive"                  =>1,
                    "createdAt"                 =>date("Y-m-d H:i:s")
                )
            );
            if($insert){
                $alert = array(
                    'title'  => 'İşlem Başarılı',
                    'text'  => 'Kayıt Oluşturuldu',
                    'type'  => 'success'
                );

            }else{
                $alert = array(
                    'title'  => 'Hata Oluştu...',
                    'text'  => 'Hata Oluştu...',
                    'type'  => 'error'
                );

            }
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("course"));

        } else{
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            $viewData->seminars = $this->course_model->custom_get_all("select id,name from seminar where isActive=1");
            $viewData->schools = $this->course_model->custom_get_all("select id,school_name from schools where isActive=1");
            $viewData->teachers = $this->course_model->custom_get_all("select id,fullname from courseteacher where isActive=1");

            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }

    }

    public function updateForm($id)
    {
        $viewData = new stdClass();

        $item = $this->course_model->get(
            array(
                "id"    =>$id
            )
        );

        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "update";
        $viewData->seminars = $this->course_model->custom_get_all("select id,name from seminar where isActive=1");
        $viewData->schools = $this->course_model->custom_get_all("select id,school_name from schools where isActive=1");
        $viewData->teachers = $this->course_model->custom_get_all("select id,fullname from courseteacher where isActive=1");
        $viewData->item = $item;
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function update($id)
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("slcEgitimAdi", "Eğitim Seçimi", "required|trim");
        $this->form_validation->set_rules("slcEgitimYeri", "Eğitim Yeri", "required|trim");
        $this->form_validation->set_rules("slcEgitimYapan", "Eğitimi Veren Kişi", "required|trim");
        $this->form_validation->set_rules("txtEgitimKontenjan", "Kontenjan", "required|trim");
        $this->form_validation->set_rules("txtEgitimTarihi", "Eğitim Tarihi", "required|trim");
        $this->form_validation->set_rules("txtEgitimBasSaat", "Eğitim Başlangıç Saati", "required|trim");
        $this->form_validation->set_rules("txtEgitimBitSaat", "Eğitim Bitiş Saati", "required|trim");
        $this->form_validation->set_message(
            array("required"=>"<b>{field}</b> alanını doldurunuz.")
        );
        $validation = $this->form_validation->run();
        if($validation){
            $update = $this->course_model->update(
                array(
                    "id"            =>$id
                ),
                array(
                    "seminarid"                 =>$this->input->post("slcEgitimAdi"),
                    "schoolid"                  =>$this->input->post("slcEgitimYeri"),
                    "teacherid"                 =>$this->input->post("slcEgitimYapan"),
                    "quota"                     =>$this->input->post("txtEgitimKontenjan"),
                    "startdate"                 =>$this->input->post("txtEgitimTarihi"),
                    "starttime"                 =>$this->input->post("txtEgitimBasSaat"),
                    "endtime"                   =>$this->input->post("txtEgitimBitSaat"),
                    "description"               =>$this->input->post("txtEgitimDetay"),
                    "isActive"                  =>1,
                    "createdAt"                 =>date("Y-m-d H:i:s")
                )
            );
            if($update){
                $alert = array(
                    'title'  => 'İşlem Başarılı',
                    'text'  => 'Kayıt Oluşturuldu',
                    'type'  => 'success'
                );
            }else{
                $alert = array(
                    'title'  => 'Hata Oluştu...',
                    'text'  => 'Hata Oluştu...',
                    'type'  => 'error'
                );
            }
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("course"));
        } else{
            $item = $this->course_model->get(
                array(
                    "id"    =>$id
                )
            );
            $viewData = new stdClass();
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $viewData->seminars = $this->course_model->custom_get_all("select id,name from seminar where isActive=1");
            $viewData->schools = $this->course_model->custom_get_all("select id,school_name from schools where isActive=1");
            $viewData->teachers = $this->course_model->custom_get_all("select id,fullname from courseteacher where isActive=1");

            $viewData->item = $item;
            $viewData->form_error = true;
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }


    }

    public function delete($id){
        $delete = $this->course_model->delete(
            array(
                "id"    =>$id
            )
        );

        if($delete){
             redirect(base_url("course"));
        }else{
             redirect(base_url("course"));
        }
    }

    public function isActiveSetter($id){
        if($id){
            $isActive = ($this->input->post("data")==="true") ? 1 : 0;
            $this->course_model->update(
                array(
                    "id"            =>$id
                ),
                array(
                    "isActive"      =>$isActive
                )
            );
        }
    }

    public function rankSetter(){
        $data = $this->input->post("data");
        parse_str($data,$order);
        $items = $order["ord"];

        foreach ($items as $rank=>$id){
            $this->course_model->update(
                array(
                    "id"         =>$id,
                    "rank !="    =>$rank
                ),
                array(
                    "rank"       =>$rank
                )
            );
        }


    }
}
