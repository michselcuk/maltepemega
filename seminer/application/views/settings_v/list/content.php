    <div class="widget">
        <div id="settingsTabs" class="nav-tabs-horizontal white m-b-lg">
            <!-- tabs list -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#kisiselBilgilerim" aria-controls="stream" role="tab" data-toggle="tab">Kişisel Bilgilerim</a></li>
                
                <li role="presentation"><a href="#sifreGuncelleme" aria-controls="friends" role="tab" data-toggle="tab">Şifre Güncelleme</a></li>
            </ul><!-- .nav-tabs -->
        </div>


        <!-- Tab panes -->
        <div class="tab-content">

            <div role="tabpanel" class="tab-pane in active fade p-md" id="kisiselBilgilerim">
                  
                <div class="row" style="margin-top: 30px">
                    <form method="post" action="<?php echo(base_url("settings/update/$items->id")); ?>">
                        <div class="form-group col-md-10 col-md-offset-1">
                            <label>Adı Soyadı</label>
                            <input type="text" class="form-control" name="txtOgretmenAdi" placeholder="Öğretmenin Adı Soyadı" value="<?=$items->fullname?>">
                            <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOgretmenAdi"); ?></small>
                            <?php } ?>
                        </div>

                            
                        <div class="form-group col-md-10 col-md-offset-1">
                            <label>Görev Yaptığı Okul</label>
                            <select name="slcOgretmenOkul" class="form-control">
                                <option selected="selected" disabled>- Seçiniz -</option>
                                <?php foreach ($schools as $school) { ?>
                                <option <?php echo ($school->id == $items->schoolid) ? "selected":""; ?>  value="<?= $school->id ?>"><?= $school->school_name ?></option>
                                <?php } ?>
                            </select>
                            <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("slcOgretmenOkul"); ?></small>
                            <?php } ?>
                        </div>

                        <div class="form-group col-md-10 col-md-offset-1">
                            <label>Cep Telefonu</label>
                            <input type="text" class="form-control" name="txtOgretmenCeptel" placeholder="Öğretmenin Cep Telefonu"  value="<?=$items->phone?>">
                            <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOgretmenCeptel"); ?></small>
                            <?php } ?>
                        </div>

                    <div class="form-group col-md-10 col-md-offset-1">
                            <label>E-Posta Adresi</label>
                            <input type="text" class="form-control" name="txtOgretmenEposta" placeholder="Öğretmenin E-Posta Adresi"  value="<?=$items->email?>">
                            <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOgretmenEposta"); ?></small>
                            <?php } ?>
                        </div>

                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-outline">Güncelle</button>
                            <a href="<?php echo base_url("dashboard"); ?>" class="btn btn-danger btn-outline">Vazgeç</a>
                        </div>
                    </div> 
                      </form>
                </div>
          
            </div><!-- .tab-pane -->

            <div role="tabpanel" id="sifreGuncelleme" class="tab-pane fade p-md">
                   
                <div class="row" style="margin-top: 30px">
                    <form method="post" action="<?php echo(base_url("settings/updatePassword/$items->id")); ?>">
                        <div class="form-group col-md-10 col-md-offset-1">
                            <label>Yeni Şifre</label>
                            <input type="password" class="form-control" name="txtOgretmenSifre" placeholder="Şifre">
                            <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOgretmenSifre"); ?></small>
                            <?php } ?>
                        </div>
                        <div class="form-group col-md-10 col-md-offset-1">
                            <label>Yeni Şifre (Tekrar)</label>
                            <input type="password" class="form-control" name="txtOgretmenSifre2" placeholder="Şifre">
                            <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOgretmenSifre2"); ?></small>
                            <?php } ?>
                        </div>

                
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-outline">Güncelle</button>
                            <a href="<?php echo base_url("dashboard"); ?>" class="btn btn-danger btn-outline">Vazgeç</a>
                        </div>
                    </div>  
                </form>
                    </div>

            </div><!-- .tab-pane -->
            </div>
        </div>
   

<script type="text/javascript">
$(document).ready(function() {
    // Tab nesnesinin sayfa güncellendiğinde en son tabda kalması
    $('#settingsTabs a[href="' + activeTab + '"]').tab('show');
    $('a[data-toggle="tab"]').on('click', function(e) {
    localStorage.setItem("activeTab", $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if (activeTab) {
    $('#settingsTabs a[href="' + activeTab + '"]').tab('show');
    localStorage.removeItem("activeTab");
    }
    // Tab son
    });
</script>