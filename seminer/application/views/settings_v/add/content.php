<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Yeni Okul Ekle</h4>
    </div>
    <div class="col-md-12">
        <div class="widget">
			<div class="widget-body">
				<form method="post" action="<?php echo(base_url("school/save")); ?>">

					    <div class="form-group">
								<label>Okul Adı</label>
                                <input type="text" class="form-control" name="txtOkulAdi" placeholder="Okul Adı">
								<?php if(isset($form_error)){ ?>
								<small class="input-form-error"><?= form_error("txtOkulAdi"); ?></small>
								<?php } ?>
						</div>
                        <div class="form-group">
                            <label>Okul Sorumlusu</label>
                            <input type="text" class="form-control" name="txtOkulSorumlu" placeholder="Okul Sorumlusunun Adı Soyadı">
                            <?php if(isset($form_error)){ ?>
                                <small class="input-form-error"><?= form_error("txtOkulSorumlu"); ?></small>
                            <?php } ?>
                        </div>

                        <div class="form-group">
                            <label>Okul Sorumlusu Telefon</label>
                            <input type="text" class="form-control" name="txtOkulSorumluTelefon" placeholder="Okul Sorumlusunun Cep Telefonu">
                            <?php if(isset($form_error)){ ?>
                                <small class="input-form-error"><?= form_error("txtOkulSorumluTelefon"); ?></small>
                            <?php } ?>
                        </div>

                    <div class="form-group">
                        <label>İl</label>
                        <select id="slcOkulIl" name="slcOkulIl" class="form-control">
                            <option selected="selected" disabled>- İl Seçiniz -</option>
                            <?php foreach ($cityList as $city) { ?>
                                <option value="<?= $city->id ?>"><?= $city->cityname ?></option>
                            <?php } ?>
                        </select>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error pull-right"><?= form_error("slcOkulIl"); ?></small>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label>İlçe</label>
                        <select id="slcOkulIlce" name="slcOkulIlce" class="form-control">
                            <option selected="selected" disabled>- İlçe Seçiniz -</option>
                        </select>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error pull-right"><?= form_error("slcOkulIlce"); ?></small>
                        <?php } ?>
                    </div>

						<div class="form-group">
								<label>Adres</label>
                                <textarea class="form-control" name="txtOkulAdres" placeholder="Okul Adresi"></textarea>
								<?php if(isset($form_error)){ ?>
								<small class="input-form-error"><?= form_error("txtOkulAdres"); ?></small>
								<?php } ?>
						</div>

                        <div class="form-group">
                            <label>Okul Telefonu</label>
                            <input type="text" class="form-control" name="txtOkulTelefon" placeholder="Okul Telefonu">
                            <?php if(isset($form_error)){ ?>
                                <small class="input-form-error"><?= form_error("txtOkulTelefon"); ?></small>
                            <?php } ?>
                        </div>


                        <div class="form-group">
                            <label>Web Sitesi</label>
                            <input type="text" class="form-control" name="txtOkulWeb" placeholder="Okulun Web Sitesi">
                            <?php if(isset($form_error)){ ?>
                                <small class="input-form-error"><?= form_error("txtOkulWeb"); ?></small>
                            <?php } ?>
                        </div>





                        <button type="submit" class="btn btn-primary btn-outline">Kaydet</button>
                        <a href="<?php echo base_url("haber"); ?>" class="btn btn-danger btn-outline">İptal</a>

                </form>
            </div><!-- .widget-body -->
		</div><!-- .widget -->

    </div>
</div>


