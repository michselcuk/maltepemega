<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Haber Düzenle</h4>
    </div>
    <div class="col-md-12">
		<div class="widget">
			<div class="widget-body">
				<form method="post"	action="<?php echo(base_url("haber/update/$item->id")); ?>">
                    <div class="form-group">
                        <label>Haber Başlığı</label>
                        <input type="text" class="form-control" name="txtHaberBaslik" placeholder="Haber Başlığı" value="<?= $item->title;?>">
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtHaberBaslik"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Kısa Açıklama</label>
                        <textarea class="form-control" name="txtKisaAciklama" placeholder="Haber Hakkında Kısa Açıklama Yazınız"><?= $item->description;?></textarea>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtKisaAciklama"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Haber Detayı</label>
                        <textarea name="txtHaberDetay" class="m-0" data-plugin="summernote" data-options="{height: 250}"><?= $item->detail;?></textarea>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtHaberDetay"); ?></small>
                        <?php } ?>
                    </div>

                    <button type="submit" class="btn btn-success btn-outline">Güncelle</button>
                    <a href="<?php echo base_url("haber"); ?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- .widget -->
</div><!-- END column -->



