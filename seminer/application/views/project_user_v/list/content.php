<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Öğretmen Proje Listesi
            <a href="<?=base_url("project_user/new_form")?>" class="btn btn-outline btn-sm btn-primary pull-right">
                <i class="fa fa-plus"></i>
                İlçe Projesi Ekle
            </a>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget p-lg">
            <?php if (empty($items)) { ?>
                <div class="alert alert-info text-center">
                    <p>Burada herhangi bir veri bulunmamaktadır. Eklemek için lütfen
                        <a href="<?=base_url("project_user/new_form")?>">tıklayınız.</a>
                    </p>
                </div>
            <?php  } else { ?>
                <table class="table table-striped table-bordered table-hover content-container">
                    <thead>
                    <tr>
                        <th><i class="fa fa-reorder"></i> </th>
                        <th>id</th>
                        <th>Başlık</th>
                        <th>Kısa Açıklama</th>

                        <!--
                        <th>Projeye Katılan Öğretmenler</th>
                        <th>Projeye Katılan Öğretmen Sayısı</th>
                        <th>Projeye Katılan Öğrenciler</th>
                        <th>Projeye Katılan Öğrenci Sayısı</th>
                        -->
                        <th>Okul Onay</th>
                        <th>İlçe Onay</th>
                        <th style="width:15%;">İşlem</th>
                    </tr>
                    </thead>
                    <tbody class="sortable" data-url="<?=base_url("project_user/rankSetter");?>" >
                    <?php foreach ($items as $item) { ?>

                        <tr id="ord-<?=$item->id; ?>" >
                            <td><i class="fa fa-reorder"></i></td>
                            <td><?=$item->id; ?></td>
                            <td><?=$item->title; ?></td>

                            <td><?=$item->short_description; ?></td>


                           <!--
                            <td><?/*=$item->project_teachers; */?></td>
                            <td><?/*=$item->project_teachers_count; */?></td>
                            <td><?/*=$item->project_students; */?></td>
                            <td><?/*=$item->project_students_count; */?></td>
                           -->

                            <td>
                                <input id="switch-2-2" type="checkbox"
                                       readonly
                                       data-switchery data-color="#10c469"
                                    <?php echo($item->school_confirm) ? 'checked' : ''; ?>
                                />
                            </td>
                            <td>
                                <input id="switch-2-2" type="checkbox"
                                       readonly
                                       data-switchery data-color="#10c469"
                                    <?php echo($item->admin_confirm) ? 'checked' : ''; ?>
                                />
                            </td>
                            <td style="width: 20%;">
                                <button data-url="<?=base_url("project_user/delete/$item->id")?>"
                                        class="btn btn-sm btn-danger btn-outline remove-btn">
                                    <i class="fa fa-trash"></i>
                                    Sil
                                </button>
                                <a href="<?=base_url("project_user/update_form/$item->id")?>" class="btn btn-sm btn-info btn-outline">
                                    <i class="fa fa-pencil-square-o"></i>
                                    Güncelle
                                </a>
                                <a href="<?=base_url("project_extras/index/$item->id");?>" class="btn btn-sm btn-dark btn-outline">
                                    <i class="fa fa-image"></i>
                                    İşlemler
                                </a>
                                <a href="<?=base_url("report/index/$item->id")?>" class="btn btn-sm btn-success btn-outline">
                                    <i class="fa fa-line-chart"></i>
                                    Rapor
                                </a>
                            </td>
                        </tr>
                    <?php } ?>

                    </tbody>

                </table>
            <?php } ?>
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
