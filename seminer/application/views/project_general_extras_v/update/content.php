<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Projeye Yapılan İşlemi Güncelle
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">

            <div class="widget-body">

                <form action="<?=base_url("project_general_extras/update/$item->id")?>" method="post">
                    <div class="form-group">
                        <label>Proje Adı</label>
                        <select class="form-control" name="project_general_id">
                            <?php foreach ($project_generals as $project_general) { ?>
                                <option value="<?=$project_general->id;?>"
                                <?=($project_general->id == $item->project_general_id) ? 'selected' : '';?>
                                >
                                    <?=$project_general->title ;?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Yapılan İşlem Başlık</label>
                        <input type="text" class="form-control"
                               name="title" value="<?=$item->title ;?>">
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("title");?>
                            </strong>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label>Projeye Katılan Öğretmenler</label>
                        <input type="text" class="form-control"
                               name="project_teachers" value="<?=$item->project_teachers ;?>">
                    </div>

                    <div class="form-group">
                        <label>Projeye Katılan Öğretmen Sayısı</label>
                        <input type="number" class="form-control"
                               name="project_teachers_count" value="<?=$item->project_teachers_count ;?>">
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("project_teachers_count");?>
                            </strong>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label>Projeye Katılan Öğrenciler</label>
                        <input type="text" class="form-control"
                               name="project_students" value="<?=$item->project_students ;?>">
                    </div>

                    <div class="form-group">
                        <label>Projeye Katılan Öğrenci Sayısı</label>
                        <input type="number" class="form-control"
                               name="project_students_count" value="<?=$item->project_students_count ;?>">
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("project_students_count");?>
                            </strong>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <label>Açıklama</label><br>
                        <textarea class="m-0"
                                  name="description"
                                  data-plugin="summernote"
                                  data-options="{height: 250}"><?=$item->description;?></textarea>
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("description");?>
                            </strong>
                        </div>
                    <?php } ?>
                    <button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
                    <a href="<?=base_url("project_general_extras");?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
