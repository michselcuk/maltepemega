<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            İlçe Proje Listesi
            <a href="<?=base_url("project/new_form")?>" class="btn btn-outline btn-sm btn-primary pull-right">
                <i class="fa fa-plus"></i>
                İlçe Projesi Ekle
            </a>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget p-lg">
            <?php if (empty($items)) { ?>
                <div class="alert alert-info text-center">
                    <p>Burada herhangi bir veri bulunmamaktadır. Eklemek için lütfen
                        <a href="<?=base_url("project/new_form")?>">tıklayınız.</a>
                    </p>
                </div>
            <?php  } else { ?>
                <table class="table table-striped table-bordered table-hover content-container">
                    <thead>
                    <tr>
                        <th><i class="fa fa-reorder"></i> </th>
                        <th>id</th>
                        <th>Başlık</th>
                        <th>Projeyi Hazırlayan Öğretmen</th>
                        <th>Projeyi Hazırlayan Okul</th>
                        <th>Kısa Açıklama</th>
                        <!--
                        <th>Projeye Katılan Öğretmenler</th>
                        <th>Projeye Katılan Öğretmen Sayısı</th>
                        <th>Projeye Katılan Öğrenciler</th>
                        <th>Projeye Katılan Öğrenci Sayısı</th>
                        -->
                        <th>Sitede Yayınla</th>
                        <th>Okul Onay</th>
                        <th>İlçe Onay</th>
                        <th style="width:15%;">İşlem</th>
                    </tr>
                    </thead>
                    <tbody class="sortable" data-url="<?=base_url("project/rankSetter");?>" >
                    <?php foreach ($items as $item) { ?>

                        <tr id="ord-<?=$item->id; ?>" >
                            <td><i class="fa fa-reorder"></i></td>
                            <td><?=$item->id; ?></td>
                            <td><?=$item->title; ?></td>
                            <td><?=$item->fullname; ?></td>
                            <td><?=$item->school_name; ?></td>


                            <td><?=$item->short_description; ?></td>
                           <!--
                            <td><?/*=$item->project_teachers; */?></td>
                            <td><?/*=$item->project_teachers_count; */?></td>
                            <td><?/*=$item->project_students; */?></td>
                            <td><?/*=$item->project_students_count; */?></td>
                           -->
                            <td>
                                <input id="switch-2-2" type="checkbox"
                                       data-url="<?=base_url("project/publish/$item->id");?>"
                                       class="publish"
                                       data-switchery data-color="#10c469"
                                    <?php echo($item->publish) ? 'checked' : ''; ?>
                                />
                            </td>
                            <td>
                                <input id="switch-2-2" type="checkbox"
                                       data-url="<?=base_url("project/school_confirm/$item->id");?>"
                                       class="school_confirm"
                                       data-switchery data-color="#10c469"
                                    <?php echo($item->school_confirm) ? 'checked' : ''; ?>
                                />
                            </td>
                            <td>
                                <input id="switch-2-2" type="checkbox"
                                       data-url="<?=base_url("project/admin_confirm/$item->id");?>"
                                       class="admin_confirm"
                                       data-switchery data-color="#10c469"
                                    <?php echo($item->admin_confirm) ? 'checked' : ''; ?>
                                />
                            </td>
                            <td style="width: 20%;">
                                <button data-url="<?=base_url("project/delete/$item->id")?>"
                                        class="btn btn-xs btn-danger btn-outline remove-btn">
                                    <i class="fa fa-trash"></i>
                                    Sil
                                </button>
                                <a href="<?=base_url("project/update_form/$item->id")?>" class="btn btn-xs btn-info btn-outline">
                                    <i class="fa fa-pencil-square-o"></i>
                                    Güncelle
                                </a>
                                <a href="<?=base_url("project_extras/index/$item->id");?>" class="btn btn-xs btn-dark btn-outline">
                                    <i class="fa fa-image"></i>
                                    İşlemler
                                </a>
                                <a href="<?=base_url("report/index/$item->id")?>" class="btn btn-xs btn-success btn-outline">
                                    <i class="fa fa-line-chart"></i>
                                    Rapor
                                </a>
                            </td>
                        </tr>
                    <?php } ?>

                    </tbody>

                </table>
            <?php } ?>
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
