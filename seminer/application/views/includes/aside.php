<?php $user = get_active_user(); ?>

<aside id="menubar" class="menubar light">
    <div class="app-user">
        <div class="media">
            <div class="media-left">
                <i class="fa fa-user" style="margin-right:20px"></i>
                <!--
                <div class="avatar avatar-md avatar-circle">
                    <a href="javascript:void(0)"><img class="img-responsive" src="<echo base_url("assets");/assets/images/users.png" alt="Kullanıcı"/></a>
                </div>--><!-- .avatar -->
            </div>

            <div class="media-body">
                <div class="foldable">
                    <h5><a href="javascript:void(0)" style="color:#ddd"
                           class="username"><?= $user->fullname; ?></a></h5>
                </div>
            </div><!-- .media-body -->
        </div><!-- .media -->
       <small style="margin-left: 30px; color:#ababab;"><?= $this->session->userdata('rol')->userrole; ?></small>
    </div><!-- .app-user -->


    <div class="menubar-scroll">
        <div class="menubar-scroll-inner">
            <ul class="app-menu">

                <!-- Anasayfa -->
                <li>
                    <a href="<?php echo base_url(""); ?>">
                        <i class="menu-icon  fa fa-home fa-lg"></i>
                        <span class="menu-text">Anasayfa</span>
                    </a>
                </li>

                <!-- Eğitimler -->
                <li class="has-submenu">
                    <a href="javascript:void(0)" class="submenu-toggle">
                        <i class="menu-icon zmdi zmdi-shape zmdi-hc-lg"></i>
                        <span class="menu-text">Eğitimler</span>
                        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                    </a>
                    <ul class="submenu">

                        <?php if (isAllowedViewModule("actual")) { ?>
                            <li>
                                <a href="<?php echo base_url("basvurulan-egitimler"); ?>">
                                    <span class="menu-text">Başvurulan Eğitimler</span>
                                </a>
                            </li>
                        <?php } ?>
                        <!-- Katıldığım Eğitimler -->
                        <?php if (isAllowedViewModule("courselist")) { ?>
                            <li>
                                <a href="<?php echo base_url("courselist"); ?>">
                                    <span class="menu-text">Tüm Eğitimler</span>
                                </a>
                            </li>
                        <?php } ?>

                    </ul>
                </li>

                <!-- Projeler -->
                <li class="has-submenu">
                    <a href="javascript:void(0)" class="submenu-toggle">
                        <i class="menu-icon fa fa-cubes"></i>
                        <span class="menu-text">Projeler</span>
                        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                    </a>
                    <ul class="submenu">

                        <?php if (isAllowedViewModule("project_general")) { ?>
                            <li>
                                <a href="<?= base_url("project_general") ?>">
                                    <span class="menu-text">İl Projeleri</span>
                                </a>
                            </li>
                        <?php } ?>
                        <li class="has-submenu">
                            <a href="javascript:void(0)" class="submenu-toggle">
                                <i class="menu-icon zmdi zmdi-apps zmdi-hc-lg"></i>
                                <span class="menu-text">İl Projeleri</span>
                                <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                            </a>
                            <ul class="submenu">
                                <?php if (isAllowedViewModule("project_general_extras")) { ?>
                                    <li>
                                        <a href="<?= base_url("project_general_extras") ?>">
                                            <span class="menu-text">İl Proje Çalışmaları (İlçe)</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if (isAllowedViewModule("project_general_school_extras")) { ?>
                                    <li>
                                        <a href="<?= base_url("project_general_school_extras") ?>">
                                            <span class="menu-text">İl Proje Çalışmaları (Okul)</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if (isAllowedViewModule("project_general_user_extras")) { ?>
                                    <li>
                                        <a href="<?= base_url("project_general_user_extras") ?>">
                                            <span class="menu-text">İl Proje Çalışmaları (Öğretmen)</span>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="has-submenu">
                            <a href="javascript:void(0)" class="submenu-toggle">
                                <i class="menu-icon zmdi zmdi-apps zmdi-hc-lg"></i>
                                <span class="menu-text">İlçe Projeleri</span>
                                <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                            </a>
                            <ul class="submenu">
                                <?php if (isAllowedViewModule("project")) { ?>
                                    <li>
                                        <a href="<?= base_url("project") ?>">
                                            <span class="menu-text">Tüm Projeler (İlçe)</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if (isAllowedViewModule("project_school")) { ?>
                                    <li>
                                        <a href="<?= base_url("project_school") ?>"">
                                        <span class="menu-text">Projeler (Okul)</span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if (isAllowedViewModule("project_user")) { ?>
                                    <li>
                                        <a href="<?= base_url("project_user") ?>"">
                                        <span class="menu-text">Projeler (Öğretmen)</span>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </li>

                <!-- Listeler -->
                <?php if($user->userrole != 2){ ?>
                <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-format-list-bulleted zmdi-hc-lg"></i>
                            <span class="menu-text">Listeler</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">

                            <?php if (isAllowedViewModule("checkteachers")) { ?>
                            <li><a href="<?php echo base_url("checkteachers"); ?>"><span class="menu-text">Yoklama İşlemleri</span></a>
                            </li>
                            <?php } ?>

                            <?php if (isAllowedViewModule("schoolreport")) { ?>
                            <li><a href="<?php echo base_url("schoolreport"); ?>"><span class="menu-text">Katılımcı Öğretmenler</span></a>
                            </li>
                            <?php } ?>
                            <?php if (isAllowedViewModule("teachersreport")) { ?>
                            <li><a href="<?php echo base_url("teachersreport"); ?>"><span class="menu-text">Okul Öğretmenleri</span></a>
                            </li>
                            <?php } ?>
                              <?php if (isAllowedViewModule("missingreport")) { ?>
                            <li><a href="<?php echo base_url("missingreport"); ?>"><span class="menu-text">Eksik Eğitim Seçenler</span></a>
                            </li>
                            <?php } ?>
                             <?php if (isAllowedViewModule("teachersreportend")) { ?>
                            <li><a href="<?php echo base_url("teachersreportend"); ?>"><span class="menu-text">Okul Öğretmen Yoklaması</span></a>
                            </li>
                            <?php } ?>


                        </ul>
                    </li>
                <?php }  ?>

                <!-- Bilgi Giriş İşlemleri İşlemleri -->
                <?php if($user->userrole != 2){ ?>
                <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon fa fa-keyboard-o"></i>
                            <span class="menu-text">Bilgi Giriş İşlemleri</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <?php if (isAllowedViewModule("seminar")) { ?>
                            <li><a href="<?php echo base_url("seminar"); ?>"><span class="menu-text">Eğitimler</span></a></li>
                            <?php } ?>
                            <?php if (isAllowedViewModule("school")) { ?>
                            <li><a href="<?php echo base_url("school"); ?>"><span class="menu-text">Okullar</span></a></li>
                            <?php } ?>
                            <?php if (isAllowedViewModule("teachers")) { ?>
                            <li><a href="<?php echo base_url("ogretmen-listesi"); ?>"><span class="menu-text">Öğretmenler</span></a></li>
                            <?php } ?>
                            <?php if (isAllowedViewModule("courseteachers")) { ?>
                            <li><a href="<?php echo base_url("courseteachers"); ?>"><span class="menu-text">Eğitimciler</span></a></li>
                            <?php } ?>
                            <!-- Eğitim Oluştur -->
                            <?php if (isAllowedViewModule("course")) { ?>
                                <li><a href="<?php echo base_url("course"); ?>"><span class="menu-text">Eğitim Oluştur</span></a></li>
                            <?php } ?>


                        </ul>
                    </li>
                <?php }  ?>

                <!-- Sistem Yönetimi -->
                <?php if($user->userrole == 1){ ?>
                <li class="has-submenu">
                    <a href="javascript:void(0)" class="submenu-toggle">
                        <i class="menu-icon zmdi zmdi-settings zmdi-hc-lg""></i>
                        <span class="menu-text">Sistem Yönetimi</span>
                        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                    </a>
                    <ul class="submenu">
                        <?php if (isAllowedViewModule("users")) { ?>
                            <!-- Kullanıcılar -->
                            <li>
                                <a href="<?php echo base_url("users"); ?>">
                                    <span class="menu-text">Site Yöneticileri</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if (isAllowedViewModule("userroles")) { ?>
                            <!-- Kullanıcı Rolleri -->
                            <li>
                                <a href="<?php echo base_url("userroles"); ?>">
                                    <span class="menu-text">Kullanıcı Rolleri</span>
                                </a>
                            </li>
                        <?php } ?>

                    </ul>
                </li>
                <?php }  ?>

                <!-- Ayarlar -->
                <?php if (isAllowedViewModule("settings")) { ?>
                <li>
                    <a href="<?php echo base_url("hesap-ayarlari"); ?>">
                        <i class="menu-icon fa fa-sliders fa-lg "></i>
                        <span class="menu-text">Hesap Ayarlarım</span>
                    </a>
                </li>
                    <?php } ?>

                <!-- Güvenli Çıkış -->
                <li>
                    <a href="<?php echo base_url("userop/logout"); ?>">
                        <i class="menu-icon fa fa-sign-out fa-lg"></i>
                        <span class="menu-text">Güvenli Çıkış</span>
                    </a>
                </li>


            </ul><!-- .app-menu -->
        </div><!-- .menubar-scroll-inner -->
    </div><!-- .menubar-scroll -->
</aside>