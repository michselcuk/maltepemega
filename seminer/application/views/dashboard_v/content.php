<div class="panel panel-info">
    <div class="panel-heading">
        <h4 class="panel-title"><i class="menu-icon fa fa-filter"></i>&nbsp;&nbsp;Eğitimleri Filtrele</h4>
    </div>
    <div class="panel-body">
<div class="col-md-3">
        <label>Eğitim Günü Seçiniz</label>
        <select name="slcEgitimGunu" class="form-control" id="slcEgitimGunu">
            <?php
            $i = 2;
            foreach ($gunler as $gun) { ?>
                <option <?php echo ($i == $gunID) ? "selected" : ""; ?> value="<?= $i; ?>"><?= $gun; ?></option>
                <?php
                $i++;
            } ?>
        </select>&nbsp;
</div>
<div class="col-md-3">
        <label>Okul Seçiniz</label>
        <select name="slcEgitimOkulu" class="form-control" id="slcEgitimOkulu">

                <option selected value="0">- Tüm Okullar -</option>
                <?php foreach ($schools as $school) { ?>
                    <option <?php echo ($school->id == $okulID) ? "selected":""; ?>  value="<?= $school->id ?>"><?= $school->school_name ?></option>
                <?php } ?>

        </select>&nbsp;
</div>

<div class="col-md-3">
        <label>Başlama Saati Seçiniz</label>
        <select name="slcEgitimSaat" class="form-control" id="slcEgitimSaat">

                <option selected value="0">- Tüm Saatler -</option>
                <?php foreach ($starttimes as $times) { ?>
                    <option <?php echo ($times->stime == $saat) ? "selected":""; ?> value="<?= $times->stime ?>"><?= date("H:i", strtotime($times->stime)); ?></option>
                <?php } ?>

        </select>&nbsp;
</div>

    </div>
</div>

<div class="row div_widget">
    <?php
    foreach ($courses as $course) {
        if ($course->gun == $gunID) {
            ?>
            <div class="col-sm-6 col-md-4 widget-max-height">
                <div class="widget">
                    <header class="widget-header">
                        <h3 class="widget-title text-center"><?= $course->title; ?></h3>
                        <h4 class="widget-title text-center"><?= $course->startdate; ?></h4>
                    </header><!-- .widget-header -->
                    <hr class="widget-separator">
                    <div class="widget-body text-center">
                        <h4 class="m-b-md"><?= $course->name; ?></h4>
                        <h5 class="m-b-md"><i class="fa fa-user"></i>&nbsp; <?= $course->fullname; ?></h5>
                        <!--<button id="btnEgitimDetayModal" data-course-id="<?// $course->courseid; ?>"  data-toggle="modal" data-target="#exampleModal"
                                class="btn btn-dark btn-outline btn-xs">Detaylı Bilgi</button>
-->
                       <h5 class="m-b-md"><i class="fa fa-map-marker"></i>&nbsp; <?= $course->school_name; ?></h5>
                        <h5 class="m-b-md"><i class="fa fa-clock-o"></i>&nbsp; <?= $course->starttime . " - " . $course->endtime; ?></h5>
                        <br/>
                        <div class="clearfix">
                            <div class="pieprogress <?= $course->seminartype == 1 ? 'text-purple' : 'text-info'; ?>"
                                 data-plugin="circleProgress"
                                 data-value="<?= round(($course->actual) / ($course->quota), 1); ?>" data-thickness="10"
                                 data-start-angle="-300"
                                 data-empty-fill="<?= $course->seminartype == 1 ? 'rgba(206,147,216,.5)' : 'rgba(79,195,247,.5)'; ?>"
                                 data-fill="{&quot;color&quot;: &quot;<?= $course->seminartype == 1 ? '#8e24aa' : '#20A8D1'; ?>&quot;}">
                                <strong>% <?= round(100 * ($course->actual) / ($course->quota)); ?></strong>
                            </div>
                            <div class="pull-left">
                                <!-- <h3 class="m-b-xs text-left counter" data-plugin="counterUp"></h3>-->
                                <h3 class="m-b-xs text-left"><?= $course->quota - $course->actual; ?></h3>
                                <small class="text-muted">Kontenjan</small>
                            </div>
                            <div class="pull-right">
                                <h3 class="m-b-xs text-right counter"
                                    data-plugin="counterUp"><?= $course->actual; ?></h3>
                                <small class="text-muted">Başvuru</small>
                            </div>
                        </div>
                        <br/><!-- .widget-body -->
                        <button data-course-id="<?= $course->courseid; ?>" data-user-id="<?= $userInfo->id; ?>"
                                class="btn p-v-xl <?= $course->seminartype == 1 ? 'btn-purple' : 'btn-info'; ?> courseBtn">
                            Başvur
                        </button>
                    </div><!-- .widget -->
                </div>
            </div>
        <?php }
    } ?>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eğitim Detayı</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover table-striped table-responsive">
                    <tbody>

                        <tr>
                            <td>Eğitimin Adı</td>
                            <td><span id="lblEgitimAdi"></span></td>
                        </tr>
                        <tr>
                            <td>Tarih</td>
                            <td><span id="lblTarih"></span></td>
                        </tr>
                        <tr>
                            <td>Saat</td>
                            <td><span id="lblSaat"></span></td>
                        </tr>
                        <tr>
                            <td>Eğitim Yeri</td>
                            <td><label id="lblEgitimYeri"></label></td>
                        </tr>
                        <tr>
                            <td>Eğitim Türü</td>
                            <td><label id="lblEgitimTuru"></label></td>
                        </tr>
                        <tr>
                            <td>Eğitmen</td>
                            <td><span id="lblEgitmen"></span></td>
                        </tr>
                        <tr>
                            <td>Açıklama</td>
                            <td><span id="lblEgitimAciklama"></span></td>
                        </tr>
                    </tbody>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Kapat</button>

            </div>
        </div>
    </div>
</div>