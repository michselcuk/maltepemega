<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Eksik veya Hiç Seçim Yapmayan Öğretmenlerin Listesi</h4>
         <hr>


<br><br>
        <?php if(empty($missingTeacher)) {?>
        <div class="alert alert-danger" role="alert">
            <!-- <strong>Kayıt Yok! </strong><br> -->
            <span>Hiç kayıt bulunamadı...</span>
            <!-- <a href="#" class="alert-link"></a> -->
        </div>

        <?php
        } else {
            $i=1;
        ?>
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <th>No</th>
                <th>Öğretmen</th>
                <th>Seçilen Eğitim</th>
               
            </thead>
            <tbody>
            <?php foreach ($missingTeacher as $item) {
                if($item->secimsayisi<3) {
                    ?>
                    <tr>
                        <td class="text-center tdws"><?= $i++; ?></td>
                        <td><?= $item->fullname; ?></td>
                        <td><?= $item->secimsayisi; ?></td>

                    </tr>
                <?php
                }}
                ?>
            </tbody>

        </table>

        <?php } ?>
     
    </div><!-- .widget -->
</div><!-- END column -->
</div>


