
<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Okulunuzdaki Eğitime Katılan Öğretmenlerin Listesi</h4>
         <hr>
<form method="post" action="<?php echo(base_url("schoolreport/listele")); ?>">

        <div class="form-group">
            <label>Eğitim Seçiniz</label>
            <select id="slcOkulEgitimSecimi" name="slcOkulEgitimSecimi" class="form-control">
                <option selected="selected" disabled>- Seçiniz -</option>
                <?php foreach ($egitimler as $egitim) { ?>
                    <option <?php if(isset($slcCid)) echo ($egitim->cid == $slcCid) ? "selected":""; ?> value="<?= $egitim->cid ?>"><?= date("d.m.Y", strtotime($egitim->startdate)) . " Tarihli Eğitim -  " . $egitim->name;  ?></option>
                <?php } ?>
            </select>
            <?php if(isset($form_error)){ ?>
                <small class="input-form-error"><?= form_error("slcOkulEgitimSecimi"); ?></small>
            <?php } ?>
        </div>

        <input name="btnOgretmenlistele" type="submit" class="btn btn-info btn-outline" value="Öğretmenleri Listele">
        <input name="btnOgretmenyazdir" type="submit" class="btn btn-danger btn-outline" value="Listeyi İndir">
</form>

      

        <br><br>
        <?php if(empty($teachers)) {?>
        <div class="alert alert-danger" role="alert" style="margin-top: 60px">
            <!-- <strong>Kayıt Yok! </strong><br> -->
            <span>Hiç kayıt bulunamadı...</span>
            <!-- <a href="#" class="alert-link"></a> -->
        </div>

        <?php
        } else {
            $i=1;
        ?>
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <th>No</th>
                <th>Öğretmenin Adı Soyadı</th>
                <th>Öğretmenin Okulu</th>
                <th>Eğitim Adı</th>
                <th class="text-center tdws">Türü</th>
                <th class="text-center tdws">Tarih</th>
                <th class="text-center tdws">Saat</th>
               
               
            </thead>
            <tbody>
            <?php foreach ($teachers as $item) { ?>
            <tr>
                <td class="text-center tdws"><?= $i++; ?></td>
                <td><?= $item->fullname; ?></td>
                <td><?= $item->school_name; ?></td>
                <td><?= $item->name; ?></td>
                <td class="text-center tdws"><?= $item->title; ?></td>
                <td class="text-center tdws"><?= date("d.m.Y",strtotime($item->startdate)); ?></td>
                <td class="text-center tdws"><?= date("H:i",strtotime($item->starttime))." - ". date("H:i",strtotime($item->endtime)); ?></td>
                
        
            </tr>
            <?php } ?>
            </tbody>

        </table>

        <?php } ?>
     
    </div><!-- .widget -->
</div><!-- END column -->
</div>


