<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Okul Öğretmenlerinin Başvurduğu Eğitimler</h4>
         <hr>
            <a href="<?php echo base_url("teachersreport/pdfList"); ?>" class="btn btn-primary  btn-sm btn-outline pull-right"><i class="fa fa-download"></i>&nbsp;Başvuru Listesini İndir </a>
        <br><br>
        <?php if(empty($teachers)) {?>
        <div class="alert alert-danger" role="alert">
            <!-- <strong>Kayıt Yok! </strong><br> -->
            <span>Hiç kayıt bulunamadı...</span>
            <!-- <a href="#" class="alert-link"></a> -->
        </div>

        <?php
        } else {
            $i=1;
        ?>
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <th>No</th>
                <th>Öğretmen</th>
                <th>Eğitim Adı</th>
                <th class="text-center tdws">Türü</th>
                <th class="text-center tdws">Tarih</th>
                <th class="text-center tdws">Saat</th>
                <th>Eğitim Yeri</th>
               
            </thead>
            <tbody>
            <?php foreach ($teachers as $item) { ?>
            <tr>
                <td class="text-center tdws"><?= $i++; ?></td>
                <td><?= $item->fullname; ?></td>
                <td><?= $item->name; ?></td>
                <td class="text-center tdws"><?= $item->title; ?></td>
                <td class="text-center tdws"><?= date("d.m.Y",strtotime($item->startdate)); ?></td>
                <td class="text-center tdws"><?= date("H:i",strtotime($item->starttime))." - ". date("H:i",strtotime($item->endtime)); ?></td>
                <td><?= $item->school_name; ?></td>
        
            </tr>
            <?php } ?>
            </tbody>

        </table>

        <?php } ?>
     
    </div><!-- .widget -->
</div><!-- END column -->
</div>


