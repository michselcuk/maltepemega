<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg pull-left">Eğitim Detayı</h4>
        <br/>
        <a href='<?php echo(base_url("actual")); ?>'
           type="button" class="btn btn-outline btn-success btn-xs pull-right">
            <i class='fa fa-arrow-left'></i>&nbsp;Geri Dön
        </a>
        <br/>
        <hr>
        <table class="table table-bordered table-hover table-striped table-responsive">
            <tbody>
            <?php foreach ($items as $item) { ?>
                <tr>
                    <td>Eğitimin Adı</td>
                    <td><?= $item->name; ?></td>
                </tr>
                <tr>
                    <td>Tarih</td>
                    <td><?= $item->startdate; ?></td>
                </tr>
                <tr>
                    <td>Saat</td>
                    <td><?= date("H:i",strtotime($item->starttime))." - ".date("H:i",strtotime($item->endtime));?></td>
                </tr>
                <tr>
                    <td>Eğitim Yeri</td>
                    <td><?= $item->school_name; ?></td>
                </tr>
                <tr>
                    <td>Eğitim Türü</td>
                    <td><?= $item->title; ?></td>
                </tr>

            <?php } ?>
            </tbody>

        </table>

    </div><!-- .widget -->
</div><!-- END column -->
</div>


