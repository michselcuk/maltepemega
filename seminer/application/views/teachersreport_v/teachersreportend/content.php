
<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Okul Öğretmenlerimizin Eğitim Yoklama Listesi</h4>
         <hr>
      
            <a href="<?php echo base_url("teachersreportend/pdfList"); ?>" class="btn btn-primary  btn-sm btn-outline pull-right"><i class="fa fa-download"></i>&nbsp;Başvuru Listesini İndir </a>
            <br><br>
            <?php if(empty($teachers)) { ?>
        <div class="alert alert-danger" role="alert">
            <!-- <strong>Kayıt Yok! </strong><br> -->
            <span>Hiç kayıt bulunamadı...</span>
            <!-- <a href="#" class="alert-link"></a> -->
        </div>
        <?php
        } else {
            $i=1;
        ?>
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <th>No</th>
                <th>Öğretmenin Adı Soyadı</th>
                <th>Eğitim Adı</th>
                <th class="text-center tdws">Tarih</th>
                <th class="text-center tdws">Durum</th>
               
            </thead>
            <tbody>
            <?php foreach ($teachers as $item) { ?>
            <tr>
                <td class="text-center tdws"><?= $i++; ?></td>
                <td><?= $item->fullname; ?></td>
                <td><?= $item->name; ?></td>
                <td class="text-center tdws"><?= date("d.m.Y",strtotime($item->startdate)); ?></td>
                <td class="text-center tdws"><?= ($item->joined==1)?"GELDİ":"<b style='color:red'>GELMEDİ</b>"; ?></td></td>
                
        
            </tr>
            <?php } ?>
            </tbody>

        </table>

        <?php } ?>
     
    </div><!-- .widget -->
</div><!-- END column -->
</div>


