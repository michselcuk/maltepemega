<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Eğitim Listesi</h4>
        <hr>
        <?php if(isAllowedWriteModule()) { ?>
            <a href='<?php echo(base_url("seminar/newForm")); ?>'
               type="button" class="btn btn-outline btn-success btn-sm pull-right">
                <i class='fa fa-plus-circle'></i>&nbsp;Yeni Eğitim Ekle
            </a>
        <?php } ?>
        <br><br>

        <?php if(empty($items)) {?>
        <div class="alert alert-danger" role="alert">
            <!-- <strong>Kayıt Yok! </strong><br> -->
            <span>Hiç kayıt bulunamadı...</span>
            <!-- <a href="#" class="alert-link"></a> -->
        </div>

        <?php
        } else {
            $i=1;
        ?>
<br/>
        <table class="table table-bordered table-hover table-striped">
            <thead>
              
                <th>No</th>
                <th>Eğitim Adı</th>
                <th>Eğitim Türü</th>
                <th class="text-center">Durum</th>
                <th class="text-center">İşlem</th>
            </thead>
            <tbody class="sortable" data-url="<?php echo base_url("seminar/rankSetter"); ?>">
            <?php foreach ($items as $item) { ?>
            <tr>
         
                <td><?= $i++; ?></td>
                <td><?= $item->name; ?></td>
                <td><?= $item->title; ?></td>
                <td class="text-center">
                    <?php if(isAllowedUpdateModule()) { ?>
                    <input
                            data-url="<?php echo base_url("seminar/isActiveSetter/$item->seminarid")?>"
                            class="isActive"
                            type="checkbox"
                            data-switchery data-color="#35b8e0"
                            data-size="small"
                            <?= ($item->isActive) ? "checked" : ""; ?>
                    />
                    <?php } ?>
                </td>
                <td class="text-center">
<!--                    <a href="--><?php //echo base_url("seminar/detail/$item->seminarid"); ?><!--" class="btn btn-outline btn-dark btn-xs">-->
<!--                        <i class="fa fa-image"></i>&nbsp;Detay</a>-->
                    <a href="<?php echo base_url("seminar/updateForm/$item->seminarid"); ?>" class="btn btn-outline btn-info btn-xs">
                    <i class="fa fa-edit"></i>&nbsp;Düzenle</a>
                    <button data-url="<?php echo base_url("seminar/delete/$item->seminarid"); ?>" class="btn btn-outline btn-danger btn-xs btn-haberSil">
                            <i class="fa fa-trash"></i>&nbsp;Sil</button>

                </td>
            </tr>
            <?php } ?>
            </tbody>

        </table>

        <?php } ?>
    </div><!-- .widget -->
</div><!-- END column -->
</div>


