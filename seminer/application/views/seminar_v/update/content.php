<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Seminer Bilgilerini Düzenle</h4>
    </div>
    <div class="col-md-12">
		<div class="widget">
			<div class="widget-body">
                <form method="post" action="<?php echo(base_url("seminar/update/$item->id")); ?>">

                    <div class="form-group">
                        <label>Eğitim Adı</label>
                        <input type="text" class="form-control" name="txtSeminerAdi" placeholder="Eğitim Adı" value="<?= $item->name;?>">
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error pull-right"><?= form_error("txtSeminerAdi"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Kısa Açıklama</label>
                        <textarea class="form-control" name="txtSeminerKisaAciklama" placeholder="Eğitim Hakkında Kısa Açıklama Yazınız"><?= $item->description;?></textarea>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error pull-right"><?= form_error("txtSeminerKisaAciklama"); ?></small>
                        <?php } ?>
                    </div>



                    <div class="form-group">
                        <label>Eğitim Türü</label>
                        <select name="slcSeminerTipi" class="form-control">
                            <option selected="selected" disabled>- Seçiniz -</option>
                            <?php foreach ($seminarType as $type) { ?>
                                <option <?php echo ($type->id == $item->seminartype) ? "selected":""; ?> value="<?= $type->id ?>"><?= $type->title ?></option>
                            <?php } ?>
                        </select>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error pull-right"><?= form_error("slcSeminerTipi"); ?></small>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label>Eğitim Detayı</label>
                        <textarea name="txtSeminerDetay" class="m-0" data-plugin="summernote" data-options="{height: 250}"><?= $item->detail;?></textarea>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error pull-right"><?= form_error("txtSeminerDetay"); ?></small>
                        <?php } ?>
                    </div>

                    <button type="submit" class="btn btn-primary btn-outline">Kaydet</button>
                    <a href="<?php echo base_url("seminar"); ?>" class="btn btn-danger btn-outline">İptal</a>

                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- .widget -->
</div><!-- END column -->



