<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            İl Projeleri Listesi
            <a href="<?=base_url("project_general/new_form")?>" class="btn btn-outline btn-sm btn-primary pull-right">
                <i class="fa fa-plus"></i>
                Yeni İl Projesi Ekle
            </a>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget p-lg">
            <?php if (empty($items)) { ?>
                <div class="alert alert-info text-center">
                    <p>Burada herhangi bir veri bulunmamaktadır. Eklemek için lütfen
                    <a href="<?=base_url("project_general/new_form")?>">tıklayınız.</a>
                    </p>
                </div>
            <?php  } else { ?>
                <table class="table table-striped table-bordered table-hover content-container">
                <thead>
                    <tr>
                        <th><i class="fa fa-reorder"></i> </th>
                        <th>id</th>
                        <th>Başlık</th>

                        <th>Açıklama</th>
                        <th>Durum</th>
                        <th>İşlem</th>
                    </tr>
                </thead>
                <tbody class="sortable" data-url="<?=base_url("project_general/rankSetter");?>" >
                <?php foreach ($items as $item) { ?>

                    <tr id="ord-<?=$item->id?>" >
                        <td><i class="fa fa-reorder"></i></td>
                        <td><?=$item->id?></td>
                        <td><?=$item->title?></td>

                        <td><?=$item->description?></td>
                        <td>
                            <input id="switch-2-2" type="checkbox"
                                   data-url="<?=base_url("project_general/isActiveSetter/$item->id");?>"
                                   class="isActive"
                                   data-size="small"
                                   data-switchery data-color="#10c469"
                                   <?php echo($item->isActive) ? 'checked' : ''; ?>
                                   />
                        </td>
                        <td class="tdws">
                            <button data-url="<?=base_url("project_general/delete/$item->id")?>"
                                    class="btn btn-sm btn-danger btn-outline remove-btn">
                                <i class="fa fa-trash"></i>
                                Sil
                            </button>
                            <a href="<?=base_url("project_general/update_form/$item->id")?>" class="btn btn-sm btn-info btn-outline">
                                <i class="fa fa-pencil-square-o"></i>
                                Güncelle
                            </a>

                        </td>
                    </tr>
                <?php } ?>

                </tbody>

            </table>
            <?php } ?>
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
