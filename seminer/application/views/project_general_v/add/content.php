<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            İl Projesi Ekle
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">

            <div class="widget-body">

                <form action="<?=base_url("project_general/save")?>" method="post"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Başlık</label>
                        <input type="text" class="form-control"
                               name="title">
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("title");?>
                            </strong>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label>Kapak Resmi</label>
                        <input type="file" class="form-control"
                               name="img_url">
                    </div>
                    <div class="form-group">
                        <label>Açıklama</label><br>
                        <textarea class="m-0"
                                  name="description"
                                  data-plugin="summernote"
                                  data-options="{height: 250}">

                        </textarea>
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("description");?>
                            </strong>
                        </div>
                    <?php } ?>
                    <button type="submit" class="btn btn-primary btn-md btn-outline">Proje Ekle</button>
                    <a href="<?=base_url("project_general")?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
