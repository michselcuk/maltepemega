<?php if (empty($item_images)) { ?>
    <div class="alert alert-info text-center">
        <p>Burada herhangi bir veri bulunmamaktadır. Eklemek için lütfen
            <a href="<?=base_url("product/new_form")?>">tıklayınız.</a>
        </p>
    </div>
<?php  }
else { ?>
    <table class="table table-striped table-bordered picture_list">
        <thead>
        <tr>
            <th><i class="fa fa-reorder"></i> </th>
            <th class="text-center">ID</th>
            <th class="text-center">Görsel</th>
            <th class="text-center">Resim Adı</th>
            <th class="text-center">Durum</th>
            <th class="text-center">Kapak</th>
            <th class="text-center">İşlem</th>
        </tr>
        </thead>
        <tbody class="sortable" data-url="<?=base_url("product/imageRankSetter");?>" >
        <?php foreach ($item_images as $item_image) { ?>
            <tr id="ord-<?=$item_image->id?>" >
                <td><i class="fa fa-reorder"></i> </td>
                <td class="w-100 text-center"><?=$item_image->id;?></td>
                <td class="w-100">
                    <img width="50"
                         src="<?=base_url("uploads/{$viewFolder}/$item_image->img_url");?>"
                         alt="<?=$item->url;?>" class="img-responsive" >
                </td>
                <td>
                    <?=$item_image->img_url;?>

                </td>
                <td class="w-100 text-center" >
                    <input id="switch-2-2" type="checkbox"
                           data-url="<?=base_url("product/isImageActiveSetter/$item_image->id");?>"
                           class="isActive"
                           data-switchery data-color="#10c469"
                        <?php echo($item_image->isActive) ? 'checked' : ''; ?>
                    />
                </td>
                <td class="w-100 text-center" >
                    <input id="switch-2-2" type="checkbox"
                           data-url="<?=base_url("product/isCoverSetter/$item_image->id/$item_image->product_id");?>"
                           class="isCover"
                           data-switchery data-color="#35b8e0"
                        <?php echo($item_image->isCover) ? 'checked' : ''; ?>
                    />
                </td>
                <td class="w-100 text-center">
                    <button data-url="<?=base_url("product/imageDelete/$item_image->id/$item_image->product_id")?>"
                            class="btn btn-sm btn-danger btn-outline remove-btn btn-block">
                        <i class="fa fa-trash"></i>
                        Sil
                    </button>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>