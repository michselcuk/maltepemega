<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Ürün Güncelle - <?=$item->id;?>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">

            <div class="widget-body">

                <form action="<?=base_url("project_general/update/$item->id")?>" method="post"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Başlık</label>
                        <input type="text" class="form-control"
                               name="title" value="<?=$item->title;?>">
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("title");?>
                            </strong>
                        </div>
                    <?php } ?>
                    <?php if ($item->img_url){ ?>
                    <div class="form-group ">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Kapak Resmi</label>
                            </div>
                            <div class="col-md-9">
                                <img src="<?=base_url("uploads/project_general_v/$item->img_url");?>">
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <label>Kapak Resmi Güncelle</label>
                        <input type="file" class="form-control"
                               name="img_url">
                    </div>
                    <div class="form-group">
                        <label>Açıklama</label><br>
                        <textarea class="m-0" name="description" data-plugin="summernote" data-options="{height: 250}">
                            <?=$item->description;?>
                        </textarea>
                    </div>

                    <button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
                    <a href="<?=base_url("project_general")?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
