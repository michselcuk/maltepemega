<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Yeni Eğitim Ekle</h4>
    </div>
    <div class="col-md-12">
        <div class="widget">
			<div class="widget-body">
				<form method="post" action="<?php echo(base_url("course/save")); ?>">

                    <div class="form-group">
                        <label>Eğitim Seçiniz</label>
                        <select name="slcEgitimAdi" class="form-control">
                            <option selected="selected" disabled>- Seçiniz -</option>
                            <?php foreach ($seminars as $seminar) { ?>
                                <option value="<?= $seminar->id ?>"><?= $seminar->name ?></option>
                            <?php } ?>
                        </select>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("slcEgitimAdi"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Eğitim Yeri</label>
                        <select name="slcEgitimYeri" class="form-control">
                            <option selected="selected" disabled>- Seçiniz -</option>
                            <?php foreach ($schools as $school) { ?>
                                <option value="<?= $school->id ?>"><?= $school->school_name ?></option>
                            <?php } ?>
                        </select>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("slcEgitimYeri"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Eğitimi Veren Kişi</label>
                        <select name="slcEgitimYapan" class="form-control">
                            <option selected="selected" disabled>- Seçiniz -</option>
                            <?php foreach ($teachers as $teacher) { ?>
                                <option value="<?= $teacher->id ?>"><?= $teacher->fullname ?></option>
                            <?php } ?>
                        </select>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("slcEgitimYapan"); ?></small>
                        <?php } ?>
                    </div>


                        <div class="form-group">
                            <label>Kontenjan</label>
                            <input type="number" class="form-control" name="txtEgitimKontenjan" placeholder="Seminer Kontenjanı">
                            <?php if(isset($form_error)){ ?>
                                <small class="input-form-error"><?= form_error("txtEgitimKontenjan"); ?></small>
                            <?php } ?>
                        </div>

                    <div class="form-group" style="position: relative">
                        <label>Eğitim Tarihi</label>
                        <div class='input-group date col-md-6' name="txtEgitimTarihi"  data-plugin="datetimepicker" data-options="{format:'YYYY-MM-DD'}">
                            <input type='text' name="txtEgitimTarihi" class="form-control" />
                            <span class="input-group-addon bg-info text-white">
							    <span class="glyphicon glyphicon-calendar"></span>
                            </span>

                        </div>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtEgitimTarihi"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Başlama Saati</label>
                        <div class="input-group bootstrap-timepicker timepicker col-sm-2">
                            <input  name="txtEgitimBasSaat" id="timepicker2" type="text" class="form-control input-small" data-plugin="timepicker" data-options="{ showInputs: false, showMeridian: false }">
                            <span class="input-group-addon bg-info"><i class="glyphicon glyphicon-time"></i></span>
                        </div>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtEgitimBasSaat"); ?></small>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label>Bitiş Saati</label>
                        <div class="input-group bootstrap-timepicker timepicker col-sm-2">
                            <input  name="txtEgitimBitSaat"  id="timepicker2" type="text" class="form-control input-small" data-plugin="timepicker" data-options="{ showInputs: false, showMeridian: false }">
                            <span class="input-group-addon bg-info"><i class="glyphicon glyphicon-time"></i></span>
                        </div>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtEgitimBitSaat"); ?></small>
                        <?php } ?>
                    </div>



						<div class="form-group">
								<label>Eğitim Detayı</label>
                                <textarea name="txtEgitimDetay" class="m-0" data-plugin="summernote" data-options="{height: 250}"></textarea>
								<?php if(isset($form_error)){ ?>
								<small class="input-form-error"><?= form_error("txtEgitimDetay"); ?></small>
								<?php } ?>
						</div>

                        <button type="submit" class="btn btn-primary btn-outline">Kaydet</button>
                        <a href="<?php echo base_url("course"); ?>" class="btn btn-danger btn-outline">İptal</a>

                </form>
            </div><!-- .widget-body -->
		</div><!-- .widget -->

    </div>
</div>


