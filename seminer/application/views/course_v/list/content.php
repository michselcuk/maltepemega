<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Eğitim Listesi</h4>
        <hr>

            <a href='<?php echo(base_url("course/newForm")); ?>'
               type="button" class="btn btn-outline btn-success btn-sm pull-right">
                <i class='fa fa-plus-circle'></i>&nbsp;Yeni Eğitim Ekle
            </a>

        <br><br>

        <?php if(empty($items)) {?>
        <div class="alert alert-danger" role="alert">
            <!-- <strong>Kayıt Yok! </strong><br> -->
            <span>Hiç kayıt bulunamadı...</span>
            <!-- <a href="#" class="alert-link"></a> -->
        </div>

        <?php
        } else {
            $i=1;
        ?>

        <table class="table table-bordered table-hover table-striped">
            <thead>
                <th class="text-center tdws">No</th>
                <th>Eğitim Adı</th>
                <th class="text-center tdws">Tarih</th>
                <th class="text-center tdws">Saat</th>
                <th class="text-center tdws">Kontenjan</th>
                <th class="text-center tdws">Durum</th>
                <th class="text-center tdws">İşlem</th>
            </thead>
            <tbody>
            <?php foreach ($items as $item) { ?>
            <tr>
          
                <td><?= $i++; ?></td>
                <td><?= $item->name; ?></td>
                <td class="text-center"><?= date("d.m.Y",strtotime($item->startdate)); ?></td>
                <td class="text-center tdws"><?= date("H:i",strtotime($item->starttime))." - ". date("H:i",strtotime($item->endtime)); ?></td>
                <td class="text-center tdws"><?= $item->quota; ?></td>
                <td class="text-center tdws">

                    <input
                            data-url="<?php echo base_url("course/isActiveSetter/$item->courseid")?>"
                            class="isActive"
                            type="checkbox"
                            data-switchery data-color="#35b8e0"
                            data-size="small"
                            <?= ($item->isActive) ? "checked" : ""; ?>
                    />

                </td>
                <td class="text-center tdws">
                    <!--<a href="<?php //echo base_url("course/detail/$item->courseid"); ?>" class="btn btn-outline btn-dark btn-xs">
                        <i class="fa fa-image"></i>&nbsp;Detay</a>-->
                    <a href="<?php echo base_url("course/updateForm/$item->courseid"); ?>" class="btn btn-outline btn-info btn-xs">
                    <i class="fa fa-edit"></i>&nbsp;Düzenle</a>
                    <button data-url="<?php echo base_url("course/delete/$item->courseid"); ?>" class="btn btn-outline btn-danger btn-xs btn-haberSil">
                            <i class="fa fa-trash"></i>&nbsp;Sil</button>

                </td>
            </tr>
            <?php } ?>
            </tbody>

        </table>

        <?php } ?>
    </div><!-- .widget -->
</div><!-- END column -->
</div>


