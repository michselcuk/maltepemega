<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            <?=$project->title ;?>
        </h4>
    </div><!-- END column -->

    <div class="col-md-12">
        <div class="widget p-lg">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <td>Proje Adı</td>
                        <td><?=$project->title ;?></td>
                    </tr>
                    <tr>
                        <td>Projeyi Yazan Öğretmen</td>
                        <td><?=$project->fullname ;?></td>
                    </tr>
                    <tr>
                        <td>Projenin Yapıldığı Okul</td>
                        <td><?=$project->school_name ;?></td>
                    </tr>

                    <tr>
                        <td>Proje Kısa Açıklaması</td>
                        <td><?=$project->short_description ;?></td>
                    </tr>

                    <tr>
                        <td>Proje Açıklama</td>
                        <td><?=$project->description ;?></td>
                    </tr>

                    <tr>
                        <td>Projede Destek Olan Öğretmen(ler)</td>
                        <td><?=$project->project_teachers ;?></td>
                    </tr>

                    <tr>
                        <td>Projede Çalışan Öğretmen Sayısı</td>
                        <td><?=$project->project_teachers_count ;?></td>
                    </tr>
                    <tr>
                        <td>Projede Çalışan Öğrenci(ler)</td>
                        <td><?=$project->project_students ;?></td>
                    </tr>
                    <tr>
                        <td>Projede Çalışan Öğrenci Sayısı</td>
                        <td><?=$project->project_students_count ;?></td>
                    </tr>

                    <tr>
                        <td>Proje Okul Onay Durumu</td>
                        <td>
                            <?=($project->school_confirm==0)?
                                '<strong class="text-danger">Okul Onayı Bekliyor</strong>':
                                '<strong class="text-success">Onaylandı</strong>';
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Proje İlçe Onay Durumu</td>
                        <td>
                            <?=($project->admin_confirm==0)?
                                '<strong class="text-danger">İlçe Onayı Bekliyor</strong>':
                                '<strong class="text-success">Onaylandı</strong>';
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <td>Web Sayfasında Yayınlanma Durumu</td>
                        <td>
                            <?=($project->publish==0)?
                                '<strong class="text-danger">Web Sayfasında Yayınlanmıyor</strong>':
                                '<strong class="text-success">Web Sayfasınd Yayınlanıyor</strong>';
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table><br>
            <h4>Proje İle İlgili Yapılan Çalışmalar</h4><br>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <td>Çalışma No </td>
                        <td>Çalışma Başlığı </td>
                        <td>Çalışma Açıklaması </td>
                        <td>Çalışma Fotoğraf(lar)ı </td>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0;
                        foreach ($project_extras as $project_extra){ $i++; ?>
                    <tr>
                        <td><?=$i ;?>
                            <?php //echo $project_extra->id ;?>
                        </td>
                        <td><?=$project_extra->title;?></td>
                        <td><?=$project_extra->description;?></td>
                        <td>
                            <?php foreach ($project_extras_images as $project_extras_image){ ?>
                                <p><?php
                                        if($project_extra->id == $project_extras_image->project_extra_id) { ?>
                                            <img width="50"
                         src="<?=base_url("uploads/project_extras_v/$project_extras_image->img_url");?>"
                         class="img-responsive" >

                                     <?php  } ?>
                                </p>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>

        </div><!-- .widget
    </div><!-- END column -->

</div>
