<?php if (empty($item_images)) { ?>
    <div class="alert alert-info text-center">
        <p>
            Projede yapılan işlemler için henüz fotoğraf bulunmamaktadır.
        </p>
    </div>
<?php  }
else { ?>
    <table class="table table-striped table-bordered picture_list">
        <thead>
        <tr>
            <th><i class="fa fa-reorder"></i> </th>
            <th class="text-center">Resim ID</th>
            <th class="text-center">Görsel</th>
<!--            <th class="text-center">Resim Adı</th>-->
<!--            <th class="text-center">Durum</th>-->
<!--            <th class="text-center">Kapak</th>-->
            <th class="text-center">İşlem</th>
        </tr>
        </thead>

        <tbody class="sortable" data-url="<?=base_url("project_extras/imageRankSetter");?>" >
        <?php foreach ($item_images as $item_image) { ?>
            <tr id="ord-<?=$item_image->id?>" >
                <td class="w-100"><i class="fa fa-reorder"></i> </td>
                <td class="w-100 text-center"><?=$item_image->id;?></td>
                <td class="">
                    <img width="100"
                         src="<?=base_url("uploads/{$viewFolder}/$item_image->img_url");?>"
                         class="img-responsive" >
                </td>
                <!--<td>
                    <?/*=$item_image->img_url;*/?>

                </td>-->
                <!--<td class="w-100 text-center" >
                    <input id="switch-2-2" type="checkbox"
                           data-url="<?/*=base_url("project_extras/isImageActiveSetter/$item_image->id");*/?>"
                           class="isActive"
                           data-switchery data-color="#10c469"
                        <?php /*echo($item_image->isActive) ? 'checked' : ''; */?>
                    />
                </td>-->

                <td class="w-100 text-center">
                    <button data-url="<?=base_url("project_extras/imageDelete/$item_image->id/$item_image->project_extra_id")?>"
                            class="btn btn-sm btn-danger btn-outline remove-btn btn-block">
                        <i class="fa fa-trash"></i>
                        Sil
                    </button>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>