<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            <strong><?=$project->title;?></strong>
            projesine yapılan çalışmayı güncelliyorsunuz
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">

            <div class="widget-body">

                <form action="<?=base_url("project_extras/update/$item->id")?>" method="post">
                    <input type="hidden" name="project_id" value="<?=$item->project_id ;?>">
                    <div class="form-group">
                        <label>Yapılan İşlem Başlık</label>
                        <input type="text" class="form-control"
                               name="title" value="<?=$item->title ;?>">
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("title");?>
                            </strong>
                        </div>
                    <?php } ?>


                    <div class="form-group">
                        <label>Açıklama</label><br>
                        <textarea class="m-0"
                                  name="description"
                                  data-plugin="summernote"
                                  data-options="{height: 250}"><?=$item->description;?></textarea>
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("description");?>
                            </strong>
                        </div>
                    <?php } ?>
                    <button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
                    <a href="<?=base_url("project_extras/index/$item->project_id");?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
