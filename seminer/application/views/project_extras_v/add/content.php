<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            <strong><?=$project->title?></strong>
            projesine çalışma ekliyorsunuz

        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">

            <div class="widget-body">

                <form action="<?=base_url("project_extras/save")?>" method="post">
                    <input type="hidden" name="project_id" value="<?=$id; ?>">
                    <div class="form-group">
                        <label>İşlem Başlığı</label>
                        <input type="text" class="form-control"
                               name="title">
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("title");?>
                            </strong>
                        </div>
                    <?php } ?>


                    <div class="form-group">
                        <label>Açıklama</label><br>
                        <textarea class="m-0"
                                  name="description"
                                  data-plugin="summernote"
                                  data-options="{height: 250}">

                        </textarea>
                    </div>

                    <button type="submit" class="btn btn-primary btn-md btn-outline">İşlemi Kaydet</button>
                    <a href="<?=base_url("project_extras/index/$id");?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
