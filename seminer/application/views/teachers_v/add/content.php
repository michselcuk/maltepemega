<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Yeni Öğretmen Ekle</h4>
    </div>
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form method="post" action="<?php echo(base_url("teachers/save")); ?>">
                    <div class="form-group">
                        <label>Adı Soyadı</label>
                        <input type="text" class="form-control" name="txtOgretmenAdi" placeholder="Öğretmenin Adı Soyadı">
                        <?php if(isset($form_error)){ ?>
                        <small class="input-form-error"><?= form_error("txtOgretmenAdi"); ?></small>
                        <?php } ?>
                    </div>
                    <div class="row">
                    <div class="form-group col-md-6">
                        <label>Görev Yaptığı Okul</label>
                        <select name="slcOgretmenOkul" class="form-control">
                            <option selected="selected" disabled>- Seçiniz -</option>
                            <?php foreach ($schools as $school) { ?>
                            <option value="<?= $school->id ?>"><?= $school->school_name ?></option>
                            <?php } ?>
                        </select>
                        <?php if(isset($form_error)){ ?>
                        <small class="input-form-error"><?= form_error("slcOgretmenOkul"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group col-md-6">
                        <label>Kullanıcı Yetki Türü</label>
                        <select name="slcKullaniciTuru" class="form-control">
                            <option selected="selected" disabled>- Seçiniz -</option>
                            <?php foreach ($roles as $role) { ?>
                                <option value="<?= $role->id ?>"><?= $role->userrole ?></option>
                            <?php } ?>
                        </select>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("slcKullaniciTuru"); ?></small>
                        <?php } ?>
                    </div>
                    </div>
                    <div class="row">
                    <div class="form-group col-md-6">
                        <label>Cep Telefonu</label>
                        <input type="text" class="form-control" name="txtOgretmenCeptel" placeholder="Öğretmenin Cep Telefonu">
                        <?php if(isset($form_error)){ ?>
                        <small class="input-form-error"><?= form_error("txtOgretmenCeptel"); ?></small>
                        <?php } ?>
                    </div>
                    <div class="form-group col-md-6">
                        <label>E-Posta Adresi</label>
                        <input type="text" class="form-control" name="txtOgretmenEposta" placeholder="Öğretmenin E-Posta Adresi">
                        <?php if(isset($form_error)){ ?>
                        <small class="input-form-error"><?= form_error("txtOgretmenEposta"); ?></small>
                        <?php } ?>
                    </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Yeni Şifre</label>
                            <input type="text" class="form-control" name="txtOgretmenSifre" placeholder="Şifre" value="12345678">
                            <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOgretmenSifre"); ?></small>
                            <?php } ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Yeni Şifre (Tekrar)</label>
                            <input type="text" class="form-control" name="txtOgretmenSifre2" placeholder="Şifre" value="12345678">
                            <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOgretmenSifre2"); ?></small>
                            <?php } ?>
                        </div>
                    </div>
                    
                    
                    <button type="submit" class="btn btn-primary btn-outline">Kaydet</button>
                    <a href="<?php echo base_url("teachers"); ?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
                </div><!-- .widget-body -->
                </div><!-- .widget -->
            </div>
        </div>