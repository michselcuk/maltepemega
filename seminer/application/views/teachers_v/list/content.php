<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Öğretmenler Listesi</h4>
       <hr/>
            <a href='<?php echo(base_url("yeni-ogretmen-ekle")); ?>'
               type="button" class="btn btn-outline btn-success btn-sm pull-right">
                <i class='fa fa-plus-circle'></i>&nbsp;Yeni Öğretmen Ekle
            </a>
           <br/> <br/><hr/>
 
  
    <table id="tbl-teachers" class="table table-bordered table-hover table-striped">
        <thead>
        <th class="text-center tdws">No</th>
        <th>Adı Soyadı</th>
        <th class="text-center tdws">Cep Telefonu</th>
        <th>Okulu</th>
        <th class="text-center tdws">İşlem</th>
        </thead>
        <tbody>
       
        </tbody>

    </table>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#tbl-teachers').DataTable({
        "pageLength": 100,
        "ajax": {
            url : "<?= base_url('teachers/teachersList'); ?>",
            type : 'post'
        },
        "language":{
            "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Turkish.json"
        },
        "columnDefs": [
            { 
                "orderable": false, "targets": [2,4]
            }
        ]
    });
});
</script>