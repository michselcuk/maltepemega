<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Kullanıcı Rolleri</h4>
        <?php if(isAdmin()){?>
        <a href='<?php echo(base_url("userroles/newForm")); ?>'
           type="button"
           class="btn btn-outline btn-success btn-sm pull-right">
            <i class='fa fa-plus-circle'></i>&nbsp;Yeni Kullanıcı Rolü Ekle
        </a>
        <?php }?>

        <br><br>

        <?php if(empty($items)) {?>
        <div class="alert alert-danger" role="alert">
            <!-- <strong>Kayıt Yok! </strong><br> -->
            <span>Hiç kayıt bulunamadı...</span>
            <!-- <a href="#" class="alert-link"></a> -->
        </div>

        <?php
        } else {
            $i=1;
        ?>

        <table class="table table-bordered  table-striped">
            <thead>
                <th>No</th>
                <th>Kullanıcı Rolü</th>
                <th class="text-center tdws">Durum</th>
                <th class="text-center tdws">İşlemler</th>
            </thead>
            <tbody>
            <?php foreach ($items as $item) { ?>
            <tr id="ord-<?= $item->id; ?>">
                <td><?= $i++; ?></td>
                <td><?= $item->userrole; ?></td>
                <td  class="text-center tdws">
                    <input
                            data-url="<?php echo base_url("userroles/isActiveSetter/$item->id")?>"
                            class="isActive"
                            type="checkbox"
                            data-switchery data-color="#35b8e0"
                            data-size="small"
                            <?= ($item->isActive) ? "checked" : ""; ?>
                    />
                </td>
                <td  class="text-center tdws">
                    <a href="<?php echo base_url("userroles/updateForm/$item->id"); ?>" class="btn btn-outline btn-info btn-xs">
                    <i class="fa fa-edit"></i>&nbsp;Düzenle
                    </a>
                    <button
                            data-url="<?php echo base_url("userroles/delete/$item->id"); ?>"
                            class="btn btn-outline btn-danger btn-xs btn-haberSil">
                            <i class="fa fa-trash"></i>&nbsp;Sil
                    </button>

                    <a href="<?php echo base_url("userroles/permissionsForm/$item->id"); ?>" class="btn btn-outline btn-dark btn-xs">
                        <i class="fa fa-eye"></i>&nbsp;Yetki Tanımı</a>
                </td>
            </tr>
            <?php } ?>
            </tbody>

        </table>

        <?php } ?>
    </div><!-- .widget -->
</div><!-- END column -->
</div>


