<?php $permissions = json_decode($item->permissions); ?>
<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Kullanıcı Yetkilerini Düzenle</h4>
    </div>
    <div class="col-md-12">
		<div class="widget">
			<div class="widget-body">
				<form method="post"	action="<?php echo(base_url("userroles/update_permissions/$item->id")); ?>">
                    <hr class="form-group">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th>Modül Adı</th>
                            <th class="text-center">Görüntüleme</th>
                            <th class="text-center">Ekleme</th>
                            <th class="text-center">Düzenleme</th>
                            <th class="text-center">Silme</th>
                        </thead>
                        <tbody>
                        <?php foreach (getControllerList() as $controllername){ ?>
                            <tr>
                                <td><?= $controllername; ?></td>
                                <td class="text-center">
                                    <input <?php echo (isset($permissions->$controllername) && isset($permissions->$controllername->read)) ? "checked" : ""; ?>

                                            type="checkbox" name="permission[<?= $controllername; ?>][read]" data-switchery data-color="#35b8e0" data-size="small"/>
                                </td>
                                <td class="text-center">
                                    <input <?php echo (isset($permissions->$controllername) && isset($permissions->$controllername->write)) ? "checked" : ""; ?>
                                            type="checkbox" name="permission[<?= $controllername; ?>][write]" data-switchery data-color="#35b8e0" data-size="small"/>
                                </td>
                                <td class="text-center">
                                    <input <?php echo (isset($permissions->$controllername) && isset($permissions->$controllername->update)) ? "checked" : ""; ?>
                                            type="checkbox" name="permission[<?= $controllername; ?>][update]" data-switchery data-color="#35b8e0" data-size="small"/>
                                </td>
                                <td class="text-center">
                                    <input <?php echo (isset($permissions->$controllername) && isset($permissions->$controllername->delete)) ? "checked" : ""; ?>
                                            type="checkbox" name="permission[<?= $controllername; ?>][delete]" data-switchery data-color="#35b8e0" data-size="small"/>
                                </td>
                            </tr>
                        <?php  } ?>
                        </tbody>
                    </table>
                    <hr>
                    <button type="Bilgileri Güncelle" class="btn btn-success btn-outline">Güncelle</button>
                    <a href="<?php echo base_url("userroles"); ?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- .widget -->
</div><!-- END column -->



