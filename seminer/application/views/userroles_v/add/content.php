<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Yeni Kullanıcı Rolü Ekle</h4>
    </div>
    <div class="col-md-12">
        <div class="widget">
			<div class="widget-body">
				<form method="post" action="<?php echo(base_url("userroles/save")); ?>">

                    <div class="form-group">
						<label>Kullanıcı Rolü</label>
                        <input type="text" class="form-control" name="txtKullaniciRolu" placeholder="Kullanıcı Rolü"
                               value="<?php echo isset($form_error) ? set_value("txtKullaniciRolu"):""; ?>">
					    <?php if(isset($form_error)){ ?>
						<small class="input-form-error pull-right"><?= form_error("txtKullaniciRolu"); ?></small>
						<?php } ?>
					</div>


                    <button type="submit" class="btn btn-primary btn-outline">Ekle</button>
                    <a href="<?php echo base_url("userroles"); ?>" class="btn btn-danger btn-outline">İptal</a>

                </form>
            </div><!-- .widget-body -->
		</div><!-- .widget -->

    </div>
</div>


