<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Kullanıcı Bilgilerini Düzenle</h4>
    </div>
    <div class="col-md-12">
		<div class="widget">
			<div class="widget-body">
				<form method="post"	action="<?php echo(base_url("userroles/update/$item->id")); ?>">

                        <div class="form-group">
                            <label>Kullanıcı Rolü</label>
                            <input type="text" class="form-control" name="txtKullaniciRoluGuncelle" placeholder="Kullanıcı Rolü"
                                   value="<?php echo isset($form_error) ? set_value("txtKullaniciRoluGuncelle"):""; ?>">
                            <?php if(isset($form_error)){ ?>
                                <small class="input-form-error pull-right"><?= form_error("txtKullaniciRoluGuncelle"); ?></small>
                            <?php } ?>
                        </div>
                        <button type="Bilgileri Güncelle" class="btn btn-success btn-outline">Güncelle</button>
                    <a href="<?php echo base_url("userroles"); ?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- .widget -->
</div><!-- END column -->



