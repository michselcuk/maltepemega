<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            İl Projesine Çalışma Ekle
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">

            <div class="widget-body">

                <form action="<?=base_url("project_general_school_extras/save")?>" method="post">
                    <div class="form-group">
                        <label>Proje Adı</label>
                        <select class="form-control" name="project_general_id">
                            <?php foreach ($project_generals as $project_general) { ?>
                                <option value="<?=$project_general->id;?>">
                                    <?=$project_general->title ;?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>İşlem Başlığı</label>
                        <input type="text" class="form-control"
                               name="title">
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("title");?>
                            </strong>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label>Projeye Katılan Öğretmenler</label>
                        <input type="text" class="form-control"
                               name="project_teachers">
                    </div>

                    <div class="form-group">
                        <label>Projeye Katılan Öğretmen Sayısı</label>
                        <input type="number" class="form-control"
                               name="project_teachers_count">
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("project_teachers_count");?>
                            </strong>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label>Projeye Katılan Öğrenciler</label>
                        <input type="text" class="form-control"
                               name="project_students">
                    </div>

                    <div class="form-group">
                        <label>Projeye Katılan Öğrenci Sayısı</label>
                        <input type="number" class="form-control"
                               name="project_students_count">
                    </div>
                    <?php if (isset($form_error)){ ?>
                        <div class="alert alert-danger">
                            <strong>
                                <?php echo form_error("project_students_count");?>
                            </strong>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label>Açıklama</label><br>
                        <textarea class="m-0"
                                  name="description"
                                  data-plugin="summernote"
                                  data-options="{height: 250}">

                        </textarea>
                    </div>

                    <button type="submit" class="btn btn-primary btn-md btn-outline">İşlemi Kaydet</button>
                    <a href="<?=base_url("project_general_school_extras");?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
