<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Okulunuzdan İl Projeleri ile İlgili Yapılan Çalışmalar
            <a href="<?=base_url("project_general_school_extras/new_form")?>" class="btn btn-outline btn-sm btn-primary pull-right">
                <i class="fa fa-plus"></i>
                Yeni Çalışma Ekle
            </a>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget p-lg">
            <?php if (empty($items)) { ?>
                <div class="alert alert-info text-center">
                    <p>Burada herhangi bir veri bulunmamaktadır. Eklemek için lütfen
                        <a href="<?=base_url("project_general_school_extras/new_form")?>">tıklayınız.</a>
                    </p>
                </div>
            <?php  } else { ?>
                <table class="table table-striped table-bordered table-hover content-container">
                    <thead>
                    <tr>
                        <th><i class="fa fa-reorder"></i> </th>
                        <th>id</th>
                        <th>İl Projesi Adı</th>
                        <th>Çalışma Başlığı</th>
                        <th>Açıklama</th>
                        <th>Sitede Yayınla</th>
                        <th style="width:15%;">İşlem</th>
                    </tr>
                    </thead>
                    <tbody class="sortable" data-url="<?=base_url("project_general_school_extras/rankSetter");?>" >
                    <?php foreach ($items as $item) { ?>

                        <tr id="ord-<?=$item->id; ?>" >
                            <td><i class="fa fa-reorder"></i></td>
                            <td><?=$item->id; ?></td>
                            <td><?=$item->project_title; ?></td>
                            <td><?=$item->title; ?></td>
                            <td><?=$item->description; ?></td>
                            <td>
                                <input id="switch-2-2" type="checkbox" readonly
                                       data-url="<?=base_url("project_general_school_extras/publish/$item->id");?>"
                                       class="publish"
                                       data-switchery data-color="#10c469"
                                    <?php echo($item->publish) ? 'checked' : ''; ?>
                                />
                            </td>
                            <td>
                                <button data-url="<?=base_url("project_general_school_extras/delete/$item->id")?>"
                                        class="btn btn-sm btn-danger btn-outline remove-btn">
                                    <i class="fa fa-trash"></i>
                                    Sil
                                </button>
                                <a href="<?=base_url("project_general_school_extras/update_form/$item->id")?>" class="btn btn-sm btn-info btn-outline">
                                    <i class="fa fa-pencil-square-o"></i>
                                    Güncelle
                                </a>
                                <a href="<?=base_url("project_general_school_extras/image_form/$item->id")?>" class="btn btn-sm btn-dark btn-outline">
                                    <i class="fa fa-image"></i>
                                    Resimler
                                </a>
                            </td>
                        </tr>
                    <?php } ?>

                    </tbody>

                </table>
            <?php } ?>
        </div><!-- .widget -->
    </div><!-- END column -->
</div>
