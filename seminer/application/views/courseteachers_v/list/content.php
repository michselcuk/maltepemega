
<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Eğitmen Listesi</h4>
       <hr/>
            <a href='<?php echo(base_url("courseteachers/newForm")); ?>'
               type="button" class="btn btn-outline btn-success btn-sm pull-right">
                <i class='fa fa-plus-circle'></i>&nbsp;Yeni Eğitmen Ekle
            </a>
           <br/> <br/><hr/>
 
  
    <table id="tbl-courseteachers" class="table table-bordered table-hover table-striped">
        <thead>
        <th>No</th>
        <th>Adı Soyadı</th>
        <th>Cep Telefonu</th>
        <th class="text-center">İşlem</th>
        </thead>
        <tbody>
       
        </tbody>

    </table>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#tbl-courseteachers').DataTable({
        "pageLength": 50,
        "ajax": {
            url : "<?= base_url('courseteachers/courseteachersList'); ?>",
            type : 'post'
        },
        "language":{
            "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Turkish.json"
        },
        "columnDefs": [
            { 
                "orderable": false, "targets": [2,3]
            }
        ]
    });
});
</script>