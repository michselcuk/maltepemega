<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Yeni Eğitmen Ekle</h4>
    </div>
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form method="post" action="<?php echo(base_url("courseteachers/save")); ?>">
                    <div class="form-group">
                        <label>Adı Soyadı</label>
                        <input type="text" class="form-control" name="txtOgretmenAdi"
                               placeholder="Öğretmenin Adı Soyadı">
                        <?php if (isset($form_error)) { ?>
                            <small class="input-form-error"><?= form_error("txtOgretmenAdi"); ?></small>
                        <?php } ?>
                    </div>


                        <div class="form-group">
                            <label>Cep Telefonu</label>
                            <input type="text" class="form-control" name="txtOgretmenCeptel"
                                   placeholder="Öğretmenin Cep Telefonu">
                            <?php if (isset($form_error)) { ?>
                                <small class="input-form-error"><?= form_error("txtOgretmenCeptel"); ?></small>
                            <?php } ?>
                        </div>

                            <div class="form-group">
                                <label>E-Posta Adresi</label>
                                <input type="text" class="form-control" name="txtOgretmenEposta"
                                       placeholder="Öğretmenin E-Posta Adresi">
                                <?php if (isset($form_error)) { ?>
                                    <small class="input-form-error"><?= form_error("txtOgretmenEposta"); ?></small>
                                <?php } ?>
                            </div>

                    <button type="submit" class="btn btn-primary btn-outline">Kaydet</button>
                    <a href="<?php echo base_url("courseteachers"); ?>" class="btn btn-danger btn-outline">İptal</a>


            </div>

    </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div>
</div>