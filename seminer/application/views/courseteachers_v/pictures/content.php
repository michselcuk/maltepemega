<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Fotoğraf İşlemleri</h4>
				<div class="widget">				
					<div class="widget-body">

                        <form action="../api/dropzone" class="dropzone" data-plugin="dropzone" data-options="{ url: '../api/dropzone'}">
                            <div class="dz-message">
                                <h3 class="m-h-lg">Drop files here or click to upload.</h3>
                                <p class="m-b-lg text-muted">(This is just a demo dropzone. Selected files are not actually uploaded.)</p>
                            </div>
                        </form>

					</div><!-- .widget-body -->
				</div><!-- .widget -->
      
    </div><!-- .widget -->
</div><!-- END column -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="widget p-lg">

            <div class="widget">
                <div class="widget-body">

                  <table class="table table-bordered table-striped table-hover">
                      <thead>
                      <th>id</th>
                      <th>resim</th>
                      <th>resim adı</th>
                      <th>durumu</th>
                      <th>işlem</th>
                      </thead>
                      <tbody>
                      <tr>
                          <td>1</td>
                          <td><img width="50" src="https://cdn.educba.com/academy/wp-content/uploads/2018/10/CodeIgniter-Interview-Questions.jpg" alt="" class="img-responsive"></td>
                          <td>manzara</td>
                          <td class="text-center">
                              <input
                                      data-url="<?php echo base_url("ogrenci/isActiveSetter/")?>"
                                      class="isActive"
                                      type="checkbox"
                                      data-switchery data-color="#35b8e0"
                                      data-size="small"
                                  <?= (1) ? "checked" : ""; ?>
                              />
                          </td>
                          <td>
                              <button
                                      data-url="<?php echo base_url("ogrenci/delete/"); ?>"
                                      class="btn btn-outline btn-danger btn-xs btn-ogrenciSil">
                                  <i class="fa fa-trash"></i>&nbsp;Sil
                              </button>
                          </td>
                      </tr>
                      </tbody>
                  </table>

                </div><!-- .widget-body -->
            </div><!-- .widget -->

        </div><!-- .widget -->
    </div><!-- END column -->
</div>

