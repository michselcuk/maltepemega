<div class="simple-page-wrap">
    <div class="simple-page-logo animated swing" style="margin-top:-30px;">
            <img src="<?php echo base_url("assets/assets/images/megalogo.png"); ?>" width="120px">

            <span style="color:#fff"></br>Maltepe Eğitim ve Gelişim Akademisi</span>

    </div><!-- logo -->
    <div class="simple-page-form animated flipInY" id="login-form">
        <h4 class="form-title m-b-xl text-center">Panel Giriş Ekranı</h4>
        <form action="<?php echo base_url("girisyap"); ?>" method="post">
            <div class="form-group">
                <input type="text" id="txtLoginCeptel" class="form-control" placeholder="Cep Telefonu" name="txtLoginCeptel">
                <?php if(isset($form_error)){ ?>
                    <small class="input-form-error pull-right"><?= form_error("txtLoginCeptel"); ?></small>
                <?php } ?>
            </div>

            <div class="form-group">
                <input type="password" name="txtLoginSifre" class="form-control" placeholder="Şifre">
                <?php if(isset($form_error)){ ?>
                    <small class="input-form-error pull-right"><?= form_error("txtLoginSifre"); ?></small>
                <?php } ?>
            </div>

            <div class="form-group m-b-xl">
                <div class="checkbox checkbox-primary">
                    <input type="checkbox" id="keep_me_logged_in"/>
                    <label for="keep_me_logged_in">Beni Hatırla</label>
                </div>
            </div>
            <input type="submit" class="btn btn-primary" value="GİRİŞ YAP">
        </form>
    </div><!-- #login-form -->
    <!--
       <div class="simple-page-footer">
           <p><a href="password-forget.html">Şifremi Unuttum!</a></p>

           <p>
               <small>Don't have an account ?</small>
               <a href="signup.html">CREATE AN ACCOUNT</a>
           </p>
           -->
    </div><!-- .simple-page-footer
-->

</div><!-- .simple-page-wrap -->