<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Kullanıcı Bilgilerini Düzenle</h4>
    </div>
    <div class="col-md-12">
		<div class="widget">
			<div class="widget-body">
				<form method="post"	action="<?php echo(base_url("users/update/$item->id")); ?>">
                    <div class="form-group">
                        <label>Adı Soyadı</label>
                        <input type="text" class="form-control" name="txtKullaniciAdi" placeholder="Kullanıcı Adı Soyadı"  value="<?= $item->fullname;?>">
                        <?php if(isset($form_error)){ ?>
                        <small class="input-form-error"><?= form_error("txtKullaniciAdi"); ?></small>
                        <?php } ?>
                    </div>

                      
                    <div class="form-group">
                        <label>Cep Telefonu</label>
                        <input type="text" class="form-control" name="txtKullaniciCeptel" placeholder="Kullanıcı Cep Telefonu" value="<?= $item->phone;?>">
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtKullaniciCeptel"); ?></small>
                        <?php } ?>
                    </div>

                          <div class="form-group">
                        <label>E-Posta Adresi</label>
                        <input type="email" class="form-control" name="txtKullaniciEposta" placeholder="Kullanıcı E-Posta Adresi"  value="<?= $item->email;?>">
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtKullaniciEposta"); ?></small>
                        <?php } ?>
                    </div>

                        <div class="form-group">
                            <label>Kullanıcı Rolü</label>
                            <select name="slcKullaniciRolu" class="form-control">
                                <?php foreach ($user_roles as $user_role) { ?>
                                    <option <?php echo ($user_role->id == $item->userrole) ? "selected":""; ?> value="<?= $user_role->id ?>"><?= $user_role->userrole ?></option>
                                <?php } ?>
                            </select>
                            <?php if(isset($form_error)){ ?>
                                <small class="input-form-error"><?= form_error("slcKullaniciRolu"); ?></small>
                            <?php } ?>
                        </div>

                    <div class="form-group">
                        <label>Şifre</label>
                        <input type="password" class="form-control" name="txtKullaniciSifre" placeholder="Kullanıcı Şifresi">
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtKullaniciSifre"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Şifre (Tekrar)</label>
                        <input type="password" class="form-control" name="txtKullaniciSifre2" placeholder="Kullanıcı Şifresi (Tekrar)">
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtKullaniciSifre2"); ?></small>
                        <?php } ?>
                    </div>

                    <button type="Bilgileri Güncelle" class="btn btn-success btn-outline">Güncelle</button>
                    <a href="<?php echo base_url("users"); ?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- .widget -->
</div><!-- END column -->



