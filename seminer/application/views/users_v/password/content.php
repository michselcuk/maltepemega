<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Kullanıcı Bilgilerini Düzenle</h4>
    </div>
    <div class="col-md-12">
		<div class="widget">
			<div class="widget-body">
				<form method="post"	action="<?php echo(base_url("users/update_password/$item->id")); ?>">
                    <div class="form-group">

                        <div class="form-group">
                            <label>Şifre</label>
                            <input type="password" class="form-control" name="txtKullaniciSifre" placeholder="Kullanıcı Şifresi">
                            <?php if(isset($form_error)){ ?>
                                <small class="input-form-error pull-right"><?= form_error("txtKullaniciSifre"); ?></small>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <label>Şifre Tekrar</label>
                            <input type="password" class="form-control" name="txtKullaniciSifre_Tekrar" placeholder="Şifre Tekrar">
                            <?php if(isset($form_error)){ ?>
                                <small class="input-form-error pull-right"><?= form_error("txtKullaniciSifre_Tekrar"); ?></small>
                            <?php } ?>
                        </div>
                    <button type="Bilgileri Güncelle" class="btn btn-success btn-outline">Güncelle</button>
                    <a href="<?php echo base_url("users"); ?>" class="btn btn-danger btn-outline">İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- .widget -->
</div><!-- END column -->



