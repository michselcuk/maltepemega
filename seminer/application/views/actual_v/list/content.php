<div class="row">
    <div class="col-md-12">
        <div class="widget p-lg">
            <h4 class="m-b-lg">Başvurulan Eğitimler</h4>
            <hr>
            <a href="<?php echo base_url("actual/pdfList"); ?>"
               class="btn btn-primary  btn-sm btn-outline pull-right"><i class="fa fa-download"></i>&nbsp;Eğitim
                Listesini İndir </a>
            <br><br>
            <?php if (empty($actual)) { ?>
                <div class="alert alert-danger" role="alert">
                    <!-- <strong>Kayıt Yok! </strong><br> -->
                    <span>Hiç kayıt bulunamadı...</span>
                    <!-- <a href="#" class="alert-link"></a> -->
                </div>

                <?php
            } else {
                $i = 1;
                ?>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <th>No</th>
                    <th>Eğitim Adı</th>
                    <th class="hidden-xs">Türü</th>
                    <th class="hidden-xs">Tarih</th>
                    <th class="hidden-xs">Saat</th>
                    <th class="hidden-xs">Eğitim Yeri</th>
                    <th class="text-center col-xs-6 col-sm-3 col-md-2">İşlem</th>
                    </thead>
                    <tbody>
                    <?php foreach ($actual as $item) { ?>
                        <tr>
                            <td><?= $i++; ?></td>
                            <td><?= $item->name; ?></td>
                            <td class="hidden-xs"><?= $item->title; ?></td>
                            <td class="text-center hidden-xs"><?= date("d.m.Y", strtotime($item->startdate)); ?></td>
                            <td class="text-center hidden-xs"><?= date("H:i", strtotime($item->starttime)) . " - " . date("H:i", strtotime($item->endtime)); ?></td>
                            <td class="hidden-xs"><?= $item->school_name; ?></td>
                            <td class="text-center  col-xs-6 col-sm-3 col-md-2">

                                <a href="<?php echo base_url("actual/detail/$item->actualid"); ?>" class="btn btn-outline btn-dark btn-xs">
                                    <i class="fa fa-image"></i>&nbsp;Detay</a>
                                <button data-url="<?php echo base_url("actual/delete/$item->actualid"); ?>"
                                        class="btn btn-outline btn-danger btn-xs btn-haberSil">
                                    <i class="fa fa-trash"></i>&nbsp;İptal Et
                                </button>

                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>

                </table>

            <?php } ?>

        </div><!-- .widget -->
    </div><!-- END column -->
</div>


