<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">Okul Bilgileri Düzenle</h4>
    </div>
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form method="post" action="<?php echo(base_url("school/update/$item->id")); ?>">

                    <div class="form-group">
                        <label>Okul Adı</label>
                        <input type="text" class="form-control" name="txtOkulAdi" placeholder="Okul Adı" value="<?= $item->school_name;?>">
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOkulAdi"); ?></small>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label>Okul Sorumlusu</label>
                        <input type="text" class="form-control" name="txtOkulSorumlu" placeholder="Okul Sorumlusunun Adı Soyadı" value="<?= $item->manager;?>">
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOkulSorumlu"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Okul Sorumlusu Telefon</label>
                        <input type="text" class="form-control" name="txtOkulSorumluTelefon" placeholder="Okul Sorumlusunun Cep Telefonu" value="<?= $item->manager_phone;?>">
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOkulSorumluTelefon"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>İl</label>
                        <select id="slcOkulIlDuzelt" name="slcOkulIlDuzelt" class="form-control">
                            <?php foreach ($cityList as $city) { ?>
                                <option <?php echo ($city->id == $item->city) ? "selected":""; ?> value="<?= $city->id ?>"><?= $city->cityname ?></option>
                            <?php } ?>
                        </select>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("slcOkulIlDuzelt"); ?></small>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label>İlçe</label>
                        <select id="slcOkulIlceDuzelt" name="slcOkulIlceDuzelt" class="form-control">
                            <?php
                            if(isset($firstLoad)){
                            foreach ($townLists as $town) { ?>
                                <option<?php echo ($town->id == $item->town) ? "selected":""; ?> value="<?= $town->id ?>"><?= $town->town; ?></option>
                            <?php }} ?>
                        </select>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("slcOkulIlceDuzelt"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Adres</label>
                        <textarea class="form-control" name="txtOkulAdres" placeholder="Okul Adresi"><?= $item->address;?></textarea>
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOkulAdres"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Okul Telefonu</label>
                        <input type="text" class="form-control" name="txtOkulTelefon" placeholder="Okul Telefonu" value="<?= $item->school_phone;?>">
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOkulTelefon"); ?></small>
                        <?php } ?>
                    </div>


                    <div class="form-group">
                        <label>Web Sitesi</label>
                        <input type="text" class="form-control" name="txtOkulWeb" placeholder="Okulun Web Sitesi" value="<?= $item->web;?>">
                        <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?= form_error("txtOkulWeb"); ?></small>
                        <?php } ?>
                    </div>





                    <button type="submit" class="btn btn-primary btn-outline">Kaydet</button>
                    <a href="<?php echo base_url("school"); ?>" class="btn btn-danger btn-outline">İptal</a>

                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->

    </div>
</div><!-- END column -->



