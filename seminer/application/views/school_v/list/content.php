<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Seminer Okulları Listesi</h4>

        <a href='<?php echo(base_url("school/newForm")); ?>'
           type="button"
           class="btn btn-outline btn-success btn-sm pull-right">
            <i class='fa fa-plus-circle'></i>&nbsp;Yeni Okul Ekle
        </a>

        <br><br>

        <?php if(empty($items)) {?>
        <div class="alert alert-danger" role="alert">
            <!-- <strong>Kayıt Yok! </strong><br> -->
            <span>Hiç kayıt bulunamadı...</span>
            <!-- <a href="#" class="alert-link"></a> -->
        </div>

        <?php
        } else {
            $i=1;
        ?>

        <table class="table table-bordered table-hover table-striped">
            <thead>
                <th>No</th>
                <th>Okul Adı</th>
                <th>Telefon</th>
                <th class="text-center">Durum</th>
                <th class="text-center">İşlem</th>
            </thead>
            <tbody>
            <?php foreach ($items as $item) { ?>
            <tr id="ord-<?= $item->id; ?>">
                <td><?= $i++; ?></td>
                <td><?= $item->school_name; ?></td>
                <td class="text-center"><?= $item->school_phone; ?></td>
                <td class="text-center">
                    <input
                            data-url="<?php echo base_url("school/isActiveSetter/$item->id")?>"
                            class="isActive"
                            type="checkbox"
                            data-switchery data-color="#35b8e0"
                            data-size="small"
                            <?= ($item->isActive) ? "checked" : ""; ?>
                    />
                </td>
                <td class="text-center">
                    <a href="<?php echo base_url("school/updateForm/$item->id"); ?>" class="btn btn-outline btn-info btn-xs">
                    <i class="fa fa-edit"></i>&nbsp;Düzenle</a>
                     <button data-url="<?php echo base_url("school/delete/$item->id"); ?>" class="btn btn-outline btn-danger btn-xs btn-haberSil"><i class="fa fa-trash"></i>&nbsp;Sil</button>

                </td>
            </tr>
            <?php } ?>
            </tbody>

        </table>

        <?php } ?>
    </div><!-- .widget -->
</div><!-- END column -->
</div>


