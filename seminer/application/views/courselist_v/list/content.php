<div class="row">
<div class="col-md-12">
    <div class="widget p-lg">
        <h4 class="m-b-lg">Eğitim Listesi</h4>
        <hr>


        <?php if(empty($items)) {?>
        <div class="alert alert-danger" role="alert">
            <!-- <strong>Kayıt Yok! </strong><br> -->
            <span>Hiç kayıt bulunamadı...</span>
            <!-- <a href="#" class="alert-link"></a> -->
        </div>

        <?php
        } else {
            $i=1;
        ?>

        <table class="table table-bordered table-hover table-striped">
            <thead>
                <th class="text-center tdws">No</th>
                <th>Eğitim Adı</th>
                <th class="text-center hidden-xs tdws">Tarih</th>
                <th class="text-center hidden-xs tdws">Saat</th>
                <th class="text-center hidden-xs tdws">Kontenjan</th>
                <th class="text-center tdws">Detay</th>
            </thead>
            <tbody>
            <?php foreach ($items as $item) { ?>
            <tr>
          
                <td><?= $i++; ?></td>
                <td><?= $item->name; ?></td>
                <td class="text-center hidden-xs "><?= date("d.m.Y",strtotime($item->startdate)); ?></td>
                <td class="text-center hidden-xs tdws"><?= date("H:i",strtotime($item->starttime))." - ". date("H:i",strtotime($item->endtime)); ?></td>
                <td class="text-center hidden-xs tdws"><?= $item->quota; ?></td>

                <td class="text-center tdws">
                    <a href="<?php echo base_url("courselist/detail/$item->courseid"); ?>" class="btn btn-outline btn-dark btn-xs">
                        <i class="fa fa-image"></i>&nbsp;Detay</a>

                </td>
            </tr>
            <?php } ?>
            </tbody>

        </table>

        <?php } ?>
    </div><!-- .widget -->
</div><!-- END column -->
</div>


