<?php

function convertToSEO($text){
    $turkce  = array("ç", "Ç", "ğ", "Ğ", "ü", "Ü", "ö", "Ö", "ı", "İ", "ş", "Ş", ".", ",", "!", "'", "\"", " ", "?", "*", "_", "|", "=", "(", ")", "[", "]", "{", "}");
    $convert = array("c", "c", "g", "g", "u", "u", "o", "o", "i", "i", "s", "s", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-");
    return strtolower(str_replace($turkce, $convert, $text));
}

function OzelKarakterTemizle($veri)
{
    $veri =str_replace("`","",$veri);
    $veri =str_replace(" ","",$veri);
    $veri =str_replace("=","",$veri);
    $veri =str_replace("&","",$veri);
    $veri =str_replace("%","",$veri);
    $veri =str_replace("!","",$veri);
    $veri =str_replace("#","",$veri);
    $veri =str_replace("<","",$veri);
    $veri =str_replace(">","",$veri);
    $veri =str_replace("*","",$veri);
    $veri =str_replace("And","",$veri);
    $veri =str_replace("'","",$veri);
    $veri =str_replace("chr(34)","",$veri);
    $veri =str_replace("chr(39)","",$veri);
    return $veri;
}
function upload_picture($file,$uploadPath,$width,$height,$name){
$t=&get_instance();
$t->load->library("simpleimagelib");
$upload_error=false;
try {
  $simpleImage = $t->simpleimagelib->get_simple_image_instance();
  $simpleImage
    ->fromFile($file)                   
    ->autoOrient()                              
    ->thumbnail($width, $height)                          
    ->toFile("{$uploadPath}/$name", 'image/png');                               
 
} catch(Exception $err) {

  $error = $err->getMessage();
  $upload_error=true;
}

if($upload_error){
echo $error;
}else{
return true;
}


}
function get_active_user(){
    $t = &get_instance();
    $user = $t->session->userdata("user");
    if($user)
        return $user;
    else
        return false;
}

function isAdmin(){
    $t = &get_instance();
    return true;
    $user = $t->session->userdata("user");
    if($user->user_role == "admin")
        return true;
    else
        return false;
}

//    function isAdmin(){
//        $t = &get_instance();
//
//        $user = $t->session->userdata("user");
//
//        return ($user->userrole == 1) ? true : false;
//    }

function get_user_roles(){
    $t = &get_instance();
    return  $t->session->userdata("user_roles");
}

//    function getUserRoles(){
//        $t = &get_instance();
//        setUserRoles();
//        return $t->session->userdata("user_roles");
//
//    }

function set_user_roles(){
    $t = &get_instance();
    $t->load->model("userrole_model");
    $userroles = $t->userrole_model->get_all(
        array(
            "isActive"      => 1
        )
    );
    $roles = [];
    foreach ($userroles as $role){
        $roles[$role->id] = $role->permissions;
    }
    $t->session->set_userdata("user_roles",$roles);

}

function getControllerList(){
    $t = &get_instance();
    $controllers = array();
    $t->load->helper("file");
    $files = get_dir_file_info(APPPATH."controllers",FALSE);
    foreach (array_keys($files) as $file){
        if($file !== "index.html"){
            $controllers[] = strtolower(str_replace(".php","", $file));
        }
    }
    return $controllers;

}



//////////////////////////////////////////////////////////////
///
///


//    function getControllerList(){
//        $t = &get_instance();
//        $controllers = array();
//        $t->load->helper("file");
//        $files = get_dir_file_info(APPPATH."controllers", FALSE);
//        foreach (array_keys($files) as $file){
//            if ($file != "index.html" && $file !="Product.php"){
//
//                $controllers[]=strtolower(str_replace(".php",'',$file));
//            }
//        }
//        return $controllers;
//    }
//
//

//    function setUserRoles(){
//        $t= &get_instance();
//        $t->load->model("user_role_model");
//
//        $user_roles = $t->user_role_model->get_all(
//            array(
//                "isActive"  =>  1
//            )
//        );
//        $roles = [];
//        foreach ($user_roles as $role){
//            $roles[$role->id] = $role->permissions;
//        }
//
//        $t->session->set_userdata("user_roles", $roles);
//    }