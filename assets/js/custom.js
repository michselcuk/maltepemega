$(document).ready(function () {

    $(".sortable").sortable();

    $(".btn-ogrenciSil").click(function () {

        var data_url = $(this).attr("data-url");

        Swal.fire({
            title: 'Uyarı!',
            text: "Öğrenci Silinsin mi?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Evet',
            cancelButtonText: 'İptal',
        }).then((result) => {
            if (result.value) {
                window.location.href = data_url;
            }
        })
    });

    $(".btn-haberSil").click(function () {

        var data_url = $(this).attr("data-url");

        Swal.fire({
            title: 'Uyarı!',
            text: "Kayıt Silinsin mi?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Evet',
            cancelButtonText: 'İptal',
        }).then((result) => {
            if (result.value) {
                window.location.href = data_url;
            }
        })
    });

    $(".isActive").change(function () {
        var data = $(this).prop("checked");
        var data_url = $(this).attr("data-url");

        if(typeof data!=="undefined" && typeof data_url!=="undefined"){
            $.post(data_url,{data:data},function (response) {})
        }
    })

    $(".sortable").on("sortupdate", function (event,ui) {
        var data = $(this).sortable("serialize");
        var data_url = $(this).data("url");
        $.post(data_url, {data:data}, function (response) {})

    })

   // $("#dateTimeSeminar").datetimepicker("locale:'tr'");

    $("#slcOkulIl").change(function () {
        var ilid=$(this).val();
        if(ilid){
            $.ajax({
                cache: false,
                url:'townList/'+ilid,
                type: 'post',
                dataType:'json',
                success:function(data){
                    $("#slcOkulIlce").empty();
                   $.each(data,function (key,value) {
                   $("#slcOkulIlce").append('<option value="'+value.id+'">'+value.town+'</option>')
                   })

                }

            });
        }else {
           $("#slcOkulIlce").empty();
        }
    })

    $("#slcOkulIlDuzelt").change(function (){

        var ilid=$(this).val();

        $("#slcOkulIlceDuzelt").empty();
        if(ilid){
            $.ajax({
                cache: false,
                url:'townList/'+ilid,
                type: 'post',
                dataType:'json',
                success:function(data){
                  alert("dsad");
                    $("#slcOkulIlceDuzelt").empty();
                    $.each(data,function (key,value) {
                        $("#slcOkulIlceDuzelt").append('<option value="'+value.id+'">'+value.town+'</option>')
                    })

                }

            });
        }else {
            $("#slcOkulIlceDuzelt").empty();
        }
    })

    

    $(".courseBtn").click(function () {

        Swal.fire({
            title: 'Uyarı!',
            text: "Eğitime Kaydınız  Yapılsın mı?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Evet',
            cancelButtonText: 'İptal',
        }).then((result) => {
            if (result.value) {
                var courseId = $(this).attr("data-course-id");
                var userId = $(this).attr("data-user-id");
                var url = "dashboard/courseSave";

                $.post(url, {courseOk:1,user:userId,course:courseId},function (resp) {
                    if(resp=="success"){
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'center',

                        })
                        Toast.fire({
                            type: 'success',
                            title: 'Eğitime Kaydoldunuz...'
                        })
                        setTimeout(function(){location.reload()}, 2000);

                    }
                    else {
                        Swal.fire({
                            type: 'error',
                            title: 'Hata',
                            text: resp,

                        })
                    }

                })
            }
        })
    })



$("#slcEgitimGunu").change(function (){
        //  $('.pazartesi').attr("disabled",true);
        $(".div_widget").empty();
        var gunId=$(this).val();
        $.getJSON('dashboard/courses', function(data){
            var widget ='';
            $.each(data, function(i, order){
                var tarih = order.startdate;
                var egitimadi = order.name;
                var kota = order.quota;

                widget ="<div class='col-md-4 col-sm-6'><div class='widget'><header class='widget-header'><h4 class='widget-title text-center'>"+tarih+"</h4></header><hr class='widget-separator'><div class='widget-body text-center'><h4 class='m-b-md'>"+egitimadi+"</h4><div class='clearfix'><div class='pieprogress text-success' data-plugin='circleProgress' data-value='.5' data-thickness='10' data-start-angle='-300' data-empty-fill='rgba(16, 196, 105,.3)' data-fill='{&quot;color&quot;: &quot;#10c469&quot;}'><strong>%80</strong></div><div class='pull-left'><h3 class='m-b-xs text-left counter'  data-plugin='counterUp'>"+kota+"</h3><small class='text-muted'>Kontenjan</small></div><div class='pull-right'><h3 class='m-b-xs text-right counter' data-plugin='counterUp'>20</h3><small class='text-muted'>Başvuru</small></div></div><br/><button class='btn p-v-xl btn-success' id='btnKurs'>Başvur</button></div></div></div>";
                if(order.gun==gunId)
                $(".div_widget").append(widget);

            });
        });
       })


    $("#slcEgitimGunu").change(function () {
        var gun = $(this).val();
            if(gun){
            $.post('dashboard/sessionDegistir', {gunID:gun},function () {
            })
                setTimeout(function(){location.reload()}, 200);
        }
    })


    $("#slcEgitimOkulu").change(function () {
        var okul = $(this).val();
        if(okul){
            $.post('dashboard/okulDegistir', {okulID:okul},function () {
            })
            setTimeout(function(){location.reload()}, 200);
        }
    })

        $("#slcEgitimSaat").change(function () {
        var saat = $(this).val();
       
        if(saat){
            $.post('dashboard/saatDegistir', {saatDeger:saat},function () {
            })
            setTimeout(function(){location.reload()}, 200);
        }
    })

    $("#btnEgitimDetayModal").click(function () {
        var courseId = $(this).attr("data-course-id");

        if(courseId){
            $.ajax({
                cache: false,
                url:'dashboard/modalCourseDetail/'+courseId,
                type: 'post',
                dataType:'json',
                success:function(data){
                    $("#lblEgitimAciklama").val(data[0].description);
                    $("#lblEgitimAdi").html(data[0].name);
                    $("#lblTarih").html(data[0].startdate);
                    $("#lblSaat").html(data[0].starttime);
                    $("#lblEgitimYeri").html(data[0].school_name);
                    $("#lblEgitimTuru").html(data[0].title);
                }

            });
        }

    })

})