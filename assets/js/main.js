$(document).ready(function(){

    $('.owl-one').owlCarousel({
        loop:true,
        nav:true,
        autoplay:true,
        autoplayTimeout:10000,
        autoplayHoverPause:true,
        smartSpeed: 1100,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items:1
            },
            1000: {
                items:1
            }
        }
    });

    $('.owl-two').owlCarousel({
      loop:false,
      nav:false,
      autoplay:true,
      autoplayTimeout:10000,
      autoplayHoverPause:true,
      smartSpeed: 500,
      responsive: {
            0: {
                items: 1
            },
            600: {
                items:1
            },
            1000: {
                items:5
            }
        }
    });

   $('.owl-three').owlCarousel({
        loop:false,
        nav:false,
        autoplay:true,
        margin: 10,
        autoplayTimeout:10000,
        autoplayHoverPause:true,
        smartSpeed: 500,
        responsive: {
                0: {
                    items: 1
                },
                600: {
                    items:1
                },
                1000: {
                    items:1
                }
        }
    });  

   $('.owl-four').owlCarousel({
        loop:false,
        nav:false,
        autoplay:true,
        margin: 10,
        autoplayTimeout:10000,
        autoplayHoverPause:true,
        smartSpeed: 500,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items:2
            },
            1000: {
                items:4
            }
        }
    });  

    $('.owl-five').owlCarousel({
        loop:false,
        nav:false,
        autoplay:true,
        margin: 10,
        autoplayTimeout:10000,
        autoplayHoverPause:true,
        smartSpeed: 500,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items:1
            },
            1000: {
                items:1
            }
        }
    });      

    $( ".owl-prev").html('<i class="fas fa-caret-circle-left"></i>');
    $( ".owl-next").html('<i class="fas fa-caret-circle-right">');
 

    var animationDelay = 2500;

    animateHeadline($('.cd-headline'));
    
    function animateHeadline($headlines) {
       $headlines.each(function(){
          var headline = $(this);
          //trigger animation
          setTimeout(function(){ hideWord( headline.find('.is-visible') ) }, animationDelay);
          //other checks here ...
       });
    }


    function hideWord($word) {
        var nextWord = takeNext($word);
        switchWord($word, nextWord);
        setTimeout(function(){ hideWord(nextWord) }, animationDelay);
     }
     
     function takeNext($word) {
        return (!$word.is(':last-child')) ? $word.next() : $word.parent().children().eq(0);
     }
     
     function switchWord($oldWord, $newWord) {
        $oldWord.removeClass('is-visible').addClass('is-hidden');
        $newWord.removeClass('is-hidden').addClass('is-visible');
     }


     singleLetters($('.cd-headline.letters').find('b'));

     function singleLetters($words) {
        $words.each(function(){
           var word = $(this),
               letters = word.text().split(''),
               selected = word.hasClass('is-visible');
           for (i in letters) {
              letters[i] = (selected) ? '<i class="in">' + letters[i] + '</i>': '<i>' + letters[i] + '</i>';
           }
           var newLetters = letters.join('');
           word.html(newLetters);
        });
     }

    $(".responsiveNavbar").click(function(){
        $(".navbarContent").toggleClass("active");
        $(".menu").toggleClass("active");
        $(".navbarWrapper").toggleClass("active");
    });

    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });

    $(".loginBtn").click(function(){
        $(".wrapper").toggleClass("active");
        $(".loginForm").toggleClass("active");
        $(".menu").removeClass("active");
        $(".navbarContent").removeClass("active");
        $(".navbarWrapper").removeClass("active");
        return false;
    });

    $(".wrapper").click(function(){
        $(this).toggleClass("active");
        $(".loginForm").removeClass("active");
        $(".menu").removeClass("active");
        $(".navbarContent").removeClass("active");

    });

    $(".navbarWrapper").click(function(){
        $(this).toggleClass("active");
        $(".menu").removeClass("active");
        $(".navbarContent").removeClass("active");

    });

    $('.circleChart').circlechart(); 
    

});
