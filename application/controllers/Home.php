<?php

class Home extends CI_Controller{
	public $viewFolder = "";

	public function __construct()
	{
		parent::__construct();
		$this->viewFolder = "homepage";
	}

	public function index()
	{
		$viewData = new stdClass();
		$viewData->viewFolder="home_v";
		$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->model("sayilar_model");
		$viewData->sayilar=$this->sayilar_model->get(
			array(
			)
		);
		$this->load->model("slider_image_model");
		$viewData->sliderimage=$this->slider_image_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);

		$this->load->helper("text");
		$this->load->model("duyurular_model");
		$viewData->duyuru=$this->duyurular_model->get(
			array(
				"aktif" =>1,
			),"sira ASC"
		);

		$this->load->model("project_general_model");
		$this->load->helper("text");
		$viewData->ilprojeleri=$this->project_general_model->get(
			array(
				"isActive" =>1,
			),"id DESC"
		);

		$this->load->model("project_model");
		$viewData->ilceprojeleri=$this->project_model->get(
			array(
				"publish" =>1
			),"rank ASC"
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}
	
	public function il_projeleri()
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="il_projeleri_v";
		$this->load->model("project_general_model");

		$this->load->helper("text");
		$viewData->ilprojeleri=$this->project_general_model->get_all(
			array(
				"isActive" =>1
			), "id DESC"
		);
		$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}
		public function ilce_projeleri()
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="ilce_projeleri_v";
		$this->load->model("project_model");
		$this->load->model("project_extras_model");
		$this->load->model("project_extras_image_model");
		$this->load->helper("text");
		$viewData->ilceprojeleri=$this->project_model->get_all(
			array(
			"publish" =>1
			),"rank ASC"
		);
		$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);

		$this->load->view($viewData->viewFolder, $viewData);
	}




	public function proje_detay($url)
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="proje_detay_v";
		$this->load->model("project_model");
		$this->load->model("project_extras_model");
		$this->load->model("project_extras_image_model");
		$this->load->helper("text");
		$viewData->ilceprojeleri=$this->project_model->get(
			array(
				"publish" =>1,
				"project_url" =>$url
			)
		);

		$viewData->proje_images=$this->project_model->project_extras_images(
			array(
				"isActive" =>1,
				"project_id" =>$viewData->ilceprojeleri->id,
			), "rank ASC"
		);

		$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}
	public function il_proje_detay($url)
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="il_proje_detay_v";
		$this->load->model("project_general_model");
		$this->load->model("project_general_extras_model");
		$this->load->model("project_general_extras_image_model");
		$this->load->helper("text");
		$viewData->ilprojeleri=$this->project_general_model->get(
			array(
				"isActive" =>1,
				"project_url" =>$url
			)
		);

		$viewData->proje_detay=$this->project_general_extras_model->project_extras(
			array(
				"project_general_id" =>$viewData->ilprojeleri->id,
			), "rank ASC"
		);
		$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);


		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}
		public function il_proje_detay_calismalar($parentid,$id)
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="il_proje_detay_calismalar_v";
		$this->load->model("project_general_model");
		$this->load->model("project_general_extras_model");
		$this->load->model("project_general_extras_image_model");
		$this->load->helper("text");
		$viewData->ilprojeleri=$this->project_general_model->get(
			array(
				"isActive" =>1,
				"id" =>$parentid
			)
		);

		$viewData->mevcut_proje_detay=$this->project_general_extras_model->project_extra(
			array(
				"project_general_id" =>$parentid,
				"id" => $id
			), "rank ASC"
		);
		$viewData->proje_images=$this->project_general_model->project_extras_images(
			array(
				"isActive" =>1,
				"project_general_id" =>$viewData->ilprojeleri->id,
				"project_general_extra_id" => $id,
			), "rank ASC"
		);
		$viewData->proje_detay=$this->project_general_extras_model->project_extras(
			array(
				"project_general_id" =>$viewData->ilprojeleri->id,
			), "rank ASC"
		);

		$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}




		public function duyurular()
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="duyurular_v";
		$this->load->model("duyurular_model");
		$this->load->helper("text");
		$viewData->duyurular=$this->duyurular_model->get_all(
			array(
				"aktif" =>1,
			),"sira ASC"
		);
$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}

	public function duyurular_detay($url)
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="duyurular_detay_v";
		$this->load->model("duyurular_model");
		$this->load->model("duyurular_image_model");
		$this->load->helper("text");
		$viewData->duyurular=$this->duyurular_model->get(
			array(
				"aktif" =>1,
				"url" =>$url
			)
		);

		$viewData->digerduyurular=$this->duyurular_model->get_all(
			array(
				"aktif" =>1,
				"id !=" =>$viewData->duyurular->id
			), "sira ASC", array("start" => 0, "count" =>3)
		);

		$viewData->duyurular_images=$this->duyurular_image_model->get_all(
			array(
				"aktif" =>1,
				"duyuru_id" =>$viewData->duyurular->id,
			), "sira ASC"
		);
$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}

	public function megatanitim()
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="megatanitim_v";
		$this->load->model("megatanitim_model");
		$this->load->model("megatanitim_image_model");
		$this->load->model("ekip_model");
		$this->load->helper("text");
		$viewData->megatanitim=$this->megatanitim_model->get(
			array(
			)
		);
		$viewData->megatanitim_images=$this->megatanitim_image_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$viewData->ekip=$this->ekip_model->get_all(
			array("aktif" =>1,
			),"sira ASC"
		);
		$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}

}