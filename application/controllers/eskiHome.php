<?php

class Home extends CI_Controller{
	public $viewFolder = "";

	public function __construct()
	{
		parent::__construct();
		$this->viewFolder = "homepage";
	}

	public function index()
	{
		$viewData = new stdClass();
		$viewData->viewFolder="home_v";
		$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->model("sayilar_model");
		$viewData->sayilar=$this->sayilar_model->get(
			array(
			)
		);
		$this->load->model("slider_image_model");
		$viewData->sliderimage=$this->slider_image_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);

		$this->load->helper("text");
		$this->load->model("duyurular_model");
		$viewData->duyuru=$this->duyurular_model->get(
			array(
				"aktif" =>1,
			),"sira ASC"
		);
		$this->load->model("projeler_model");
		$viewData->ilprojeleri=$this->projeler_model->get(
			array(
				"aktif" =>1,
				"il" => 1
			),"sira ASC"
		);
		$viewData->ilceprojeleri=$this->projeler_model->get(
			array(
				"aktif" =>1,
				"ilce" => 1
			),"sira ASC"
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}
	
	public function il_projeleri()
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="il_projeleri_v";
		$this->load->model("projeler_model");

		$this->load->helper("text");
		$viewData->ilprojeleri=$this->projeler_model->get_all(
			array(
				"aktif" =>1,
				"il" => 1
			),"sira ASC"
		);
		$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}
		public function ilce_projeleri()
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="ilce_projeleri_v";
		$this->load->model("projeler_model");
		$this->load->helper("text");
		$viewData->ilceprojeleri=$this->projeler_model->get_all(
			array(
				"aktif" =>1,
				"ilce" => 1
			),"sira ASC"
		);
$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);


		$this->load->view($viewData->viewFolder, $viewData);
	}




	public function proje_detay($url)
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="proje_detay_v";
		$this->load->model("projeler_model");
		$this->load->model("projeler_image_model");
		$this->load->helper("text");
		$viewData->ilprojeleri=$this->projeler_model->get(
			array(
				"aktif" =>1,
				"url" =>$url
			)
		);
		$viewData->proje_images=$this->projeler_image_model->get_all(
			array(
				"aktif" =>1,
				"proje_id" =>$viewData->ilprojeleri->id,
			), "sira ASC"
		);
$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}

		public function duyurular()
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="duyurular_v";
		$this->load->model("duyurular_model");
		$this->load->helper("text");
		$viewData->duyurular=$this->duyurular_model->get_all(
			array(
				"aktif" =>1,
			),"sira ASC"
		);
$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}

	public function duyurular_detay($url)
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="duyurular_detay_v";
		$this->load->model("duyurular_model");
		$this->load->model("duyurular_image_model");
		$this->load->helper("text");
		$viewData->duyurular=$this->duyurular_model->get(
			array(
				"aktif" =>1,
				"url" =>$url
			)
		);

		$viewData->digerduyurular=$this->duyurular_model->get_all(
			array(
				"aktif" =>1,
				"id !=" =>$viewData->duyurular->id
			), "sira ASC", array("start" => 0, "count" =>3)
		);

		$viewData->duyurular_images=$this->duyurular_image_model->get_all(
			array(
				"aktif" =>1,
				"duyuru_id" =>$viewData->duyurular->id,
			), "sira ASC"
		);
$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}

	public function megatanitim()
	{
		
		$viewData = new stdClass();
		$viewData->viewFolder="megatanitim_v";
		$this->load->model("megatanitim_model");
		$this->load->model("megatanitim_image_model");
		$this->load->model("ekip_model");
		$this->load->helper("text");
		$viewData->megatanitim=$this->megatanitim_model->get(
			array(
			)
		);
		$viewData->megatanitim_images=$this->megatanitim_image_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$viewData->ekip=$this->ekip_model->get_all(
			array("aktif" =>1,
			),"sira ASC"
		);
		$this->load->model("partner_model");
		$viewData->partner=$this->partner_model->get_all(
			array(
				"aktif" =>1,
			), "sira ASC"
		);
		$this->load->model("settings_model");
		$viewData->ayar=$this->settings_model->get(
			array(
			)
		);
		$this->load->view($viewData->viewFolder, $viewData);
	}

}