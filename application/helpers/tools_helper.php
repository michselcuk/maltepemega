<?php

function kapak_getir_ilce($proje_id){
	$t=get_instance();
	$t->load->model("project_model");
	$cover_image=$t->project_model->project_extras_images(
			array(
				"isActive" =>1,
				"project_id" =>$proje_id,
				"isCover" => 1
			), "rank ASC"
		);


if(empty($cover_image)){
	$cover_image=$t->project_model->project_extras_images(
			array(
				"isActive" =>1,
				"project_extra_id" =>$proje_id,
			), "rank ASC"
		);
}

foreach ($cover_image as $cimage) {

	return !empty($cimage) ? $cimage->img_url:"";
}



}
function kapak_getir_il($proje_id){
	$a=get_instance();
	$a->load->model("project_general_model");
	$cover_image=$a->project_general_model->project_extras_images(
			array(
				"isActive" =>1,
				"project_general_id" =>$proje_id,
				"isCover" => 1
			), "rank ASC"
		);


if(empty($cover_image)){
	$cover_image=$a->project_general_model->project_extras_images(
			array(
				"isActive" =>1,
				"project_general_extra_id" =>$proje_id,
			), "rank ASC"
		);
}

foreach ($cover_image as $cimage) {

	return !empty($cimage) ? $cimage->img_url:"";
}



}

function duyurukapak_getir($duyuru_id){
	$t=get_instance();
	$t->load->model("duyurular_image_model");
	$cover_image=$t->duyurular_image_model->get(
		array(
		"isCover" => 1,
		"aktif" => 1,
		"duyuru_id" => $duyuru_id
		)
	);
if(empty($cover_image)){
	$cover_image=$t->duyurular_image_model->get(
		array(
		"aktif" => 1,
		"duyuru_id" => $duyuru_id
		)
	);


}

return !empty($cover_image) ? $cover_image->img_url:"";


}







