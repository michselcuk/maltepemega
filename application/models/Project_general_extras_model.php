<?php
    class Project_general_extras_model extends CI_Model{
        //tablo adı tanımla
        public $tableName = "project_general_extras";

        public function __construct()
        {
            parent::__construct();
        }
        //tüm projeleri getir
        public function get_all($where=array()){
            return $this->db->where($where)->get($this->tableName)->result();
        }
        //tel veri getir
        public function get($where=array()){
            return $this->db->where($where)->get($this->tableName)->row();
        }
        //ekle
        public function add($data = array() ){
            return $this->db->insert($this->tableName, $data);
        }
        //sil
        public function delete($where = array()){
            return $this->db->where($where)->delete($this->tableName);
        }
        //güncelle
        public function update($where = array(),$data = array()){
            return $this->db->where($where)->update($this->tableName,$data);
        }
        //tüm projelerden kullanıcılar ile eşleşenleri ve projelerden okul ile eşleşenleri getir.
        public function projects_users_schools($where = array(),$order = "id ASC"){
            $this->db->select("projects.*, users.full_name,schools.title AS school_name")
                ->from("projects")
                ->join("users","projects.user_id = users.id")
                ->join("schools","projects.school_id=schools.id");
            return $this->db->where($where)->order_by($order)->get()->result();
        }
        public function project_general_extras($where = array())
        {
            $this->db->select("project_general_extras.*, 
        project_general.id AS project_general_id, project_general.title AS project_title")
                ->from("project_general_extras")
                ->join("project_general","project_general_extras.project_general_id = project_general.id");
            return $this->db->where($where)->get()->result();
        }
        public function project_user_school($where = array())
        {
            $this->db->select("projects.*, users.full_name,schools.title AS school_name")
                ->from("projects")
                ->join("users","projects.user_id = users.id")
                ->join("schools","projects.school_id=schools.id");
            return $this->db->where($where)->get()->row();
        }
        //giriş yapan kullanıcının okul bilgileri
        public function user_school_info($where = array())
        {
            $school_info = $this->db->select("*")->from("schools")->where($where)->get()->row();
            return $school_info;

        }
        //giriş yapan kullancı bilgileri
        public function user_info()
        {
            $user = get_active_user();
            return $user;

        }
        //istenen kullanıcıya ait projeleri getirir
        public function projects_user($where = array(), $order = "id ASC")
        {
            return $this->db->select("*")->where($where)->order_by($order)->get($this->tableName)->result();
        }

        public function project_extras($where=array())
        {
            return $this->db->select("*")->from("project_general_extras")->where($where)->get()->result();

        }
                public function project_extra($where=array())
        {
            return $this->db->select("*")->from("project_general_extras")->where($where)->get()->row();

        }
//SELECT * FROM `project_extras`
// JOIN project_extras_images ON (project_extras.id = project_extras_images.project_extra_id)
// WHERE project_extras.project_id = 6
        public function project_general_extras_images($where=array())
        {
            $this->db->select("*")->from("project_general_extras")
                ->join("project_general_extras_images","project_general_extras.id=project_general_extras_images.project_general_extra_id");
            return $this->db->where($where)->get()->result();
        }
    }