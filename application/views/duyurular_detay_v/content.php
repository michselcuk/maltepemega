    <!-- content -->
    <section id="content">

        <!-- back to page -->
        <div class="backToPage">
            <div class="container">
                <div class="row">
                    <div class="col-12 border-top-3">
                        <a href="<?php echo base_url("duyurular"); ?>">Geri Dön</a>
                    </div>
                </div>
            </div>            
        </div>

        <div class="container">
            <div class="row">
                <!-- announcements detail -->
                <div class="col-lg-9">
                    <!--  announcements image -->
                    <div class="announcementsImage">
                        <div class="owl-five owl-carousel owl-theme">
                             <?php foreach($duyurular_images as $images){ ?>
                            <img src="<?php echo base_url("panel/uploads/duyurular_v/$images->img_url") ?>" alt="News Image">
                      <?php } ?>
                        </div>
                    </div>
                    <!-- announcements text -->
                    <div class="announcementsText">
                        <!-- title -->
                        <h4><?php echo $duyurular->baslik; ?></h4>
                        <!-- text -->
                        <?php echo $duyurular->aciklama; ?>   
                        
                    </div>
                </div>
                <!-- other announcements  -->
                <div class="col-lg-3 otherAnnouncements">
                    <h4>Diğer Duyuru ve Haberler</h4>
                             <?php foreach($digerduyurular as $dduyuru){ ?>
                    <!-- other announcements 1 -->
                    <div class="otherAnnouncementsContent">
<?php
    $image = duyurukapak_getir($dduyuru->id);
    $images = ($image) ? base_url("panel/uploads/duyurular_v/$image") : base_url("panel/uploads/projeler_v/proje.jpg");
?>
                        <img src="<?php echo $images; ?>" alt="other announcements">
                        <!-- title -->
                        <span>
                            

                            <a href="<?php echo base_url("duyurular-detay/$dduyuru->url"); ?>"><?php echo $dduyuru->baslik; ?></a></span>
                    </div>
<?php }?>

                </div>
            </div>
        </div>