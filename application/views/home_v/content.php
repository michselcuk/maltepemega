
    <!-- content -->
    <section id="content">
        <!-- slogan -->
        <div class="container">
            <div class="row">
                <div class="col-12">
                        <div class="cd-intro">
                            <!-- slogan content -->
                            <h1 class="cd-headline rotate-1">
                                <span>MEGA
                                <span class="cd-words-wrapper">
                                    <b class="is-visible">Yenilikçi</b>
                                    <b>Eğitimli</b>
                                    <b>Özgün</b>
                                </span>
                                çalışmalara imza atar.
                                </span>
                            </h1>
                            <p>Siz değerli öğrencilerimizin hayallerini gerçeğe dönüştürmek için özenli çalışmalar sunuyoruz.</p>
                        </div> 
                </div>                
            </div>
        </div>
		<?php if($duyuru && $ilprojeleri && $ilceprojeleri){ ?>
        <!-- cards -->
        <div class="container">
            <div class="row">
                <!-- announcements -->
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 d-flex justify-content-center  justify-content-lg-start">
                    <!-- card content -->
                    <div class="card">
<?php
    $image = duyurukapak_getir($duyuru->id);
    $images = ($image) ? base_url("panel/uploads/duyurular_v/$image") : base_url("panel/uploads/projeler_v/proje.jpg");
?>
                        <div class="cardImage">
                            <img src="<?php echo $images ?>" alt="Duyurular">
                        </div>
                        <!-- card text -->
                        <div class="cardText" style="height:300px;">
                            <h4>Duyurular</h4>
                            <p><?php echo $duyuru->baslik.character_limiter(strip_tags($duyuru->aciklama),60); ?> </p>
                        </div>
                        <!-- card link -->
                        <div class="cardLink">
                           <a href="duyurular"><i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- project -->
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 d-flex justify-content-center">
                    <!-- card content -->
                    <div class="card">

                        <div class="cardImage">
                            <img src="<?php echo base_url("seminer/uploads/project_general_v/$ilprojeleri->img_url"); ?>" alt="il projeleri">
                        </div>
                        <!-- card text -->
                        <div class="cardText" style="height:300px;">
                            <h4>İl Projeleri</h4>
                            <p><?php echo $ilprojeleri->title."<br>".character_limiter(strip_tags($ilprojeleri->description),60); ?> </p>
                        </div>
                        <!-- card link -->
                        <div class="cardLink">
                           <a href="il-projeleri"><i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- project -->
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 d-flex justify-content-center  justify-content-lg-end">
                    <!-- card content -->
                    <div class="card">
<?php
    $image = kapak_getir_ilce($ilceprojeleri->id);
    $images = ($image) ? base_url("seminer/uploads/project_extras_v/$image") : base_url("panel/uploads/projeler_v/proje.jpg");
?>
                        <div class="cardImage">
                            <img src="<?php echo $images ?>" alt="Duyurular">
                        </div>
                        <!-- card text -->
                        <div class="cardText" style="height:300px;">
                            <h4>İlçe Projeleri</h4>
                            <p><?php echo $ilceprojeleri->title."<br>".character_limiter(strip_tags($ilceprojeleri->description),60); ?></p>
                        </div>
                        <!-- card link -->
                        <div class="cardLink">
                           <a href="ilce-projeleri"><i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php } ?>
        <!-- countup -->
        <div class="container">
            <div class="row py-auto py-sm-auto py-md-auto py-lg-5">
                <!-- student -->
                                <!-- student -->
                <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                    <div class="countupContainer">
                        <!-- countup icon -->
                        <div class="countupIcon">
                            <img src="<?php echo base_url("assets/images/icons/university.png"); ?>" alt="Okul">
                        </div>
                        <!-- countup text -->
                        <div class="countupText">
                            <span>+</span>
                            <span class="counter"><?php echo $sayilar->okul; ?></span>
                            <p>Okul</p>
                        </div>                        
                    </div>
                </div> 
                 <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                    <div class="countupContainer">
                        <!-- countup icon -->
                        <div class="countupIcon">
                            <img src="<?php echo base_url("assets/images/icons/graduated-student-avatar.png"); ?>" alt="Öğretmen">
                        </div>
                        <!-- countup text -->
                        <div class="countupText">
                            <span>+</span>
                            <span class="counter"><?php echo $sayilar->ogretmen; ?></span>
                            <p>Öğretmen</p>
                        </div>                        
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                    <div class="countupContainer">
                        <!-- countup icon -->
                        <div class="countupIcon">
                            <img src="<?php echo base_url("assets/images/icons/reading.png"); ?>" alt="Öğrenci">
                        </div>
                        <!-- countup text -->
                        <div class="countupText">
                            <span>+</span>
                            <span class="counter"><?php echo $sayilar->ogrenci; ?></span>
                            <p>Öğrenci</p>
                        </div>                        
                    </div>
                </div>
                <!-- teacher -->
               
                <!-- student -->
                <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                    <div class="countupContainer">
                        <!-- countup icon -->
                        <div class="countupIcon">
                            <img src="<?php echo base_url("assets/images/icons/derslik.png"); ?>" alt="Veli">
                        </div>
                        <!-- countup text -->
                        <div class="countupText">
                            <span>+</span>
                            <span class="counter"><?php echo $sayilar->derslik; ?></span>
                            <p>Derslik</p>
                        </div>                        
                    </div>
                </div>
                                                           
            </div>                
        </div>
      