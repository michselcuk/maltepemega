        
      <!-- content -->
    <section id="content">

        <!-- projects image -->
        <div class="container pt-4 pt-sm-4 pt-md-4 pt-lg-5"> 
            <div class="row">
                <div class="col-lg-6">
                    <div class="owl-three owl-carousel owl-theme">
                    <?php foreach($proje_images as $images){ ?>
                        <div class="subpageSliderImage">
                            <img src="<?php echo base_url("seminer/uploads/project_extras_v/$images->img_url") ?>" alt="<?php echo $ilceprojeleri->title?>">
                        </div>
                    <?php } ?>                                         
                    </div>
                </div>
                <!-- projects text -->

                <div class="col-lg-6">
                    <div class="projectText">
                        <!-- project title -->                        
                        <h4><?php echo $ilceprojeleri->title?></h4>
                        <!-- project description -->
                       <?php echo $ilceprojeleri->description?>
                        <!-- project date -->
                       
                    </div>
                </div>
            </div>
        </div>
        <div class="container py-4 py-sm-4 py-md-4 py-lg-4">
            <div class="row">
                <div class="col-12 text-center">
                    <!-- main title -->
                    <div class="mainTitle">
                        <div class="aboutUsTitle">
                            <h2>Proje katılımcı sayısı</h2>
                        </div>                   
                        <p>Projeye katılan okul, öğretmen ve öğrenci sayıları</p>   
                    </div>
                </div>
            </div>
        </div>        

        <!-- countup -->
        <div class="container">
            <div class="row py-auto py-sm-auto py-md-auto py-lg-5">
                <!-- student -->
                                <!-- student -->
                <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="countupContainer">
                        <!-- countup icon -->
                        <div class="countupIcon">
                            <img src="<?php echo base_url("assets/images")?>/icons/university.png" alt="Okul">
                        </div>
                        <!-- countup text -->
                        <div class="countupText">
                            <span>+</span>
                            <span class="counter">1</span>
                            <p>Okul</p>
                        </div>                        
                    </div>
                </div> 
                 <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="countupContainer">
                        <!-- countup icon -->
                        <div class="countupIcon">
                            <img src="<?php echo base_url("assets/images")?>/icons/graduated-student-avatar.png" alt="Öğretmen">
                        </div>
                        <!-- countup text -->
                        <div class="countupText">
                            <span>+</span>
                            <span class="counter"><?php echo $ilceprojeleri->project_teachers_count?></span>
                            <p>Öğretmen</p>
                        </div>                        
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="countupContainer">
                        <!-- countup icon -->
                        <div class="countupIcon">
                            <img src="<?php echo base_url("assets/images")?>/icons/reading.png" alt="Öğrenci">
                        </div>
                        <!-- countup text -->
                        <div class="countupText">
                            <span>+</span>
                            <span class="counter"><?php echo $ilceprojeleri->project_students_count?></span>
                            <p>Öğrenci</p>
                        </div>                        
                    </div>
                </div>
                <!-- teacher -->                                                           
            </div>                
        </div>

      