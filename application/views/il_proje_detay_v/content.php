        
      <!-- content -->
    <section id="content">

        <!-- projects image -->
        <div class="container pt-4 pt-sm-4 pt-md-4 pt-lg-5"> 
            <div class="row">
                <div class="col-lg-6">
                    <div class="owl-three owl-carousel owl-theme">
                        <div class="subpageSliderImage">
                            <img src="

                        <?php if($ilprojeleri->img_url){
                          echo base_url("seminer/uploads/project_general_v/$ilprojeleri->img_url");  						  
                        }else {

                          echo base_url("panel/uploads/projeler_v/proje.jpg");
                        }
                            ?>
                        " alt="<?php echo $ilprojeleri->title?>">
                        </div>                  
                    </div>
                </div>
                <!-- projects text -->

                <div class="col-lg-6">
                    <div class="projectText">
                        <!-- project title -->                        
                        <h4><?php echo $ilprojeleri->title?></h4>
                        <!-- project description -->
                       <?php echo $ilprojeleri->description?>
                        <!-- project date -->
                       
                    </div>
                </div>
            </div>
        </div>

<?php if($proje_detay) { ?>        
        <div class="container py-4 py-sm-4 py-md-4 py-lg-4">
            <div class="row">
                <div class="col-12 text-center">
                    <!-- main title -->
                    <div class="mainTitle">
                        <div class="aboutUsTitle">
                            <h2>Proje katılımcı sayısı</h2>
                        </div>                   
                        <p>Projeye katılan okul, öğretmen ve öğrenci sayıları</p>   
                    </div>
                </div>
            </div>
        </div>        
<?php 
$ogrtoplam=0;
$ogrenci=0;
$okulid=array();
foreach ($proje_detay as $pdetay) {
    $ogrtoplam=$pdetay->project_teachers_count+$ogrtoplam;
    $ogrenci=$pdetay->project_students_count+$ogrenci;
    array_push($okulid, $pdetay->school_id);
}
$okulid=array_unique($okulid);
?>
        <!-- countup -->
        <div class="container">
            <div class="row py-auto py-sm-auto py-md-auto py-lg-5">
                <!-- student -->
                                <!-- student -->
                <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="countupContainer">
                        <!-- countup icon -->
                        <div class="countupIcon">
                            <img src="<?php echo base_url("assets/images")?>/icons/university.png" alt="Okul">
                        </div>
                        <!-- countup text -->
                        <div class="countupText">
                            <span>+</span>
                            <span class="counter"><?php echo count($okulid); ?></span>
                            <p>Okul</p>
                        </div>                        
                    </div>
                </div> 
                 <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="countupContainer">
                        <!-- countup icon -->
                        <div class="countupIcon">
                            <img src="<?php echo base_url("assets/images")?>/icons/graduated-student-avatar.png" alt="Öğretmen">
                        </div>
                        <!-- countup text -->
                        <div class="countupText">
                            <span>+</span>
                            <span class="counter"><?php echo $ogrtoplam; ?></span>
                            <p>Öğretmen</p>
                        </div>                        
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="countupContainer">
                        <!-- countup icon -->
                        <div class="countupIcon">
                            <img src="<?php echo base_url("assets/images")?>/icons/reading.png" alt="Öğrenci">
                        </div>
                        <!-- countup text -->
                        <div class="countupText">
                            <span>+</span>
                            <span class="counter"><?php echo  $ogrenci; ?></span>
                            <p>Öğrenci</p>
                        </div>                        
                    </div>
                </div>
                <!-- teacher -->                                                           
            </div>                
        </div>



        <div class="container py-4 py-sm-4 py-md-4 py-lg-4">
            <div class="row">
                <div class="col-12 text-center">
                    <!-- main title -->
                    <div class="mainTitle">
                        <div class="aboutUsTitle">
                            <h2>Proje çalışmaları</h2>
                        </div>                   
                    </div>
                </div>
            </div>
        </div>   
        <div class="container py-auto py-sm-auto py-md-auto py-lg-5 mt-5 mt-lg-auto">
            <div class="row">
                <div class="col-12">
                    <div class="owl-two owl-carousel owl-theme">
            <?php        foreach ($proje_detay as $pdetay) {     ?>     
                        <div>

 <?php
    $image = kapak_getir_il($pdetay->id);
    $images = ($image) ? base_url("seminer/uploads/project_general_extras_v/$image") : base_url("panel/uploads/projeler_v/proje.jpg");
?> 
                        <a href="" target="_blank"><img src="

                        <?php if($images){
                          echo $images;  
                        }else{
                          echo base_url("panel/uploads/projeler_v/proje.jpg");
                        }?>" alt="proje"></a><br><br>
                        <div class="cardText" style="height:300px;">
                            <h4><?php echo $pdetay->title; ?></h4>
                            <p><?php echo character_limiter(strip_tags($pdetay->description),20); ?> </p>


                        </div>
                        <div class="cardLink">
                           <a href="<?php echo base_url("il-proje-detay-calismalar/$ilprojeleri->id/$pdetay->id"); ?>"><i class="fas fa-chevron-right"></i></a>
                        </div>
                        </div>                     
  <?php       }    ?>   
                    </div>                                                                            
                </div>
            </div>
        </div>
    <?php       }    ?>    