        
    <!-- content -->
    <section id="content">

        <!-- page title -->
        <div class="container mt-5">
            <div class="row">
                <div class="col-12 text-center">
                    <!-- main title -->
                    <div class="mainTitle">
                        <div class="aboutUsTitle">
                            <h1>İl Projeleri</h1>
                        </div>    
                        <p>İstanbul İl Milli Eğitim Müdürlüğü tarafından yürütülen projeler</p>                
                    </div>
                </div>
            </div>
        </div>

        <div class="container  py-4 py-sm-4 py-md-4 py-lg-5 projects">
            <div class="row">
                <!-- projects 1-->
                <?php foreach($ilprojeleri as $proje){ ?>

                <div class="col-12 col-sm-12 col-md-6 col-lg-4 d-flex justify-content-center  justify-content-lg-center">
                    <!-- card content -->
                    <div class="card">
                        <!-- card image -->
                        <div class="cardImage">

<?php
    $image = kapak_getir($proje->id);
    $images = ($image) ? base_url("panel/uploads/projeler_v/$image") : base_url("panel/uploads/projeler_v/proje.jpg");
?>

                            <img src="<?php echo $images ?>" alt="<?php echo $proje->adi; ?>">
                        </div>
                        <!-- card text -->
                        <div class="cardText">
                            <h4><?php echo $proje->adi?></h4>
                            <p><?php echo character_limiter(strip_tags($proje->aciklama),90); ?> </p>
                        </div>
                        <!-- card link -->
                        <div class="cardLink">
                           <a href="<?php echo base_url("proje-detay/$proje->url"); ?>"><i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>   
  
            <?php } ?>

            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination d-flex justify-content-center">
                            <li class="page-item"><a class="page-link" href="#">İlk Sayfa</a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">Son Sayfa</a></li>
                        </ul>
                    </nav>                       
                </div>
            </div>
        </div>

      