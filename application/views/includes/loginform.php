
    <!-- navbar wrapper -->
    <div class="navbarWrapper"></div>

    <!-- login form -->
    <div class="loginForm">
        <!-- login logo -->
        <div class="loginLogo">
            <img src="<?php echo base_url("assets") ?>/images/logo/white-logo.png" alt="MEGA - Maltepe Eğitim ve Gelişim Akademisi Logo">
        </div>
        <!-- login title -->
        <span class="loginTitle">Giriş Yap</span>
        <!-- login form conten -->
        <form action="" method="">
            <!-- user name -->
            <label>
                <span class="loginFormIcon"><i class="fas fa-user"></i></span>
                <input type="text" name="userName" autocomplete="off" placeholder="Kullanıcı Adı" required>
            </label>
            <!-- user password -->
            <label>
                <span class="loginFormIcon"><i class="fas fa-key"></i></span>
                <input type="password" name="userPassword" autocomplete="off" placeholder="Şifre" required>
            </label>
            <!-- checkbox -->
            <label class="checkbox">
                <input type="checkbox" name="rememberMe">
                <span class="checkbox"></span>
                <span class="text">Beni Hatırla</span>
            </label>
            <!-- submit -->
            <button type="submit" name="send" value="1">Giriş</button>
        </form>
    </div>
     <!-- wrapper -->
    <div class="wrapper"></div>