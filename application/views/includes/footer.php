  <!-- partnership -->
        <div class="container py-auto py-sm-auto py-md-auto py-lg-5 mt-5 mt-lg-auto">
            <div class="row">
                <div class="col-12 partnership">
                    <div class="owl-two owl-carousel owl-theme">
                             <?php foreach($partner as $partners){ ?>
                            <div class="partnershipImage">
                                <a href="<?php echo $partners->link; ?>" target="_blank"><img src="<?php echo base_url("panel/uploads/partner_v/$partners->img_url"); ?>" alt="<?php echo $partners->ad; ?>"></a>
                            </div>                     
                           <?php } ?>
                    </div>                                                                            
                </div>
            </div>
        </div>
        
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <!-- footer logo -->
                <div class="col-12 col-sm-12 col-md-12 col-lg-2">
                    <a href="<?php echo base_url(); ?>" title="MEGA - Maltepe Eğitim ve Gelişim Akademisi"><img src="<?php echo base_url("assets") ?>/images/logo/white-logo.png" alt="MEGA - Maltepe Eğitim ve Gelişim Akademisi Logo"></a>
                </div>
                <!-- footer menu -->
                <div class="col-12 col-sm-12 col-md-12 col-lg-2">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>" title="Ana Sayfa">Ana Sayfa</a></li>
                        <li><a href="<?php echo base_url("megatanitim"); ?>" title="MEGA">MEGA</a></li>
                        <li><a href="<?php echo base_url("il-projeleri"); ?>" title="İl Projeleri">İl Projeleri</a></li>
                        <li><a href="<?php echo base_url("ilce-projeleri"); ?>" title="İlçe Projeleri">İlçe Projeleri</a></li>
                        <li><a href="<?php echo base_url("duyurular"); ?>" title="MEGA'dan">MEGA'dan</a></li>
                    </ul>                    
                </div>
                <!-- footer contact -->
                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                    <p><i class="fas fa-map-marker-alt"></i>
                    <?php echo $ayar->adres; ?></p>
                    <p><i class="fas fa-phone-volume"></i><?php echo $ayar->telefon; ?></p>
                    <p><i class="fas fa-envelope"></i><a href="mailto:<?php echo $ayar->mail; ?>"><?php echo $ayar->mail; ?></a></p>
                </div>
                <!-- footer maps -->
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 d-flex justify-content-center justify-content-lg-end">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1507.017940014155!2d29.124439033204645!3d40.936880823736566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cac6963a01215d%3A0xf0ea60250202d3a9!2zTWFsdGVwZSDEsGzDp2UgTWlsbGkgRcSfaXRpbSBNw7xkw7xybMO8xJ_DvA!5e0!3m2!1str!2str!4v1570219030548!5m2!1str!2str" width="90%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe></div>
            </div>
        </div>
        <div class="copyright">
            <a href="http://maltepe.meb.gov.tr/" target="_blank" title="Maltepe İlçe Milli Eğitim Müdürlüğü"><?php echo $ayar->footer; ?> &copy; <?php echo date("Y"); ?></a>
        </div>
    </footer>