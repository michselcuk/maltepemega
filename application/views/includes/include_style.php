    <link rel="stylesheet" href="<?php echo base_url("assets") ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url("assets") ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url("assets") ?>/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url("assets") ?>/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url("assets") ?>/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url("assets") ?>/css/responsive.css">