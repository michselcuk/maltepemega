    <script src="<?php echo base_url("assets") ?>/js/jquery.3.4.1.min.js"></script>
    <script src="<?php echo base_url("assets") ?>/js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url("assets") ?>/js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url("assets") ?>/js/progresscircle.js"></script>
    <script src="<?php echo base_url("assets") ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url("assets") ?>/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url("assets") ?>/js/main.js"></script>