    <section id="slider">
            <div class="owl-one owl-carousel owl-theme">
<?php foreach($sliderimage as $slider) { ?>
                <div class="sliderContent">
                    <!-- slider image -->
                    <div class="sliderImage">
                        <img src="<?php echo base_url("panel/uploads/slider_v/$slider->img_url") ?>" alt="Slider Image 1">
                    </div>
                    <!-- slider text -->
                    <div class="sliderText">
                        <div class="sliderTextDetail">
                                <h1><?php echo $slider->slogan; ?></h1>
                                <?php echo $slider->aciklama; ?>
                        </div>
                    </div>
                </div>
               <?php } ?> <!-- slider content 2 -->
                
            </div>
    </section>