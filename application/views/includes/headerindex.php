
<header >
        <div class="mainContainer">
            <!-- logo -->
            <div class="logo">
                <a href="<?php echo base_url("home"); ?>" title="<?php echo $ayar->title; ?>"><img src="<?php echo base_url("panel/uploads/settings_v/$ayar->logo") ?>" alt="MEGA - Maltepe Eğitim ve Gelişim Akademisi Logo"></a>
            </div>
            <!-- menu -->
            <div class="menu">
                <ul>
                     <li><a href="<?php echo base_url("home"); ?>" title="Ana Sayfa">Ana Sayfa</a></li>
                    <li><a href="<?php echo base_url("megatanitim"); ?>" title="MEGA">MEGA</a></li>
                    <li><a href="<?php echo base_url("il-projeleri"); ?>" title="İl Projeleri">İl Projeleri</a></li>
                    <li><a href="<?php echo base_url("ilce-projeleri"); ?>" title="İlçe Projeleri">İlçe Projeleri</a></li>
                    <li><a href="<?php echo base_url("duyurular"); ?>" title="MEGA'dan">MEGA'dan</a></li>
                    <li><a href="<?php echo base_url("seminer/"); ?>"  title="Login">Login</a></li>
                </ul>
            </div>
            <!-- responsive navbar icon -->
            <div class="responsiveNavbar">
                <div class="navbarContent">
                    <span class="line line01"></span>
                    <span class="line line02"></span>
                    <span class="line line03"></span>
                </div>
            </div>
        </div>
    </header>