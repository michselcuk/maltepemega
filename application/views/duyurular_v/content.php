
    <!-- content -->
    <section id="content">
                     <?php 
                     $i=0;
                     foreach($duyurular as $duyuru){ 
                        $i++;
                        if(($i%2)=="1"){

                        ?>

        <!-- announcements 1 -->
        <div class="container py-5 py-lg-5">
            <div class="row">
                <div class="col-lg-6">
                   <?php
    $image = duyurukapak_getir($duyuru->id);
    $images = ($image) ? base_url("panel/uploads/duyurular_v/$image") : base_url("panel/uploads/projeler_v/proje.jpg");
?>
                   <div class="announcementsImage">
                        <img src="<?php echo $images ?>" class="img-fluid" alt="announcements 1">
                   </div>
                </div>
                <div class="col-lg-6">
                    <!-- announcements text -->
                   <div class="announcementsText">
                        <!-- title -->
                        <h4><?php echo $duyuru->baslik; ?></h4>
                        <?php echo character_limiter($duyuru->aciklama,600); ?>
                        <br><br><a href="<?php echo base_url("duyurular-detay/$duyuru->url"); ?>">Devamını Oku</a>
                   </div>
                </div>                
            </div>
        </div>

       <?php 
                     }else{

                        ?> <!-- announcements 2 -->
        <div class="container py-5 py-lg-5">
            <div class="row">
                <div class="col-lg-6">
                    <!-- announcements text -->
                   <div class="announcementsText">
                        <!-- title -->
                       <h4><?php echo $duyuru->baslik; ?></h4>
                        <?php echo character_limiter($duyuru->aciklama,600); ?>
                        <br><br>
                        <a href="<?php echo base_url("duyurular-detay/$duyuru->url"); ?>">Devamını Oku</a>
                   </div>
                </div>      
                <div class="col-lg-6">
<?php
    $image = duyurukapak_getir($duyuru->id);
    $images = ($image) ? base_url("panel/uploads/duyurular_v/$image") : base_url("panel/uploads/projeler_v/proje.jpg");
?>
                   <div class="announcementsImage">
                        <img src="<?php echo $images ?>" class="img-fluid" alt="announcements 1">
                   </div>
                </div>                          
            </div>
        </div>

        <?php } }?>

        <div class="container py-auto py-sm-auto py-md-auto py-lg-5">
            <div class="row">
                <div class="col-lg-12">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination d-flex justify-content-center">
                            <li class="page-item"><a class="page-link" href="#">İlk Sayfa</a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">Son Sayfa</a></li>
                        </ul>
                    </nav>                       
                </div>
            </div>
        </div>  
      