<!DOCTYPE html>
<html lang="en">
<head>
	
	<?php $this->load->view("includes/head"); ?>

</head>
<body>
	<?php $this->load->view("includes/headerindex"); ?>
	<?php $this->load->view("includes/loginform"); ?>
	<?php $this->load->view("includes/slider"); ?>
	<?php $this->load->view("{$viewFolder}/content"); ?>



	<?php $this->load->view("includes/footer"); ?>
	<?php $this->load->view("includes/include_script"); ?>
</body>
</html>