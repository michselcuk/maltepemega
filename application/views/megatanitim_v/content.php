     
    <!-- content -->
    <section id="content">
        <div class="container py-4 py-sm-4 py-md-4 py-lg-5">
            <div class="row">
                 <!-- about us image -->
                <div class="col-lg-6">
                    <div class="owl-three owl-carousel owl-theme">
                        <!-- about us image 1 -->
                        <?php foreach($megatanitim_images as $megaimage){ ?>
                        <div class="subpageSliderImage">
                            <img src="<?php echo base_url("panel/uploads/megatanitim_v/$megaimage->img_url"); ?>" alt="About us image">
                        </div>
                        <!-- about us image 2 -->
                         <?php } ?>                                      
                    </div>
                </div>
                <!-- about us text -->
                <div class="col-lg-6 aboutUsText">
                    <div class="aboutUsTitle">
                        <h1><?php echo $megatanitim->baslik1; ?> <span><?php echo $megatanitim->baslik2; ?></span></h1>
                    </div>
                    <p><?php echo $megatanitim->aciklama; ?> </p>
                

                </div>
            </div>
        </div>
        
        
        <!-- subpage cards -->
        <div class="container py-4 py-sm-4 py-md-4 py-lg-5">
            <div class="row">
                <div class="col-lg-4">
                    <div class="subpageCards">
                        <!-- subpage cards icon -->
                        <div class="subpageCardsIcon">
                            <img src="<?php echo base_url("assets"); ?>/images/icons/team.png" alt="Veli">
                        </div>
                        <!-- subpage cards text -->
                        <div class="subpageCardsText">
                            <span>Veli</span>
                            <p>Velilerimizin ilgilerine yönelik eğitim ve söyleşilerin yapılacağı 'Eğitim Radyosu' velilerimiz için nitelikli programlar sunacaktır.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="subpageCards">
                        <!-- subpage cards icon -->
                        <div class="subpageCardsIcon">
                            <img src="<?php echo base_url("assets"); ?>/images/icons/reading.png" alt="Öğrenci">
                        </div>
                        <!-- subpage cards text -->
                        <div class="subpageCardsText">
                            <span>Öğrenci</span>
                            <p>21.yy becerilerine sahip öğrenciler yetiştirmek için 2023 vizyonuna uygun planlamalar yapmaktadır.</p>
                        </div>
                    </div>                      
                </div>
                <div class="col-lg-4">
                    <div class="subpageCards">
                        <!-- subpage cards icon -->
                        <div class="subpageCardsIcon">
                            <img src="<?php echo base_url("assets"); ?>/images/icons/graduated-student-avatar.png" alt="Öğretmen">
                        </div>
                        <!-- subpage cards text -->
                        <div class="subpageCardsText">
                            <span>Öğretmen</span>
                            <p>Teknoloji çağı olan 21. yy da Öğretmenlerden beklenen becerilerin geliştirilmesine yönelik çalışmalar planlamktadır.</p>
                        </div>
                    </div>                        
                </div>
            </div>
        </div>

        <div class="container py-4 py-sm-4 py-md-4 py-lg-4">
            <div class="row">
                <div class="col-12 text-center">
                    <!-- main title -->
                    <div class="mainTitle">
                        <div class="aboutUsTitle">
                            <h1><?php echo $megatanitim->baslik1; ?> <span><?php echo $megatanitim->baslik2; ?></span></h1>
                        </div>    
                        <p><?php echo $megatanitim->altbaslik; ?></p>                
                    </div>
                </div>
            </div>
        </div>


        <!-- about us profile -->
       <div class="container py-4 py-sm-4 py-md-4 py-lg-4">
           <div class="row">
                <div class="owl-four owl-carousel owl-theme">
                       <?php foreach($ekip as $ekipuye){ ?>              
                        <div class="col-lg-12">
                            <div class="aboutUsProfile">
                                <!-- about us profile image -->
                                <div class="aboutUsProfileImage">
                                    <img src="<?php echo base_url("panel/uploads/ekip_v/$ekipuye->img_url"); ?>" alt="<?php echo $ekipuye->adsoyad; ?>">
                                </div>
                                <!-- about us profile name -->
                                <div class="aboutUsProfileName">
                                    <h5><?php echo $ekipuye->adsoyad; ?></h5>
                                    
                                </div>
                               <p><?php echo $ekipuye->gorev; ?></p>                    
                            </div>
                        </div>  
                        <!-- about us profile 2 -->
                        <?php } ?>  
                </div>                                                          
           </div>
       </div>